	#### data at #200 ####

	LOC	Data_Segment



	#### predefined variables ####

SP	GREG
FP	GREG
HP	GREG
Base	GREG	@

_newl	BYTE	10,0
_prtc	BYTE	63,0	#printing chars
_buff	OCTA	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
_bi	OCTA	0	#buffer index, points to first empty



	#### data fragments ####




	#### execution at #100 ####

	LOC	#100

Main	SETH	FP,#00		#TODO set FP right above data fragments
	INCMH	FP,#00		# for relative access
	INCML	FP,#00
	INCL	FP,#FFFF	

	#TODO replace 65 with fragment size (for every function)
	SUBU	SP,FP,#65	#SP = FP - fragment_size (high addr on top)

	#test: you should see at least 1 ? after executing the object code
	LDA	$255,_prtc
	TRAP	0,Fputs,StdOut

	#jump into main
	PUSHJ	$7,_		#TODO: set 8 local registers
	TRAP	0,Halt,0



	#### code fragments ####

_	SET	$0,FP
	SET	FP,SP
	SET	$1,56         #frame size
	SUB	SP,SP,$1
	SET	$1,16         #old RA offset
	SUB	$1,FP,$1
	STO	$0,$1,0		#storing old FP
	GET	$0,rJ
	STO	$0,$1,8		#storing RA
	#prologue^

L3	ADD	$0,$0,0
	SETL	$0,72
	STO	$0,$254,0
	SETL	$0,5
	STO	$0,$254,8
	PUSHJ	$7,_recursive
	SETL	$0,0
	SET	$0,$0

	#epilogue
	STO	$0,FP,0       #TODO change $0 to RV register
	SET	$1,16
	SUB	$1,FP,$1
	LDO	$0,$1,8
	PUT	rJ,$0         #restore RA
	SET	SP,FP         #restore SP
	LDO	FP,$1,0       #restore FP
	POP	0,0



_recursive	SET	$0,FP
	SET	FP,SP
	SET	$1,72         #frame size
	SUB	SP,SP,$1
	SET	$1,16         #old RA offset
	SUB	$1,FP,$1
	STO	$0,$1,0		#storing old FP
	GET	$0,rJ
	STO	$0,$1,8		#storing RA
	#prologue^

L4	ADD	$0,$0,0
	SETL	$0,1
	STO	$0,$254,0
	SETL	$0,8
	ADD	$0,$253,$0
	LDO	$0,$0
	STO	$0,$254,8
	PUSHJ	$7,_printint
	SETL	$0,8
	ADD	$0,$253,$0
	LDO	$2,$0
	SETL	$0,8
	ADD	$1,$253,$0
	SETL	$0,1
	SUB	$2,$2,$0
	STO	$2,$1,0
	SETL	$0,8
	ADD	$0,$253,$0
	LDO	$2,$0
	SETL	$0,0
	SETL	$1,1
	CMP	$2,$2,$0
	ZSN	$2,$2,$1
	BNZ	$2,L0
L1	ADD	$0,$0,0
	SETL	$0,1
	STO	$0,$254,0
	SETL	$0,8
	ADD	$0,$253,$0
	LDO	$0,$0
	STO	$0,$254,8
	PUSHJ	$7,_recursive
	JMP	L2
L0	ADD	$0,$0,0
L2	ADD	$0,$0,0
	SETL	$0,0
	SET	$0,$0
	STO	$0,$253,0

	#epilogue
	STO	$0,FP,0       #TODO change $0 to RV register
	SET	$1,16
	SUB	$1,FP,$1
	LDO	$0,$1,8
	PUT	rJ,$0         #restore RA
	SET	SP,FP         #restore SP
	LDO	FP,$1,0       #restore FP
	POP	0,0







	#### predefined functions ####

_println	LDA	$255,_newl
	TRAP	0,Fputs,StdOut
	POP	0,0



_printchar	SET	$0,FP
	SET	FP,SP
	SETL	$1,#64		#TODO size of code fragment
	SUB	SP,SP,$1
	SETL	$1,#32		#TODO old RA offset
	SUB	$1,FP,$1
	STO	$0,$1,0		#storing old FP
	GET	$0,rJ
	STO	$0,$1,8		#storing RA
	#prologue^	


	#body
	LDO	$0,FP,8		#get argument
	STB	$0,_prtc	#store to buffer
	LDA	$255,_prtc
	TRAP	0,Fputs,StdOut


	#epilogue
	STO	$0,FP,0		#TODO change $0 to RV register
	SETL	$1,#32
	SUB	$1,FP,$1
	LDO	$0,$1,8
	PUT	rJ,$0		#restore RA
	SET	SP,FP		#restore SP
	LDO	FP,$1,0		#restore FP
	POP	0,0



_printint	SET	$0,FP
	SET	FP,SP
	SETL	$1,#64		#size of code fragment
	SUB	SP,SP,$1
	SETL	$1,#32		#old RA offset
	SUB	$1,FP,$1
	STO	$0,$1,0		#storing old FP
	GET	$0,rJ
	STO	$0,$1,8		#storing RA
	#prologue^	
	

	#body
	LDO	$0,FP,8		#get argument
	SET	$3,0
pilab1	DIV	$1,$0,10	#j=i/10
	MUL	$1,$1,10	#j=j*10
	SUB	$1,$0,$1	#j=i-j

	#store to buffer and increment _bi
	LDA	$2,_buff
	ADDU	$2,$2,$3	#_buff[_bi]
	STO	$1,$2
	ADDU	$3,$3,8
	
	#divide i (remove decimal)
	DIV	$0,$0,10	#i=i/10
	CMP	$1,$0,0
	BNZ	$1,pilab1

	#decrement _bi, read from buffer, print, while _bi>0
pilab2	SUBU	$3,$3,8
	LDA	$2,_buff
	ADDU	$2,$2,$3	#_buff[_bi]
	LDO	$1,$2

	#add 48 and print as char
	ADD	$1,$1,48
	STO	$1,SP,8
	PUSHJ	$7,_printchar

	CMP	$1,$3,0
	BNZ	$1,pilab2

	#epilogue
	STO	$0,FP,0		#change $0 to RV register
	SETL	$1,#32
	SUB	$1,FP,$1
	LDO	$0,$1,8
	PUT	rJ,$0		#restore RA
	SET	SP,FP		#restore SP
	LDO	FP,$1,0		#restore FP
	POP	0,0





