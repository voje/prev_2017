	LOC	Data_Segment
	GREG	@	

	#variables, needed for printing
_newl	BYTE	10,0
_prnt	BYTE	63,0

	#testing
_x	BYTE	2
_y	BYTE	3
_z	BYTE	0
_c	BYTE	"k"


	LOC	#100

Main	LDB	$0,_x
	LDB	$1,_y
	ADD	$0,$0,$1
	STB	$0,_z
	
	LDB	$0,_c
	#prints char in register $0
	STB	$0,_prnt
	LDA	$255,_prnt
	TRAP	0,Fputs,StdOut

	LDB	$0,_z
	#converts value in $0 to char and prints
	ADD	$0,$0,48
	STB	$0,_prnt
	LDA	$255,_prnt
	TRAP	0,Fputs,StdOut

	#prints new line
	LDA	$255,_newl
	TRAP	0,Fputs,StdOut

	TRAP	0,Halt,0	
