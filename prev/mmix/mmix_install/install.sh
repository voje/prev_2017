#!/bin/bash

#run: $sudo ./install.sh
#now you can run mmix and mmixal from bash

cp mmix /usr/local/bin/mmix
chmod +x /usr/local/bin/mmix

cp mmixal /usr/local/bin/mmixal
chmod +x /usr/local/bin/mmixal
