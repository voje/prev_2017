package common.utils;

import compiler.phases.frames.Label;

/**
 * Created by kristjan on 11.4.2017.
 */
public class FrameHelper {
    public long localsSize;
    public long argsSize;
    public long maxArgsSize;
    public final Label lbl;
    public final int depth;
    public boolean countingArgs;
    public long paramOffset;

    public FrameHelper(Label lbl, int depth) {
        this.depth = depth;
        this.lbl = lbl;
        this.localsSize = 0;
        this.argsSize = 0;
        this.maxArgsSize = 0;
        this.countingArgs = false;
        this.paramOffset = 0;
    }

    public void updateMaxSize() {
        if (argsSize > maxArgsSize) {
            maxArgsSize = argsSize;
        }
        argsSize = 0;
    }
}
