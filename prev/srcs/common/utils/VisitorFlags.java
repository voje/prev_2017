package common.utils;

/**
 * Created by kristjan on 2.4.2017.
 */
public class VisitorFlags {
    public VisitorFlags() {
        this.firstPass = true;
        this.declLoopCounter = 0;
        this.topLevelStmtExpr = true;
        this.topLevelExpr = true;
    }

    public boolean firstPass;
    public int declLoopCounter;
    public boolean topLevelStmtExpr;
    public boolean topLevelExpr;
}
