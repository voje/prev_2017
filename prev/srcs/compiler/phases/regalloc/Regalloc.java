package compiler.phases.regalloc;

import common.report.Report;
import compiler.phases.Phase;
import compiler.phases.asmgen.AsmGen;
import compiler.phases.asmgen.AsmInstr;
import compiler.phases.asmgen.AsmOPER;
import compiler.phases.frames.Temp;
import compiler.phases.lincode.Fragment;
import compiler.phases.liveness.InterGraph;
import compiler.phases.liveness.Liveness;
import compiler.phases.liveness.Node;

import java.util.HashMap;
import java.util.Vector;

/**
 * Created by kristjan on 27.6.2017.
 */
public class Regalloc extends Phase {

    public static boolean debug = false;
    public static boolean fix_spills = true;
    public static int K = 8;

    public Regalloc() {
        super("regalloc");
    }

    public static void color_all_graphs() {
        //keep coloring and repeating liveness phase until spills are handled
        boolean clear = true;
        int safety = 0;
        int safety_threshold = 10;
        if (debug) AsmGen.debug_print();
        do {
            clear = true;
            //loop through all fragments
            Liveness.gen_igraphs();
            for (int i = 0; i < Liveness.igraph.size(); i++) {
                int n_spills = Liveness.igraph.get(i).color_me(); //returns number of spills
                if (n_spills > 0) {
                    System.out.printf("Fixing %d spills.%n", n_spills);
                    clear = false;
                }
            }
            if (debug) {
                System.out.println("Iteration::::::::::::::::::::::::::::::::::::::::::::::::: " + safety);
                AsmGen.debug_print();
                Liveness.debug_print();
            }
                Regalloc.debug_print(); //we need this one
            if (debug) {
                System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::: " + safety);
            }
            safety++;
        } while(!clear && safety < safety_threshold);


        if (safety == safety_threshold) {
            throw new Report.Error("Could not handle spills.");
        }
    }

    public static void debug_print() {
        //this function is mandatory for updating register maps of fragments
        if (debug) {
            for (int i = 0; i < Liveness.igraph.size(); i++) {
                System.out.println("Fragment " + i);
                Liveness.igraph.get(i).print_out_live();
                Liveness.igraph.get(i).print_out_igraph();
                System.out.println();
            }
        }

        int id = 0;
        for (int i = 0; i < AsmGen.instructions.size(); i++) {
            Vector<Vector<AsmInstr>> fragment = AsmGen.instructions.get(i);
            if (debug) System.out.printf("########################## Fragment: %d ##%n", i);
            for (int j = 0; j < fragment.size(); j++) {
                Vector<AsmInstr> canon = fragment.get(j);
                if (canon.size() == 0) continue;
                if (debug) System.out.printf("-------------------------- Canon: %d --%n", j);
                for (AsmInstr instr : canon) {
                    if (debug) System.out.printf("%3d|  ", id);
                    HashMap<Temp, Integer> cm = Liveness.igraph.get(i).color_map;
                    if (debug) System.out.println(instr.toString(cm));
                    else instr.toString(cm);
                    id += 1;
                }
            }
        }
    }

    @Override
    public void close() {
        //prints out the graphs
        if (this.debug) {
            for (int i=0; i<Liveness.igraph.size(); i++) {
                System.out.println("Fragment " + i);
                Liveness.igraph.get(i).print_out_live();
                Liveness.igraph.get(i).print_out_igraph();
                System.out.println();
            }
        } //if(this.debug)

        //prints out the instructions with mappings
        if (this.debug) {
            int id = 0;
            for (int i = 0; i < AsmGen.instructions.size(); i++) {
                Vector<Vector<AsmInstr>> fragment = AsmGen.instructions.get(i);
                System.out.printf("########################## Fragment: %d ##%n", i);
                for (int j = 0; j < fragment.size(); j++) {
                    Vector<AsmInstr> canon = fragment.get(j);
                    if (canon.size() == 0) continue;
                    System.out.printf("-------------------------- Canon: %d --%n", j);
                    for (AsmInstr instr : canon) {
                        System.out.printf("%3d|  ", id);
                        HashMap<Temp, Integer> cm = Liveness.igraph.get(i).color_map;
                        System.out.println(instr.toString(cm));
                        id += 1;
                    }
                }
            }
        } //if(this.debug)
        super.close();
    }

    public void remove_unused_commands() {
        //temps that end up as $null after register allocation were never used in the program. Remove those lines
        //careful with this one: global registers may be recognized as null

        //at this point we may hawe commands with temporaries that were not assigned registers (null)
        //that means, the temps are not used in code and can be removed
        int id = 0;
        for (int i = 0; i < AsmGen.instructions.size(); i++) {
            Vector<Vector<AsmInstr>> fragment = AsmGen.instructions.get(i);
            for (int j = 0; j < fragment.size(); j++) {
                Vector<AsmInstr> canon = fragment.get(j);
                for (int k=0; k<canon.size(); k++) {
                    if (canon.size() == 0) continue;
                    AsmInstr instr = canon.get(k);
                    HashMap<Temp, Integer> cm = Liveness.igraph.get(i).color_map;
                    //Check all temps in this instruction.
                    //If we find a null, we can remove the instruction.
                    if (instr instanceof AsmOPER) {
                        AsmOPER oinstr = (AsmOPER)instr;
                        for (int ii = 0; ii < oinstr.uses().size(); ii++) {
                            if (cm.get(oinstr.uses().get(ii)) == null) {
                                canon.remove(k);
                                remove_unused_commands(); //repeat recursively
                                return;
                            }
                        }
                        for (int ii = 0; ii < oinstr.defs().size(); ii++) {
                            if (cm.get(oinstr.defs().get(ii)) == null) {
                                canon.remove(k);
                                remove_unused_commands();
                                return;
                            }
                        }
                    }
                    id += 1;
                }
            }
        }
    }

}
