package compiler.phases.imcgen;

import java.util.*;

import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;
import compiler.phases.frames.*;
import compiler.phases.imcgen.code.*;

public class ImcStmtGenerator implements AbsVisitor<ImcStmt, Stack<Frame>> {

    public ImcStmtGenerator() {};

    @Override
    public ImcStmt visit(AbsArgs args, Stack<Frame> visArg) {
        for (AbsExpr arg : args.args())
            arg.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsArrExpr arrExpr, Stack<Frame> visArg) {
        arrExpr.array.accept(this, visArg);
        arrExpr.index.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsArrType arrType, Stack<Frame> visArg) {
        arrType.len.accept(this, visArg);
        arrType.elemType.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsAssignStmt assignStmt, Stack<Frame> visArg) {
        ImcStmt is = null;
        ImcExpr ie1 = assignStmt.dst.accept(new ImcExprGenerator(), visArg);
        ImcExpr ie2 = assignStmt.src.accept(new ImcExprGenerator(), visArg);
        //add mem to ie1
        //ImcExpr iem1 = new ImcMEM(ie1);
        ImcExpr iem1 = ie1;
        ImcExpr iem2 = ie2;
        //todo : check mem
        /*
        if (! (iem1 instanceof ImcCONST) )
            iem1 = new ImcMEM(iem1);
        if (! (iem2 instanceof ImcCONST) )
            iem2 = new ImcMEM(iem2);
        */
        is = new ImcMOVE(iem1, iem2); //(expr, MEM expr)
        ImcGen.stmtImCode.put(assignStmt, is);
        return is;
    }

    @Override
    public ImcStmt visit(AbsAtomExpr atomExpr, Stack<Frame> visArg) {
        return null;
    }

    @Override
    public ImcStmt visit(AbsAtomType atomType, Stack<Frame> visArg) {
        return null;
    }

    @Override
    public ImcStmt visit(AbsBinExpr binExpr, Stack<Frame> visArg) {
        binExpr.fstExpr.accept(this, visArg);
        binExpr.sndExpr.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsCastExpr castExpr, Stack<Frame> visArg) {
        castExpr.type.accept(this, visArg);
        castExpr.expr.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsCompDecl compDecl, Stack<Frame> visArg) {
        compDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsCompDecls compDecls, Stack<Frame> visArg) {
        for (AbsCompDecl compDecl : compDecls.compDecls())
            compDecl.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsDecls decls, Stack<Frame> visArg) {
        for (AbsDecl decl : decls.decls())
            decl.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsDelExpr delExpr, Stack<Frame> visArg) {
        delExpr.expr.accept(this, visArg);
        return null;
    }

    @Override
    public  ImcStmt visit(AbsExprStmt exprStmt, Stack<Frame> visArg) {
        ImcExpr ie = exprStmt.expr.accept(new ImcExprGenerator(), visArg);
        ImcStmt ret = new ImcESTMT(ie);
        ImcGen.stmtImCode.put(exprStmt, ret);
        return ret;
    }

    @Override
    public ImcStmt visit(AbsFunDecl funDecl, Stack<Frame> visArg) {
        funDecl.parDecls.accept(this, visArg);
        funDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsFunDef funDef, Stack<Frame> visArg) {
        funDef.parDecls.accept(this, visArg);
        funDef.type.accept(this, visArg);
        funDef.value.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsFunName funName, Stack<Frame> visArg) {
        funName.args.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsIfStmt ifStmt, Stack<Frame> visArg) {
        //cjump(L1, L2) - L1 - ifthen - J L3 - L2 - ifelse - L3
        Vector<ImcStmt> stmts = new Vector<ImcStmt>();
        Label l1 = new Label();
        Label l2 = new Label();
        Label l3 = new Label();
        ImcStmt ls1 = new ImcLABEL(l1);
        ImcStmt ls2 = new ImcLABEL(l2);
        ImcStmt ls3 = new ImcLABEL(l3);

        AbsUnExpr neg_cnd = new AbsUnExpr(ifStmt.cond.location(), AbsUnExpr.Oper.NOT, ifStmt.cond);
        //ImcExpr cnd = ifStmt.cond.accept(new ImcExprGenerator(), visArg);
        ImcExpr cnd = neg_cnd.accept(new ImcExprGenerator(), visArg);
        ImcStmt ifthen = ifStmt.thenBody.accept(this, visArg);
        ImcStmt ifelse = ifStmt.elseBody.accept(this, visArg);

        stmts.add(new ImcCJUMP(cnd, l1, l2));
        stmts.add(ls2);
        stmts.add(ifthen);
        stmts.add(new ImcJUMP(l3));
        stmts.add(ls1);
        stmts.add(ifelse);
        stmts.add(ls3);

        ImcStmt res = new ImcSTMTS(stmts);
        ImcGen.stmtImCode.put(ifStmt, res);
        return res;
    }

    @Override
    public ImcStmt visit(AbsNewExpr newExpr, Stack<Frame> visArg) {
        newExpr.type.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsParDecl parDecl, Stack<Frame> visArg) {
        parDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsParDecls parDecls, Stack<Frame> visArg) {
        for (AbsParDecl parDecl : parDecls.parDecls()) {
            parDecl.accept(this, visArg);
        }
        return null;
    }

    @Override
    public ImcStmt visit(AbsPtrType ptrType, Stack<Frame> visArg) {
        ptrType.subType.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsRecExpr recExpr, Stack<Frame> visArg) {
        recExpr.record.accept(this, visArg);
        recExpr.comp.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsRecType recType, Stack<Frame> visArg) {
        recType.compDecls.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsStmtExpr stmtExpr, Stack<Frame> visArg) {
        stmtExpr.decls.accept(this, visArg);
        stmtExpr.stmts.accept(this, visArg);
        stmtExpr.expr.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsStmts stmts, Stack<Frame> visArg) {
        Vector<ImcStmt> vstmts = new Vector<ImcStmt>();
        ImcStmt tmpstmt;
        for (AbsStmt stmt : stmts.stmts()) {
            tmpstmt = stmt.accept(this, visArg);
            vstmts.add(tmpstmt);
        }
        ImcStmt ret = new ImcSTMTS(vstmts);
        return ret;
    }

    @Override
    public ImcStmt visit(AbsTypeDecl typeDecl, Stack<Frame> visArg) {
        typeDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsTypeName typeName, Stack<Frame> visArg) {
        return null;
    }

    @Override
    public ImcStmt visit(AbsUnExpr unExpr, Stack<Frame> visArg) {
        unExpr.subExpr.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsVarDecl varDecl, Stack<Frame> visArg) {
        varDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public ImcStmt visit(AbsVarName varName, Stack<Frame> visArg) {
        return null;
    }

    @Override
    public ImcStmt visit(AbsWhileStmt whileStmt, Stack<Frame> visArg) {
        //neg cond first
        Vector<ImcStmt> stmts = new Vector<ImcStmt>();
        Label l1 = new Label();
        Label l2 = new Label();
        Label l3 = new Label();
        ImcStmt ls1 = new ImcLABEL(l1);
        ImcStmt ls2 = new ImcLABEL(l2);
        ImcStmt ls3 = new ImcLABEL(l3);

        AbsUnExpr neg_cnd = new AbsUnExpr(whileStmt.cond.location(), AbsUnExpr.Oper.NOT, whileStmt.cond);
        //ImcExpr cnd = whileStmt.cond.accept(new ImcExprGenerator(), visArg);
        ImcExpr cnd = neg_cnd.accept(new ImcExprGenerator(), visArg);

        ImcStmt bod = whileStmt.body.accept(this, visArg);

        stmts.add(ls1);
        stmts.add(new ImcCJUMP(cnd, l2, l3));
        stmts.add(ls3);
        stmts.add(bod);
        stmts.add(new ImcJUMP(l1));
        stmts.add(ls2);
        ImcStmt ret = new ImcSTMTS(stmts);
        ImcGen.stmtImCode.put(whileStmt, ret);
        return ret;
    }

}
