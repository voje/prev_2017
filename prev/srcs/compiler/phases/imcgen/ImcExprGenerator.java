package compiler.phases.imcgen;

import java.util.*;

import common.report.*;
import common.utils.Variable_transport;
import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;
import compiler.phases.seman.*;
import compiler.phases.seman.type.*;
import compiler.phases.frames.*;
import compiler.phases.imcgen.code.*;

import static compiler.phases.imcgen.code.ImcBINOP.Oper.ADD;
import static compiler.phases.imcgen.code.ImcBINOP.Oper.MUL;

public class ImcExprGenerator implements AbsVisitor<ImcExpr, Stack<Frame>> {

    public ImcExprGenerator() {};

    private ImcExpr sl_depth(long framedepth, long accdepth) {
        //static link implementation: caller's FP + depth from declaration
        ImcExpr sl = new ImcTEMP(ImcGen.FP); //if acc diff == 0, return FP //todo get FP value at runtime
        long diff = framedepth - accdepth;
        if (diff < 0) {
            throw new Report.Error(String.format("ImcGen, Frame and access depth difference is < 0. (%d)", diff));
        }
        for (int i=0; i<diff; i++) {
            sl = new ImcMEM(sl);
        }
        return sl;
    }

    @Override
    public ImcExpr visit(AbsArgs args, Stack<Frame> visArg) {
        ImcExpr ietmp = null;
        Vector<ImcExpr> vie = new Vector<ImcExpr>();
        for (AbsExpr arg : args.args()) {
            ietmp = arg.accept(this, visArg);
            if (ietmp instanceof ImcNAME) {
                ietmp = new ImcMEM(ietmp);
            }
            vie.add(ietmp);
        }
        ImcExpr ret = new ImcCALL(null, vie);
        return ret;
    }

    @Override
    public ImcExpr visit(AbsArrExpr arrExpr, Stack<Frame> visArg) {
        ImcExpr ie = arrExpr.array.accept(this, visArg);
        ImcExpr ind = arrExpr.index.accept(this, visArg);
        //no way to get type size (can't use SemAn.descType() because it has provate access)
        long typesize = 42;
        //todo ... crashes if arrExpr.array isn't AbsName
        if (arrExpr.array instanceof AbsName) {
            AbsDecl ad = SemAn.declAt().get((AbsName) (arrExpr.array));
            SemType st = SemAn.descType().get(((AbsArrType) (ad.type)).elemType);
            typesize = st.size();
        } else {
            typesize = 8;
        }
        ImcExpr arr_offset = new ImcBINOP(MUL, ind, new ImcCONST(typesize));
        ImcExpr res = new ImcBINOP(ADD, ie, arr_offset);
        //res = new ImcMEM(res);
        ImcGen.exprImCode.put(arrExpr, res);
        return res;
    }

    @Override
    public ImcExpr visit(AbsArrType arrType, Stack<Frame> visArg) {
        arrType.len.accept(this, visArg);
        arrType.elemType.accept(this, visArg);
        return null;
    }

    @Override
    public ImcExpr visit(AbsAssignStmt assignStmt, Stack<Frame> visArg) {
        assignStmt.dst.accept(this, visArg);
        assignStmt.src.accept(this, visArg);
        return null;
    }

    @Override
    public ImcExpr visit(AbsAtomExpr atomExpr, Stack<Frame> visArg) {
        ImcExpr ie = null;
        Long val;
        switch (atomExpr.type) {
            case INT:
                ie = new ImcCONST(atomExpr.accept(new ConstIntEvaluator(), null));
                break;
            case CHAR:
                char ch = atomExpr.expr.charAt(1);
                val = (long)ch;
                ie = new ImcCONST(val);
                break;
            case VOID:
                ie = new ImcCONST(0);
                break;
            case PTR:
                if (atomExpr.expr.equals("null")) {
                    ie = new ImcCONST(0);
                } else {
                    val = atomExpr.accept(new ConstIntEvaluator(), null);
                    ie = new ImcCONST(val);
                }
                break;
            case BOOL:
                if (atomExpr.expr.equals("true")) {
                    ie = new ImcCONST(1);
                } else {
                    ie = new ImcCONST(0);
                }
                break;
        }
        ImcGen.exprImCode.put(atomExpr, ie);
        return ie;
    }

    @Override
    public ImcExpr visit(AbsAtomType atomType, Stack<Frame> visArg) {
        return null;
    }

    @Override
    public ImcExpr visit(AbsBinExpr binExpr, Stack<Frame> visArg) {
        ImcExpr ie1 = binExpr.fstExpr.accept(this, visArg);
        ImcExpr ie2 = binExpr.sndExpr.accept(this, visArg);
        String opstr = binExpr.oper.toString();
        ImcBINOP.Oper op = ImcBINOP.Oper.valueOf(opstr);
        ImcExpr iem1 = ie1;
        ImcExpr iem2 = ie2;
        /*
        if (binExpr.fstExpr instanceof AbsVarName)
            iem1 = new ImcMEM(ie1);
        if (binExpr.sndExpr instanceof AbsVarName)
            iem2 = new ImcMEM(ie2);
        */ //handle mem varname in AbsVarName
        ImcExpr ie = new ImcBINOP((ImcBINOP.Oper)op, iem1, iem2);
        ImcGen.exprImCode.put(binExpr, ie);
        return ie;
    }

    @Override
    public ImcExpr visit(AbsCastExpr castExpr, Stack<Frame> visArg) {
        castExpr.type.accept(this, visArg);
        ImcExpr ie = castExpr.expr.accept(this, visArg);
        ImcGen.exprImCode.put(castExpr, ie);
        return ie;
    }

    @Override
    public ImcExpr visit(AbsCompDecl compDecl, Stack<Frame> visArg) {
        compDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public ImcExpr visit(AbsCompDecls compDecls, Stack<Frame> visArg) {
        for (AbsCompDecl compDecl : compDecls.compDecls())
            compDecl.accept(this, visArg);
        return null;
    }

    @Override
    public ImcExpr visit(AbsDecls decls, Stack<Frame> visArg) {
        for (AbsDecl decl : decls.decls())
            decl.accept(this, visArg);
        return null;
    }

    @Override
    public ImcExpr visit(AbsDelExpr delExpr, Stack<Frame> visArg) {
        ImcExpr addr = delExpr.expr.accept(this, visArg);
        Vector<ImcExpr> vie = new Vector<ImcExpr>();
        vie.add(addr);
        ImcExpr ret = new ImcCALL(new Label("free"), vie);
        ImcGen.exprImCode.put(delExpr, ret);
        return ret;
    }

    @Override
    public  ImcExpr visit(AbsExprStmt exprStmt, Stack<Frame> visArg) {
        exprStmt.expr.accept(this, visArg);
        return null;
    }

    @Override
    public ImcExpr visit(AbsFunDecl funDecl, Stack<Frame> visArg) {
        funDecl.parDecls.accept(this, visArg);
        funDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public ImcExpr visit(AbsFunDef funDef, Stack<Frame> visArg) {
        //get frame
        Frame frm = Frames.frames.get(funDef); //arg: AbsFunDef
        //add frame to Stack
        if (visArg == null) {
            visArg = new Stack<Frame>();
            visArg.add(frm);
        } else {
            visArg.add(frm);
        }
        funDef.parDecls.accept(this, visArg);
        funDef.type.accept(this, visArg);
        funDef.value.accept(this, visArg);

        visArg.pop();
        return null;
    }

    @Override
    public ImcExpr visit(AbsFunName funName, Stack<Frame> visArg) {
        boolean first_call = false;
        if (visArg == null) {
            visArg = new Stack<Frame>();
        }
        first_call = ( ((Stack<Frame>)visArg).size() == 0 );

        ImcExpr ret = null;
        Frame frm = null;

        //get frame (if the function has a definition)
        AbsDecl adef = SemAn.declAt().get(funName);
        if ( adef instanceof AbsFunDef ) {
            frm = Frames.frames.get((AbsFunDef)adef); //arg: AbsFunDef
        }

        ImcExpr ieargs = funName.args.accept(this, visArg);
        Vector<ImcExpr> args = ((ImcCALL)ieargs).args();

        ImcExpr SL;
        if (first_call) {
            SL = new ImcCONST(72); //dummy static link for level 1 function call
        } else {
            //static link equals to FP (pass FP address as const)
            SL = new ImcCONST(ImcGen.FP.temp);
        }
        args.add(0, SL);


        if (adef instanceof AbsFunDef) {
            ret = new ImcCALL(frm.label, args);
        } else {
            ret = new ImcCALL(new Label(funName.name), (args));
        }
        ImcGen.exprImCode.put(funName, ret);
        return ret;
    }

    @Override
    public ImcExpr visit(AbsIfStmt ifStmt, Stack<Frame> visArg) {
        ifStmt.cond.accept(this, visArg);
        ifStmt.thenBody.accept(this, visArg);
        ifStmt.elseBody.accept(this, visArg);
        return null;
    }

    @Override
    public ImcExpr visit(AbsNewExpr newExpr, Stack<Frame> visArg) {
        newExpr.type.accept(this, visArg);
        long typesize = 0;
        SemType st = SemAn.descType().get(newExpr.type);
        typesize = st.size();
        Vector<ImcExpr> vie = new Vector<ImcExpr>();
        vie.add(new ImcCONST(typesize));
        ImcExpr ret = new ImcCALL(new Label("malloc"), vie);
        ImcGen.exprImCode.put(newExpr, ret);
        return ret;
    }

    @Override
    public ImcExpr visit(AbsParDecl parDecl, Stack<Frame> visArg) {
        parDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public ImcExpr visit(AbsParDecls parDecls, Stack<Frame> visArg) {
        for (AbsParDecl parDecl : parDecls.parDecls())
            parDecl.accept(this, visArg);
        return null;
    }

    @Override
    public ImcExpr visit(AbsPtrType ptrType, Stack<Frame> visArg) {
        ptrType.subType.accept(this, visArg);
        return null;
    }

    @Override
    public ImcExpr visit(AbsRecExpr recExpr, Stack<Frame> visArg) {
        ImcExpr re = recExpr.record.accept(this, visArg);
        AbsDecl ad = SemAn.declAt().get((AbsName)recExpr.record);
        AbsRecType art;
        //handle custom type
        if (ad.type instanceof AbsTypeName) {
            AbsDecl ad1 = SemAn.declAt().get((AbsTypeName)(ad.type));
            art = (AbsRecType)(ad1.type);
        } else {
            art = (AbsRecType)(ad.type);
        }
        SymbTable st = SemAn.recSymbTable().get(art);

        //we need an offset for each component
        //handle offsets here, using recSymbTable
        //ImcExpr offset = recExpr.comp.accept(this, visArg); //can't get offset from varName; declAt doesn't work

        long offs = 42;
        try {
            AbsDecl acd = st.fnd(recExpr.comp.name);
            Access ac = Frames.accesses.get((AbsVarDecl)acd);
            offs = ((RelAccess)ac).offset;
        } catch (SymbTable.CannotFndNameException e) {
            throw new Report.Error(recExpr.location(), "ImcGen | Can't find rec component name.");
        }
        ImcExpr offset = new ImcCONST(offs);

        ImcExpr ret = new ImcBINOP(ADD, re, offset);
        ImcGen.exprImCode.put(recExpr, ret);
        return ret;
    }

    @Override
    public ImcExpr visit(AbsRecType recType, Stack<Frame> visArg) {
        recType.compDecls.accept(this, visArg);
        return null;
    }

    @Override
    public ImcExpr visit(AbsStmtExpr stmtExpr, Stack<Frame> visArg) {
        stmtExpr.decls.accept(this, visArg);
        ImcStmt is = stmtExpr.stmts.accept(new ImcStmtGenerator(), visArg);

        if (visArg == null) {
            visArg = new Stack<Frame>();
        };
        ImcExpr ie = stmtExpr.expr.accept(this, visArg);
        if (ie instanceof ImcNAME) {
            ie = new ImcMEM(ie);
        }
        ImcExpr ret = new ImcSEXPR(is, ie);
        //Add label .... or not...
        /*
        if (visArg != null) {
            Frame fr = visArg.peek();
            ret = new ImcSEXPR(new ImcLABEL(fr.label), ret);
        }
        */
        ImcGen.exprImCode.put(stmtExpr, ret);
        return ret;
    }

    @Override
    public ImcExpr visit(AbsStmts stmts, Stack<Frame> visArg) {
        for (AbsStmt stmt : stmts.stmts())
            stmt.accept(this, visArg);
        return null;
    }

    @Override
    public ImcExpr visit(AbsTypeDecl typeDecl, Stack<Frame> visArg) {
        typeDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public ImcExpr visit(AbsTypeName typeName, Stack<Frame> visArg) {
        return null;
    }

    @Override
    public ImcExpr visit(AbsUnExpr unExpr, Stack<Frame> visArg) {
        ImcExpr se = unExpr.subExpr.accept(this, visArg);
        ImcExpr ie = null;
        switch (unExpr.oper) {
            case NOT:
                ie = new ImcUNOP(ImcUNOP.Oper.NOT, se);
                break;
            case SUB:
                if (se instanceof ImcNAME)
                    ie = new ImcBINOP(ImcBINOP.Oper.SUB, new ImcCONST(0), new ImcMEM(se));
                else
                    ie = new ImcUNOP(ImcUNOP.Oper.NEG, se);
                break;
            case ADD:
                if (se instanceof ImcNAME)
                    ie = new ImcMEM(se);
                else
                    ie = se;
                break;
            case VAL:
                //todo @$@
                //@, return value
                ie = new ImcMEM(se);
                break;
            case MEM:
                //@, return address
                ie = se;
                break;

        }
        ImcGen.exprImCode.put(unExpr, ie);
        return ie;
    }

    @Override
    public ImcExpr visit(AbsVarDecl varDecl, Stack<Frame> visArg) {
        varDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public ImcExpr visit(AbsVarName varName, Stack<Frame> visArg) {
        //handle record components (return offset within record);
        AbsDecl ad = SemAn.declAt().get(varName);
        Access ac = Frames.accesses.get((AbsVarDecl) ad);
        if (ac instanceof AbsAccess) {
            Label lb = ((AbsAccess) ac).label;
            ImcExpr ie = new ImcMEM(new ImcNAME(lb)); //todo is mem here right?
            ImcGen.exprImCode.put(varName, ie);
            return ie;
        } else {
            //we are in a frame
            //return offset of variable
            //big TODO (fix static link)
            if (visArg == null || visArg.size() == 0)
                return null; //safety check; we should have a frame

            Frame frm = visArg.peek();
            long accdepth = ((RelAccess) ac).depth;
            if (ad instanceof AbsParDecl)
                accdepth += 1; //fix: we're accessing parameters from the same FP not from the previous one
            long offset = ((RelAccess) ac).offset;
            ImcExpr SL = sl_depth(frm.depth, accdepth);
            ImcExpr ret = new ImcMEM(new ImcBINOP(ADD, SL, new ImcCONST(offset)));
            //ret = new ImcMEM(ret); //nope
            ImcGen.exprImCode.put(varName, ret);
            return ret;
        }
    }

    @Override
    public ImcExpr visit(AbsWhileStmt whileStmt, Stack<Frame> visArg) {
        whileStmt.cond.accept(this, visArg);
        whileStmt.body.accept(this, visArg);
        return null;
    }
}
