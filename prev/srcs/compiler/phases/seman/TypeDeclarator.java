package compiler.phases.seman;

import common.utils.VisitorFlags;
import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;
import compiler.phases.seman.type.*;

/**
 * Declares type synonyms introduced by type declarations.
 * 
 * Methods of this visitor return {@code null} but leave their results in
 * {@link SemAn#declType()}. <AbsTypeDecl, SemNamedType>
 * 
 * @author sliva
 *
 */
public class TypeDeclarator implements AbsVisitor<Object, Object> {

    @Override
    public Object visit(AbsTypeDecl typeDecl, Object visArg) {
        //handle declaration loop
        VisitorFlags vf = new VisitorFlags();
        vf.declLoopCounter = 0;
        SemType st = typeDecl.type.accept(new TypeDefiner(), vf);
        SemNamedType ret = new SemNamedType(typeDecl);
        SemAn.declType().put(typeDecl, ret);
        return null;
    }

    // --------------------

    @Override
    public Object visit(AbsArrType arrType, Object visArg) {
        arrType.len.accept(this, visArg);
        arrType.elemType.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsAtomType atomType, Object visArg) {
        return null;
    }

    @Override
    public Object visit(AbsPtrType ptrType, Object visArg) {
        ptrType.subType.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsRecType recType, Object visArg) {
        recType.compDecls.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsTypeName typeName, Object visArg) {
        return null;
    }

    @Override
    public Object visit(AbsArgs args, Object visArg) {
        for (AbsExpr arg : args.args())
            arg.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsArrExpr arrExpr, Object visArg) {
        arrExpr.array.accept(this, visArg);
        arrExpr.index.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsAssignStmt assignStmt, Object visArg) {
        assignStmt.dst.accept(this, visArg);
        assignStmt.src.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsAtomExpr atomExpr, Object visArg) {
        return null;
    }

    @Override
    public Object visit(AbsBinExpr binExpr, Object visArg) {
        binExpr.fstExpr.accept(this, visArg);
        binExpr.sndExpr.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsCastExpr castExpr, Object visArg) {
        castExpr.type.accept(this, visArg);
        castExpr.expr.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsCompDecl compDecl, Object visArg) {
        //todo
        compDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsCompDecls compDecls, Object visArg) {
        for (AbsCompDecl compDecl : compDecls.compDecls())
            compDecl.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsDecls decls, Object visArg) {
        //todo
        for (AbsDecl decl : decls.decls())
            decl.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsDelExpr delExpr, Object visArg) {
        delExpr.expr.accept(this, visArg);
        return null;
    }

    @Override
    public  Object visit(AbsExprStmt exprStmt, Object visArg) {
        exprStmt.expr.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsFunDecl funDecl, Object visArg) {
        //funDecl.parDecls.accept(this, visArg);
        funDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsFunDef funDef, Object visArg) {
        //funDef.parDecls.accept(this, visArg);
        funDef.type.accept(this, visArg);
        funDef.value.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsFunName funName, Object visArg) {
        funName.args.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsIfStmt ifStmt, Object visArg) {
        ifStmt.cond.accept(this, visArg);
        ifStmt.thenBody.accept(this, visArg);
        ifStmt.elseBody.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsNewExpr newExpr, Object visArg) {
        newExpr.type.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsParDecl parDecl, Object visArg) {
        //parDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsParDecls parDecls, Object visArg) {
        for (AbsParDecl parDecl : parDecls.parDecls())
            parDecl.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsRecExpr recExpr, Object visArg) {
        recExpr.record.accept(this, visArg);
        recExpr.comp.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsStmtExpr stmtExpr, Object visArg) {
        stmtExpr.decls.accept(this, visArg);
        stmtExpr.stmts.accept(this, visArg);
        stmtExpr.expr.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsStmts stmts, Object visArg) {
        for (AbsStmt stmt : stmts.stmts())
            stmt.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsUnExpr unExpr, Object visArg) {
        unExpr.subExpr.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsVarDecl varDecl, Object visArg) {
        return null;
    }

    @Override
    public Object visit(AbsVarName varName, Object visArg) {
        return null;
    }

    @Override
    public Object visit(AbsWhileStmt whileStmt, Object visArg) {
        whileStmt.cond.accept(this, visArg);
        whileStmt.body.accept(this, visArg);
        return null;
    }

}
