package compiler.phases.seman;

import java.util.*;
import common.report.*;
import common.utils.VisitorFlags;
import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;
import compiler.phases.seman.type.*;

/**
 * Constructs semantic representation of each type.
 * 
 * Methods of this visitor return the constructed semantic type if the AST node
 * represents a type or {@code null} otherwise. In either case methods leave
 * their results in {@link SemAn#descType()}.
 * 
 * @author sliva
 *
 */
public class TypeDefiner implements AbsVisitor<SemType, Object> {

    @Override
    public SemType visit(AbsPtrType ptrType, Object visArg) {
        SemType st = ptrType.subType.accept(this, visArg);
        SemType ret = new SemPtrType(st);
        SemAn.descType().put(ptrType, st);
        return ret;
    }

    @Override
    public SemType visit(AbsRecType recType, Object visArg) {
        SymbTable st = new SymbTable();
        st.newScope();
        SemAn.recSymbTable().put(recType, st);

        SymbTable rs = SemAn.recSymbTable().get(recType);
        SemType ret = recType.compDecls.accept(this, rs);
        SemAn.descType().put(recType, ret);
        return ret;
    }

    @Override
    public SemType visit(AbsCompDecls compDecls, Object visArg) {
        Vector<String> compNames = new Vector<String>();
        Vector<SemType> compTypes = new Vector<SemType>();
        SemType st;
        for (AbsCompDecl compDecl : compDecls.compDecls()) {
            st = compDecl.accept(this, visArg);
            compNames.add(compDecl.name);
            compTypes.add(st);
        }
        SemType ret = new SemRecType(compNames, compTypes);
        return ret;
    }

    @Override
    public SemType visit(AbsTypeDecl typeDecl, Object visArg) {
        //typeDecl.accept(new TypeDeclarator(), null);
        SemNamedType ret = new SemNamedType(typeDecl);
        SemAn.descType().put(typeDecl.type, ret);
        return ret;
    }

    @Override
    public SemType visit(AbsTypeName typeName, Object visArg) {
        if ( visArg instanceof VisitorFlags && ((VisitorFlags)visArg).declLoopCounter > 500 ) {
            throw new Report.Error(typeName.location, "Seman: AbsTypeName (type decl loop)");
        } else if (visArg instanceof VisitorFlags && visArg!=null) {
            ((VisitorFlags)visArg).declLoopCounter += 1;
        }
        //todo: fix loop
        AbsDecl ad = SemAn.declAt().get(typeName);
        SemType ret = ((AbsTypeDecl)ad).type.accept(new TypeDefiner(), visArg);
        SemAn.descType().put(typeName, ret);
        return ret;
    }

    @Override
    public SemType visit(AbsCompDecl compDecl, Object visArg) {
        SemType st = compDecl.type.accept(this, visArg);
        try {
            String name = compDecl.name;
            ((SymbTable) visArg).ins(name, compDecl);
        } catch (Exception e) {
            throw new Report.Error (compDecl.location(), "Seman | Problem with record comp decl.");
        }
        return st;
    }

    @Override
    public SemType visit(AbsAtomType atomType, Object visArg) {
        SemType st;
        switch (atomType.type) {
            case BOOL:
                st = new SemBoolType();
                break;
            case CHAR:
                st = new SemCharType();
                break;
            case INT:
                st = new SemIntType();
                break;
            case VOID:
                st = new SemVoidType();
                break;
            default:
                throw new Report.Error("[seman2] TypeChecker SemType");
        }
        SemAn.descType().put(atomType, st);
        return st;
    }
    @Override
    public SemType visit(AbsArrType arrType, Object visArg) {
        long arrLen = arrType.len.accept(new ConstIntEvaluator(), visArg);
        SemType elType = arrType.elemType.accept(this, visArg);
        SemType ret = new SemArrType(arrLen, elType);
        SemAn.descType().put(arrType, ret);
        return ret;
    }

    //-------------------------
    @Override
    public SemType visit(AbsArgs args, Object visArg) {
        for (AbsExpr arg : args.args())
            arg.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsArrExpr arrExpr, Object visArg) {
        arrExpr.array.accept(this, visArg);
        arrExpr.index.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsAssignStmt assignStmt, Object visArg) {
        assignStmt.dst.accept(this, visArg);
        assignStmt.src.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsAtomExpr atomExpr, Object visArg) {
        return null;
    }

    @Override
    public SemType visit(AbsBinExpr binExpr, Object visArg) {
        binExpr.fstExpr.accept(this, visArg);
        binExpr.sndExpr.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsCastExpr castExpr, Object visArg) {
        castExpr.type.accept(this, visArg);
        castExpr.expr.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsDecls decls, Object visArg) {
        for (AbsDecl decl : decls.decls())
            decl.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsDelExpr delExpr, Object visArg) {
        delExpr.expr.accept(this, visArg);
        return null;
    }

    @Override
    public  SemType visit(AbsExprStmt exprStmt, Object visArg) {
        exprStmt.expr.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsFunDecl funDecl, Object visArg) {
        funDecl.parDecls.accept(this, visArg);
        funDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsFunDef funDef, Object visArg) {
        funDef.parDecls.accept(this, visArg);
        funDef.type.accept(this, visArg);
        funDef.value.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsFunName funName, Object visArg) {
        funName.args.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsIfStmt ifStmt, Object visArg) {
        ifStmt.cond.accept(this, visArg);
        ifStmt.thenBody.accept(this, visArg);
        ifStmt.elseBody.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsNewExpr newExpr, Object visArg) {
        newExpr.type.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsParDecl parDecl, Object visArg) {
        SemType st = parDecl.type.accept(this, visArg);
        return st;
    }

    @Override
    public SemType visit(AbsParDecls parDecls, Object visArg) {
        for (AbsParDecl parDecl : parDecls.parDecls())
            parDecl.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsRecExpr recExpr, Object visArg) {
        recExpr.record.accept(this, visArg);
        recExpr.comp.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsStmtExpr stmtExpr, Object visArg) {
        stmtExpr.decls.accept(this, visArg);
        stmtExpr.stmts.accept(this, visArg);
        stmtExpr.expr.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsStmts stmts, Object visArg) {
        for (AbsStmt stmt : stmts.stmts())
            stmt.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsUnExpr unExpr, Object visArg) {
        unExpr.subExpr.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsVarDecl varDecl, Object visArg) {
        SemType st = varDecl.type.accept(this, visArg);
        return st;
    }

    @Override
    public SemType visit(AbsVarName varName, Object visArg) {
        return null;
    }

    @Override
    public SemType visit(AbsWhileStmt whileStmt, Object visArg) {
        whileStmt.cond.accept(this, visArg);
        whileStmt.body.accept(this, visArg);
        return null;
    }
}
