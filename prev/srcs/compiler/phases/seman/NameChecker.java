package compiler.phases.seman;

import common.report.*;
import common.utils.VisitorFlags;
import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;

/**
 * A visitor that traverses (a part of) the AST and checks if all names used are
 * visible where they are used. This visitor uses another visitor, namely
 * {@link NameDefiner}, whenever a declaration is encountered during the AST
 * traversal.
 * 
 * @author sliva
 *
 */
public class NameChecker implements AbsVisitor<Object, Object> {

	/** The symbol table. */
	private final SymbTable symbTable;

	/**
	 * Constructs a new name checker using the specified symbol table.
	 * 
	 * @param symbTable
	 *            The symbol table.
	 */
	public NameChecker(SymbTable symbTable) {
		this.symbTable = symbTable;
	}

	@Override
	public Object visit(AbsArgs args, Object visArg) {
		for (AbsExpr arg : args.args())
			arg.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsArrExpr arrExpr, Object visArg) {
		arrExpr.array.accept(this, visArg);
		arrExpr.index.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsArrType arrType, Object visArg) {
		arrType.len.accept(this, visArg);
		arrType.elemType.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsAssignStmt assignStmt, Object visArg) {
		assignStmt.dst.accept(this, visArg);
		assignStmt.src.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsAtomExpr atomExpr, Object visArg) {
		Long val = atomExpr.accept(new ConstIntEvaluator(), null);
		return null;
	}

	@Override
	public Object visit(AbsAtomType atomType, Object visArg) {
	    //atom type doesn't need to get checked
        return null;
	}

	@Override
	public Object visit(AbsBinExpr binExpr, Object visArg) {
		binExpr.fstExpr.accept(this, visArg);
		binExpr.sndExpr.accept(this, visArg);
		Long val = binExpr.accept(new ConstIntEvaluator(), null);
		return null;
	}

	@Override
	public Object visit(AbsCastExpr castExpr, Object visArg) {
		castExpr.type.accept(this, visArg);
		castExpr.expr.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsCompDecl compDecl, Object visArg) {
		compDecl.type.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsCompDecls compDecls, Object visArg) {
		for (AbsCompDecl compDecl : compDecls.compDecls())
			compDecl.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsDecls decls, Object visArg) {
		for (AbsDecl decl : decls.decls())
			decl.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsDelExpr delExpr, Object visArg) {
		delExpr.expr.accept(this, visArg);
		return null;
	}

	@Override
	public  Object visit(AbsExprStmt exprStmt, Object visArg) {
		exprStmt.expr.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsFunDecl funDecl, Object visArg) {
	    //we're already in name definer if we encounter a funDecl...
        //funDecl.accept(new NameDefiner(symbTable), null);

		funDecl.parDecls.accept(this, visArg);
		funDecl.type.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsFunDef funDef, Object visArg) {
		//we're already in name definer if we encounter a funDef...
        //funDef.accept(new NameDefiner(symbTable), null);

		funDef.parDecls.accept(this, visArg);
		funDef.type.accept(this, visArg);
		funDef.value.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsFunName funName, Object visArg) {
		try {
			//throws error if it doesn't find
			AbsDecl tmpdecl = symbTable.fnd(funName.name);
			SemAn.declAt().put(funName, tmpdecl);
		} catch (SymbTable.CannotFndNameException e) {
			throw new Report.Error(funName.location(), "[seman] Undeclared function: " + funName.name);
		}
		funName.args.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsIfStmt ifStmt, Object visArg) {
		ifStmt.cond.accept(this, visArg);
		ifStmt.thenBody.accept(this, visArg);
		ifStmt.elseBody.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsNewExpr newExpr, Object visArg) {
		newExpr.type.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsParDecl parDecl, Object visArg) {
		parDecl.type.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsParDecls parDecls, Object visArg) {
		for (AbsParDecl parDecl : parDecls.parDecls())
			parDecl.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsPtrType ptrType, Object visArg) {
		ptrType.subType.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsRecExpr recExpr, Object visArg) {
		recExpr.record.accept(this, visArg);
		//deal with component elsewhere
		//recExpr.comp.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsRecType recType, Object visArg) {
		recType.compDecls.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsStmtExpr stmtExpr, Object visArg) {
        //handle 2 passes (1. - names, 2. - function bodies, type assignments)

	    symbTable.newScope();

	    //custom class
	    VisitorFlags vf = new VisitorFlags();

	    //first pass
        vf.firstPass = true;
		stmtExpr.decls.accept(new NameDefiner(symbTable), vf);
		//second pass
        vf.firstPass = false;
        stmtExpr.decls.accept(new NameDefiner(symbTable), vf);

		//after defining names, accept name checker (this)
		stmtExpr.stmts.accept(this, visArg);
		stmtExpr.expr.accept(this, visArg);

		symbTable.oldScope();

		return null;
	}

	@Override
	public Object visit(AbsStmts stmts, Object visArg) {
		for (AbsStmt stmt : stmts.stmts())
			stmt.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsTypeDecl typeDecl, Object visArg) {
		typeDecl.type.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsTypeName typeName, Object visArg) {
        try {
            //throws error if it doesn't find
            AbsDecl tmpdecl = symbTable.fnd(typeName.name);
            SemAn.declAt().put(typeName, tmpdecl);
        } catch (SymbTable.CannotFndNameException e) {
            throw new Report.Error(typeName.location(), "[seman] Undeclared type: " + typeName.name);
        }
        return null;
	}

	@Override
	public Object visit(AbsUnExpr unExpr, Object visArg) {
		unExpr.subExpr.accept(this, visArg);
        Long val = unExpr.accept(new ConstIntEvaluator(), null);
		return null;
	}

	@Override
	public Object visit(AbsVarDecl varDecl, Object visArg) {
		varDecl.type.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsVarName varName, Object visArg) {
        try {
            //throws error if it doesn't find
            AbsDecl tmpdecl = symbTable.fnd(varName.name);
            SemAn.declAt().put(varName, tmpdecl);
        } catch (SymbTable.CannotFndNameException e) {
            throw new Report.Error(varName.location(), "[seman] Undeclared variable: " + varName.name);
        }
        return null;
    }

	@Override
	public Object visit(AbsWhileStmt whileStmt, Object visArg) {
		whileStmt.cond.accept(this, visArg);
		whileStmt.body.accept(this, visArg);
		return null;
	}

}
