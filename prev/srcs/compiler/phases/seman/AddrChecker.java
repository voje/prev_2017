package compiler.phases.seman;

import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;

/*
Possibly the lvalue checker.
 */


public class AddrChecker implements AbsVisitor<Boolean, Object> {

    @Override
    public Boolean visit(AbsArgs args, Object visArg) {
        for (AbsExpr arg : args.args())
            arg.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsArrExpr arrExpr, Object visArg) {
        arrExpr.array.accept(this, visArg);
        arrExpr.index.accept(this, visArg);
        SemAn.isLValue().put(arrExpr, true);
        return true;
    }

    @Override
    public Boolean visit(AbsArrType arrType, Object visArg) {
        arrType.len.accept(this, visArg);
        arrType.elemType.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsAssignStmt assignStmt, Object visArg) {
        assignStmt.dst.accept(this, visArg);
        assignStmt.src.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsAtomExpr atomExpr, Object visArg) {
        return false;
    }

    @Override
    public Boolean visit(AbsAtomType atomType, Object visArg) {
        return false;
    }

    @Override
    public Boolean visit(AbsBinExpr binExpr, Object visArg) {
        binExpr.fstExpr.accept(this, visArg);
        binExpr.sndExpr.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsCastExpr castExpr, Object visArg) {
        castExpr.type.accept(this, visArg);
        castExpr.expr.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsCompDecl compDecl, Object visArg) {
        compDecl.type.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsCompDecls compDecls, Object visArg) {
        for (AbsCompDecl compDecl : compDecls.compDecls())
            compDecl.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsDecls decls, Object visArg) {
        for (AbsDecl decl : decls.decls())
            decl.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsDelExpr delExpr, Object visArg) {
        delExpr.expr.accept(this, visArg);
        return false;
    }

    @Override
    public  Boolean visit(AbsExprStmt exprStmt, Object visArg) {
        exprStmt.expr.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsFunDecl funDecl, Object visArg) {
        funDecl.parDecls.accept(this, visArg);
        funDecl.type.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsFunDef funDef, Object visArg) {
        funDef.parDecls.accept(this, visArg);
        funDef.type.accept(this, visArg);
        funDef.value.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsFunName funName, Object visArg) {
        funName.args.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsIfStmt ifStmt, Object visArg) {
        ifStmt.cond.accept(this, visArg);
        ifStmt.thenBody.accept(this, visArg);
        ifStmt.elseBody.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsNewExpr newExpr, Object visArg) {
        newExpr.type.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsParDecl parDecl, Object visArg) {
        parDecl.type.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsParDecls parDecls, Object visArg) {
        for (AbsParDecl parDecl : parDecls.parDecls())
            parDecl.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsPtrType ptrType, Object visArg) {
        ptrType.subType.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsRecExpr recExpr, Object visArg) {
        recExpr.record.accept(this, visArg);
        recExpr.comp.accept(this, visArg);
        SemAn.isLValue().put(recExpr, true);
        return true;
    }

    @Override
    public Boolean visit(AbsRecType recType, Object visArg) {
        recType.compDecls.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsStmtExpr stmtExpr, Object visArg) {
        stmtExpr.decls.accept(this, visArg);
        stmtExpr.stmts.accept(this, visArg);
        stmtExpr.expr.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsStmts stmts, Object visArg) {
        for (AbsStmt stmt : stmts.stmts())
            stmt.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsTypeDecl typeDecl, Object visArg) {
        typeDecl.type.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsTypeName typeName, Object visArg) {
        return false;
    }

    @Override
    public Boolean visit(AbsUnExpr unExpr, Object visArg) {
        unExpr.subExpr.accept(this, visArg);
        boolean res = false;
        if (unExpr.oper.equals(AbsUnExpr.Oper.VAL)) {
            res = true;
            SemAn.isLValue().put(unExpr, true);
        }
        return res;
    }

    @Override
    public Boolean visit(AbsVarDecl varDecl, Object visArg) {
        varDecl.type.accept(this, visArg);
        return false;
    }

    @Override
    public Boolean visit(AbsVarName varName, Object visArg) {
        try {
            AbsDecl d = SemAn.declAt().get(varName);
            SemAn.isLValue().put(varName, true);
            return true;
        } catch (Error e) {
            return false;
        }
    }

    @Override
    public Boolean visit(AbsWhileStmt whileStmt, Object visArg) {
        whileStmt.cond.accept(this, visArg);
        whileStmt.body.accept(this, visArg);
        return false;
    }

}
