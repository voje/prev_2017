package compiler.phases.seman;

import common.report.*;
import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;

/**
 * A visitor that computes the value of a constant integer expression.
 * 
 * @author sliva
 *
 */
public class ConstIntEvaluator implements AbsVisitor<Long, Object> {

    public ConstIntEvaluator() {
        //
    }

    private boolean inRange(String str) {
        Long a = Long.parseLong(str);
        if ( (a >= (-Math.pow(2, 63))) && (a < Math.pow(2, 63)) )
            return true;
        return false;
    }

    @Override
    public Long visit(AbsArgs args, Object visArg) {
        for (AbsExpr arg : args.args())
            arg.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsArrExpr arrExpr, Object visArg) {
        arrExpr.array.accept(this, visArg);
        arrExpr.index.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsArrType arrType, Object visArg) {
        arrType.len.accept(this, visArg);
        arrType.elemType.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsAssignStmt assignStmt, Object visArg) {
        assignStmt.dst.accept(this, visArg);
        assignStmt.src.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsAtomExpr atomExpr, Object visArg) {
        //type, expr
        if (atomExpr.type != AbsAtomExpr.Type.INT)
            return null;
        if (inRange(atomExpr.expr)) {
            return Long.parseLong(atomExpr.expr);
        }
        throw new Report.Error(atomExpr.location(), "Integer out of range.");
    }

    @Override
    public Long visit(AbsAtomType atomType, Object visArg) {
        return null;
    }

    @Override
    public Long visit(AbsBinExpr binExpr, Object visArg) {
        Long fstVal = binExpr.fstExpr.accept(this, visArg);
        Long sndVal = binExpr.sndExpr.accept(this, visArg);
        Long outVal = null;
        if (fstVal != null && sndVal != null) {
            //binExpr.oper: IOR, XOR, AND, EQU, NEQ, LTH, GTH, LEQ, GEQ, ADD, SUB, MUL, DIV, MOD,
            switch (binExpr.oper) {
                case ADD:
                    return (fstVal + sndVal);
                case SUB:
                    return (fstVal - sndVal);
                case MUL:
                    return (fstVal * sndVal);
                case DIV:
                    return (fstVal / sndVal);
                case MOD:
                    return (fstVal % sndVal);
            }
        }
        return outVal;
    }



    @Override
    public Long visit(AbsCastExpr castExpr, Object visArg) {
        castExpr.type.accept(this, visArg);
        castExpr.expr.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsCompDecl compDecl, Object visArg) {
        compDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsCompDecls compDecls, Object visArg) {
        for (AbsCompDecl compDecl : compDecls.compDecls())
            compDecl.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsDecls decls, Object visArg) {
        for (AbsDecl decl : decls.decls())
            decl.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsDelExpr delExpr, Object visArg) {
        delExpr.expr.accept(this, visArg);
        return null;
    }

    @Override
    public  Long visit(AbsExprStmt exprStmt, Object visArg) {
        exprStmt.expr.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsFunDecl funDecl, Object visArg) {
        funDecl.parDecls.accept(this, visArg);
        funDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsFunDef funDef, Object visArg) {
        funDef.parDecls.accept(this, visArg);
        funDef.type.accept(this, visArg);
        funDef.value.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsFunName funName, Object visArg) {
        funName.args.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsIfStmt ifStmt, Object visArg) {
        ifStmt.cond.accept(this, visArg);
        ifStmt.thenBody.accept(this, visArg);
        ifStmt.elseBody.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsNewExpr newExpr, Object visArg) {
        newExpr.type.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsParDecl parDecl, Object visArg) {
        parDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsParDecls parDecls, Object visArg) {
        for (AbsParDecl parDecl : parDecls.parDecls())
            parDecl.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsPtrType ptrType, Object visArg) {
        ptrType.subType.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsRecExpr recExpr, Object visArg) {
        recExpr.record.accept(this, visArg);
        recExpr.comp.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsRecType recType, Object visArg) {
        recType.compDecls.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsStmtExpr stmtExpr, Object visArg) {
        stmtExpr.decls.accept(this, visArg);
        stmtExpr.stmts.accept(this, visArg);
        stmtExpr.expr.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsStmts stmts, Object visArg) {
        for (AbsStmt stmt : stmts.stmts())
            stmt.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsTypeDecl typeDecl, Object visArg) {
        typeDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsTypeName typeName, Object visArg) {
        return null;
    }

    @Override
    public Long visit(AbsUnExpr unExpr, Object visArg) {
        Long exprVal = unExpr.subExpr.accept(this, visArg);
        return exprVal;
    }

    @Override
    public Long visit(AbsVarDecl varDecl, Object visArg) {
        varDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public Long visit(AbsVarName varName, Object visArg) {
        return null;
    }

    @Override
    public Long visit(AbsWhileStmt whileStmt, Object visArg) {
        whileStmt.cond.accept(this, visArg);
        whileStmt.body.accept(this, visArg);
        return null;
    }
    
}
