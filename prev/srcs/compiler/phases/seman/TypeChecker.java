package compiler.phases.seman;

import common.report.*;
import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;
import compiler.phases.seman.type.*;

import java.util.Vector;

/**
 * Tests whether expressions are well typed.
 * 
 * Methods of this visitor return the semantic type of a phrase being tested if
 * the AST node represents an expression or {@code null} otherwise. In the first
 * case methods leave their results in {@link SemAn#isOfType()}.
 * 
 * @author sliva
 *
 */
public class TypeChecker implements AbsVisitor<SemType, Object> {

    @Override
    public SemType visit(AbsArgs args, Object visArg) {
        //check argument types here instead of going lower
        AbsParDecls apds = (AbsParDecls)visArg;
        Vector<AbsParDecl> vapd = apds.parDecls();
        SemType st;
        Vector<SemType> vst = new Vector<SemType>();
        for (AbsExpr arg : args.args()) {
            st = arg.accept(this, null);
            vst.add(st);
        }
        if (vapd.size() != vst.size()) {
            throw new Report.Error(((AbsParDecls)visArg).location(), "Seman, AbsArgs: incorrect number of function arguments.");
        }
        //check types
        for (int i=0; i<vapd.size(); i++) {
            st = vst.get(i);
            SemType comp_st = vapd.get(i).accept(new TypeDefiner(), null);
            if (!(st.toString().equals(comp_st.toString()))) {
                throw new Report.Error(args.location(), String.format("Seman, AbsArgs: wrong function argument type. Expected %s, got %s.", st.toString(), comp_st.toString()));
            }
        }

        return null;
    }

    @Override
    public SemType visit(AbsArrExpr arrExpr, Object visArg) {
        SemType st = arrExpr.array.accept(this, null);
        SemType ret = ((SemArrType)st).elemType;
        //todo: check if index is inside array bounds
        arrExpr.index.accept(this, visArg);
        SemAn.isOfType().put(arrExpr, ret);
        return ret;
    }

    @Override
    public SemType visit(AbsArrType arrType, Object visArg) {
        return null;
    }

    @Override
    public SemType visit(AbsAssignStmt assignStmt, Object visArg) {
        //dst = src
        SemType stdst = assignStmt.dst.accept(this, visArg);
        SemType stsrc = assignStmt.src.accept(this, visArg);
        boolean dstLval = assignStmt.dst.accept(new AddrChecker(), null);
        if (dstLval && stsrc.toString().equals(stdst.toString())) {
            SemType ret = new SemVoidType();
            return ret;
        }
        throw new Report.Error(assignStmt.location(), "Seman, AbsAssignStmt");
    }

    @Override
    public SemType visit(AbsAtomExpr atomExpr, Object visArg) {
        SemType ret;
        if (atomExpr.expr.equals("null")) {
            SemType s;
            switch(atomExpr.type) {
                case VOID:
                    s = new SemVoidType();
                    break;
                case BOOL:
                    s = new SemBoolType();
                    break;
                case CHAR:
                    s = new SemCharType();
                    break;
                case INT:
                    s = new SemIntType();
                    break;
                default:
                    s = new SemVoidType();
            }
            ret = new SemPtrType(s);
        } else if (atomExpr.expr.equals("none")) {
            ret = new SemVoidType();
        } else {
            switch(atomExpr.type) {
                case VOID:
                    ret = new SemVoidType();
                    break;
                case BOOL:
                    ret = new SemBoolType();
                    break;
                case CHAR:
                    ret = new SemCharType();
                    break;
                case INT:
                    ret = new SemIntType();
                    break;
                default:
                    throw new Report.Error(atomExpr.location(), "Seman, TypeChecker");
            }
        }
        SemAn.isOfType().put(atomExpr, ret);
        return ret;
    }

    @Override
    public SemType visit(AbsAtomType atomType, Object visArg) {
        return null;
    }

    @Override
    public SemType visit(AbsBinExpr binExpr, Object visArg) {
        // |, &, ^
        if (    binExpr.oper.equals(AbsBinExpr.Oper.IOR) ||
                binExpr.oper.equals(AbsBinExpr.Oper.XOR) ||
                binExpr.oper.equals(AbsBinExpr.Oper.AND) ) {
            try {
                SemBoolType st1 = (SemBoolType)binExpr.fstExpr.accept(this, visArg);
                SemBoolType st2 = (SemBoolType)binExpr.sndExpr.accept(this, visArg);
                SemAn.isOfType().put(binExpr, st1);
                return st1;
            } catch (Exception e) {
                throw new Report.Error(binExpr.location(), "Seman, binExpr");
            }

        // +, -, *, /, %
        } else if ( binExpr.oper.equals(AbsBinExpr.Oper.ADD) ||
                    binExpr.oper.equals(AbsBinExpr.Oper.SUB) ||
                    binExpr.oper.equals(AbsBinExpr.Oper.MUL) ||
                    binExpr.oper.equals(AbsBinExpr.Oper.DIV) ||
                    binExpr.oper.equals(AbsBinExpr.Oper.MOD) ) {
            try {
                SemIntType st1 = (SemIntType)binExpr.fstExpr.accept(this, visArg);
                SemIntType st2 = (SemIntType)binExpr.sndExpr.accept(this, visArg);
                SemAn.isOfType().put(binExpr, st1);
                return st1;
            } catch (Exception e) {
                throw new Report.Error(binExpr.location(), "Seman, binExpr");
            }

        // ==, <, ...
        } else if ( binExpr.oper.equals(AbsBinExpr.Oper.EQU) ||
                    binExpr.oper.equals(AbsBinExpr.Oper.NEQ) ||
                    binExpr.oper.equals(AbsBinExpr.Oper.LTH) ||
                    binExpr.oper.equals(AbsBinExpr.Oper.GTH) ||
                    binExpr.oper.equals(AbsBinExpr.Oper.LEQ) ||
                    binExpr.oper.equals(AbsBinExpr.Oper.GEQ) ) {
            SemType st1 = binExpr.fstExpr.accept(this, visArg);
            SemType st2 = binExpr.sndExpr.accept(this, visArg);
            if (st1.toString().equals(st2.toString())) {
                if (    st1 instanceof SemBoolType ||
                        st1 instanceof SemCharType ||
                        st1 instanceof SemIntType ||
                        (st1 instanceof SemPtrType && ((SemPtrType)st1).subType != null) ) {
                    SemType ret = new SemBoolType();
                    SemAn.isOfType().put(binExpr, ret);
                    return ret;
                }
            }
        }
        throw new Report.Error(binExpr.location(), "Seman, binExpr");
    }

    @Override
    public SemType visit(AbsCastExpr castExpr, Object visArg) {
        SemType st1 = castExpr.type.accept(new TypeDefiner(), visArg);
        SemType st2 = castExpr.expr.accept(this, null);
        if (    (st1 instanceof SemVoidType && st2 != null) ||
                (st1 instanceof SemIntType && st2 instanceof SemIntType) ||
                (st1 instanceof SemIntType && st2 instanceof SemCharType) ||
                (st1 instanceof SemIntType && st2 instanceof SemBoolType) ||
                (st1 instanceof SemPtrType &&
                        (st2 instanceof SemPtrType && (((SemPtrType)st2).subType instanceof SemVoidType) ))) {
            SemType ret = st1;
            SemAn.isOfType().put(castExpr, st1);
            return st1;
        }
        throw new Report.Error(castExpr.location(), "Seman, AbsCastExpr");
    }

    @Override
    public SemType visit(AbsCompDecl compDecl, Object visArg) {
        compDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsCompDecls compDecls, Object visArg) {
        for (AbsCompDecl compDecl : compDecls.compDecls())
            compDecl.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsDecls decls, Object visArg) {
        for (AbsDecl decl : decls.decls())
            decl.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsDelExpr delExpr, Object visArg) {
        SemType st = delExpr.expr.accept(this, visArg);
        if (!(st instanceof SemVoidType)) {
            SemType ret = new SemVoidType();
            SemAn.isOfType().put(delExpr, ret);
            return ret;
        }
        throw new Report.Error(delExpr.location(), "Seman, DelExpr");
    }

    @Override
    public  SemType visit(AbsExprStmt exprStmt, Object visArg) {
        SemType ret = exprStmt.expr.accept(this, visArg);
        return ret;
    }

    @Override
    public SemType visit(AbsFunDecl funDecl, Object visArg) {
        funDecl.parDecls.accept(this, visArg);
        funDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsFunDef funDef, Object visArg) {
        funDef.parDecls.accept(this, visArg);
        funDef.type.accept(this, visArg);
        funDef.value.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsFunName funName, Object visArg) {
        AbsDecl ad = SemAn.declAt().get(funName);
        AbsParDecls pd = null;
        if ( ad instanceof AbsFunDecl ) {
            pd = ((AbsFunDecl)ad).parDecls;
        } else if ( ad instanceof AbsFunDef ) {
            pd = ((AbsFunDef)ad).parDecls;
        }
        SemType st = ad.type.accept(new TypeDefiner(), null);
        funName.args.accept(this, pd);
        SemAn.isOfType().put(funName, st);
        return st;
    }

    @Override
    public SemType visit(AbsIfStmt ifStmt, Object visArg) {
        SemType ifcond = ifStmt.cond.accept(this, visArg);
        if (!(ifcond instanceof SemBoolType))
            throw new Report.Error(ifStmt.location(), "Seman, AbsIfStmt");
        SemType ifthen = ifStmt.thenBody.accept(this, visArg);
        if (!(ifthen instanceof SemVoidType))
            throw new Report.Error(ifStmt.location(), "Seman, AbsIfStmt");
        SemType ifelse = ifStmt.elseBody.accept(this, visArg);
        if (!(ifelse instanceof SemVoidType))
            throw new Report.Error(ifStmt.location(), "Seman, AbsIfStmt");
        return new SemVoidType();
    }

    @Override
    public SemType visit(AbsNewExpr newExpr, Object visArg) {
        SemType st = newExpr.type.accept(new TypeDefiner(), null);
        if (!(st instanceof SemVoidType)) {
            SemType ret = new SemPtrType(st);
            SemAn.isOfType().put(newExpr, ret);
            return ret;
        }
        throw new Report.Error(newExpr.location(), "Seman, AbsNewExpr");
    }

    @Override
    public SemType visit(AbsParDecl parDecl, Object visArg) {
        parDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsParDecls parDecls, Object visArg) {
        for (AbsParDecl parDecl : parDecls.parDecls())
            parDecl.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsPtrType ptrType, Object visArg) {
        ptrType.subType.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsRecExpr recExpr, Object visArg) {
        SemType st = recExpr.record.accept(this, visArg);
        SemType compt = recExpr.comp.accept(this, st); //pass in st with a list of components
        SemAn.isOfType().put(recExpr, compt);
        return compt;
    }

    @Override
    public SemType visit(AbsRecType recType, Object visArg) {
        recType.compDecls.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsStmtExpr stmtExpr, Object visArg) {
        stmtExpr.decls.accept(new TypeDeclarator(), visArg);    //declare custom types
        stmtExpr.decls.accept(new TypeDefiner(), visArg);       //define all types
        stmtExpr.decls.accept(this, visArg);                    //check types

        SemType stmtsType = stmtExpr.stmts.accept(this, visArg);
        SemType exprType = stmtExpr.expr.accept(this, visArg);
        /*
        if (!(stmtsType.getClass().equals(exprType.getClass()))) {
            throw new Report.Error(stmtExpr.location(), "Seman, AbsStmtExpr");
        }
        */
        SemType ret = exprType;     //popravek
        SemAn.isOfType().put(stmtExpr, ret);
        return ret;
    }

    @Override
    public SemType visit(AbsStmts stmts, Object visArg) {
        //all statements need to be of same type??
        SemType allSt = null;
        SemType nextSt = null;
        for (AbsStmt stmt : stmts.stmts()) {
            nextSt = stmt.accept(this, visArg);
            /*
            if (allSt == null)
                allSt = nextSt;
            else if (!(allSt.getClass().equals(nextSt.getClass())))     //next statement is not the same type
                throw new Report.Error(stmts.location(), "Seman, AbsStmts");
            */
        }
        return new SemVoidType();
    }

    @Override
    public SemType visit(AbsTypeDecl typeDecl, Object visArg) {
        typeDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsTypeName typeName, Object visArg) {
        return null;
    }

    @Override
    public SemType visit(AbsUnExpr unExpr, Object visArg) {
        SemType subt = unExpr.subExpr.accept(this, visArg);
        if (subt instanceof SemBoolType && unExpr.oper.equals(AbsUnExpr.Oper.ADD)) {
            SemAn.isOfType().put(unExpr, subt);
            return subt;
        } else if (subt instanceof SemIntType && (unExpr.oper.equals(AbsUnExpr.Oper.ADD) || unExpr.oper.equals(AbsUnExpr.Oper.SUB))) {
            SemAn.isOfType().put(unExpr, subt);
            return subt;
        } else if (unExpr.oper.equals(AbsUnExpr.Oper.NOT)) {
            if (! (subt instanceof SemBoolType)) {
                throw new Report.Error(unExpr.location(), "Seman, AbsUnExpr, !<not_boolean>");
            } else {
                SemAn.isOfType().put(unExpr, subt);
                return subt;
            }
        } else if (unExpr.oper.equals(AbsUnExpr.Oper.MEM)) {
            //$
            if (!(subt instanceof SemVoidType) && unExpr.subExpr.accept(new AddrChecker(), null)) {
                SemType ret = new SemPtrType(subt);
                SemAn.isOfType().put(unExpr, ret);
                return ret;
            }
        } else if (unExpr.oper.equals(AbsUnExpr.Oper.VAL)) {
            //@
            if (subt instanceof SemPtrType) {
                SemPtrType tmpptr = (SemPtrType) subt;
                if (!(tmpptr.subType instanceof SemVoidType)) {
                    SemType ret = tmpptr.subType;
                    SemAn.isOfType().put(unExpr, ret);
                    return ret;
                }
            }
        }
        throw new Report.Error(unExpr.location(), "Seman, AbsUnExpr");
    }

    @Override
    public SemType visit(AbsVarDecl varDecl, Object visArg) {
        varDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public SemType visit(AbsVarName varName, Object visArg) {
        if (visArg instanceof SemRecType) {
            //record component
            int component_idx = -1;
            Vector<String> comps = ((SemRecType)visArg).compNames();
            Vector<SemType> comptypes = ((SemRecType)visArg).compTypes();
            for (int i=0; i<comps.size(); i++) {
                if (varName.name.equals(comps.get(i))) {
                    component_idx = i;
                }
            }
            if (component_idx == -1) {
                throw new Report.Error(varName.location(), "Undeclared component.");
            }
            SemType ret = comptypes.get(component_idx);
            SemAn.isOfType().put(varName, ret);
            return ret;
            /*
        } else if (visArg instanceof AbsParDecl) {
            //handle arguments when you get them all back
            //checking function arguments
            AbsDecl dc = SemAn.declAt().get(varName);
            SemType st = dc.type.accept(new TypeDefiner(), null);
            SemType argType = ((AbsParDecl)visArg).type.accept(new TypeDefiner(), null);
            if (!st.toString().equals(argType.toString())) {
                throw new Report.Error(varName.location(), "Seman: AbsVarName (wrong function argument type)");
            }
            SemAn.isOfType().put(varName, st);
            return st;
            */
        } else {
            //normal variable
            AbsDecl dc = SemAn.declAt().get(varName);
            SemType st = dc.type.accept(new TypeDefiner(), null);
            SemAn.isOfType().put(varName, st);
            return st;
        }
    }

    @Override
    public SemType visit(AbsWhileStmt whileStmt, Object visArg) {
        SemType stcond = whileStmt.cond.accept(this, visArg);
        if (!(stcond instanceof SemBoolType))
            throw new Report.Error(whileStmt.location(), "Seman, AbsWhileStmt");
        SemType stbody = whileStmt.body.accept(this, visArg);
        if (!(stbody instanceof SemVoidType))
            throw new Report.Error(whileStmt.location(), "Seman, AbsWhileStmt");
        return new SemVoidType();
    }

}
