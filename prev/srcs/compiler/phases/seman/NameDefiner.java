package compiler.phases.seman;

import common.report.*;
import common.utils.VisitorFlags;
import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;

/**
 * A visitor that traverses (a part of) the AST and stores all declarations
 * encountered into the symbol table. It is meant to be called from another
 * visitor, namely {@link NameChecker}.
 * 
 * @author sliva
 *
 */
public class NameDefiner implements AbsVisitor<Object, Object> {

	/** The symbol table. */
	private final SymbTable symbTable;

	/**
	 * Constructs a new name checker using the specified symbol table.
	 * 
	 * @param symbTable
	 *            The symbol table.
	 */
	public NameDefiner(SymbTable symbTable) {
		this.symbTable = symbTable;
	}

	@Override
	public Object visit(AbsArgs args, Object visArg) {
		for (AbsExpr arg : args.args())
			arg.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsArrExpr arrExpr, Object visArg) {
		arrExpr.array.accept(this, visArg);
		arrExpr.index.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsArrType arrType, Object visArg) {
		arrType.len.accept(this, visArg);
		arrType.elemType.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsAssignStmt assignStmt, Object visArg) {
		assignStmt.dst.accept(this, visArg);
		assignStmt.src.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsAtomExpr atomExpr, Object visArg) {
		return null;
	}

	@Override
	public Object visit(AbsAtomType atomType, Object visArg) {
		return null;
	}

	@Override
	public Object visit(AbsBinExpr binExpr, Object visArg) {
		binExpr.fstExpr.accept(this, visArg);
		binExpr.sndExpr.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsCastExpr castExpr, Object visArg) {
		castExpr.type.accept(this, visArg);
		castExpr.expr.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsCompDecl compDecl, Object visArg) {
		compDecl.type.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsCompDecls compDecls, Object visArg) {
		for (AbsCompDecl compDecl : compDecls.compDecls())
			compDecl.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsDecls decls, Object visArg) {
		for (AbsDecl decl : decls.decls())
			decl.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsDelExpr delExpr, Object visArg) {
		delExpr.expr.accept(this, visArg);
		return null;
	}

	@Override
	public  Object visit(AbsExprStmt exprStmt, Object visArg) {
		exprStmt.expr.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsFunDecl funDecl, Object visArg) {
        if (((VisitorFlags) visArg).firstPass) {
            //add function name to scope
            try {
                symbTable.ins(funDecl.name, funDecl);
            } catch (SymbTable.CannotInsNameException e) {
                throw new Report.Error(funDecl.location(), "[seman] Function name already exists: " + funDecl.name);
            }
        } else {
			//check return type
			funDecl.type.accept(new NameChecker(symbTable), visArg);
            //check arg types, also send body to arg check
            funDecl.parDecls.accept(this, null);
		}
        return null;
    }

	@Override
	public Object visit(AbsFunDef funDef, Object visArg) {
	    if (((VisitorFlags)visArg).firstPass) {
            //add function name to scope
            try {
                symbTable.ins(funDef.name, funDef);
            } catch (SymbTable.CannotInsNameException e) {
                throw new Report.Error(funDef.location(), "[seman] Function name already exists: " + funDef.name);
            }
        } else {
			//check return type
			funDef.type.accept(new NameChecker(symbTable), visArg);
            //check arg types, also send body to arg check
            funDef.parDecls.accept(this, funDef.value);
        }
		return null;
	}

	@Override
	public Object visit(AbsFunName funName, Object visArg) {
		funName.args.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsIfStmt ifStmt, Object visArg) {
		ifStmt.cond.accept(this, visArg);
		ifStmt.thenBody.accept(this, visArg);
		ifStmt.elseBody.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsNewExpr newExpr, Object visArg) {
		newExpr.type.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsParDecl parDecl, Object visArg) {
		parDecl.type.accept(new NameChecker(symbTable), visArg);

		//names of variables go in lower scope
        //symbTable.newScope();

		try {
			symbTable.ins(parDecl.name, parDecl);
		} catch (SymbTable.CannotInsNameException e) {
            throw new Report.Error(parDecl.location(), "[seman] Parameter name already exists: " + parDecl.name);
		}

		//while in this scope, check function body variable names
        /*
        if (visArg instanceof AbsExpr) {
            ((AbsExpr)visArg).accept(new NameChecker(symbTable), null);
        }
        */

		//symbTable.oldScope();

		return null;
	}

	@Override
	public Object visit(AbsParDecls parDecls, Object visArg) {
        symbTable.newScope();
		for (AbsParDecl parDecl : parDecls.parDecls()) {
            parDecl.accept(this, visArg);
        }

		//check function body based on parameter declerations
        if (visArg instanceof AbsExpr) {
            ((AbsExpr)visArg).accept(new NameChecker(symbTable), null);
        }

		symbTable.oldScope();
		return null;
	}

	@Override
	public Object visit(AbsPtrType ptrType, Object visArg) {
		ptrType.subType.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsRecExpr recExpr, Object visArg) {
		recExpr.record.accept(this, visArg);
		recExpr.comp.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsRecType recType, Object visArg) {
		recType.compDecls.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsStmtExpr stmtExpr, Object visArg) {
		stmtExpr.decls.accept(this, visArg);
		stmtExpr.stmts.accept(this, visArg);
		stmtExpr.expr.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsStmts stmts, Object visArg) {
		for (AbsStmt stmt : stmts.stmts())
			stmt.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsTypeDecl typeDecl, Object visArg) {
        if (((VisitorFlags)visArg).firstPass) {
            //first pass: add type name
            try {
                symbTable.ins(typeDecl.name, typeDecl);
            } catch (SymbTable.CannotInsNameException e) {
                throw new Report.Error(typeDecl.location(), "[seman] Type name already exists: " + typeDecl.name);
            }
        } else {
            //second pass: check type
            typeDecl.type.accept(new NameChecker(symbTable), visArg);
        }
        return null;
	}

	@Override
	public Object visit(AbsTypeName typeName, Object visArg) {
		return null;
	}

	@Override
	public Object visit(AbsUnExpr unExpr, Object visArg) {
		unExpr.subExpr.accept(this, visArg);
		return null;
	}

	@Override
	public Object visit(AbsVarDecl varDecl, Object visArg) {
	    if (((VisitorFlags)visArg).firstPass) {
            //first pass: insert name of variable
            try {
                symbTable.ins(varDecl.name, varDecl);
            } catch (SymbTable.CannotInsNameException e) {
                throw new Report.Error(varDecl.location(), "[seman] Variable name already exists: " + varDecl.name);
            }
        } else {
	        //second pass: check type
            varDecl.type.accept(new NameChecker(symbTable), visArg);
        }
        return null;
	}

	@Override
	public Object visit(AbsVarName varName, Object visArg) {
		return null;
	}

	@Override
	public Object visit(AbsWhileStmt whileStmt, Object visArg) {
		whileStmt.cond.accept(this, visArg);
		whileStmt.body.accept(this, visArg);
		return null;
	}

}
