package compiler.phases.abstr;

import java.util.*;
import common.report.*;
import compiler.phases.abstr.abstree.*;
import compiler.phases.lexan.Symbol;
import compiler.phases.lexan.Term;
import compiler.phases.synan.*;
import compiler.phases.synan.dertree.*;

import static compiler.phases.lexan.Term.*;

/**
 * Transforms a derivation tree into an abstract syntax tree.
 * 
 * @author sliva
 *
 */
public class DerToAbsTree implements DerVisitor<AbsTree, AbsTree> {

    private final AbsTree absTree;

    public DerToAbsTree() {
        absTree = null;
    };

    //you have to be sure it's a symbol
    private Term getTerm(Vector<DerTree> subtrs, int child) {
        Object acc_frst = subtrs.get(child);
        if (acc_frst instanceof DerLeaf) {
            return (((DerLeaf)acc_frst).symb).token;
        } else {
            return null;
        }
    }

    @Override
    public AbsTree visit(DerLeaf leaf, AbsTree visArg) {
        //System.out.printf("[[ %s ]]%n", leaf.symb); //hideme
        switch (leaf.symb.token) {
            case INTCONST:
                return new AbsAtomExpr(leaf.location(), AbsAtomExpr.Type.INT, leaf.symb.lexeme);
            case BOOLCONST:
                return new AbsAtomExpr(leaf.location(), AbsAtomExpr.Type.BOOL, leaf.symb.lexeme);
            case PTRCONST:
                return new AbsAtomExpr(leaf.location(), AbsAtomExpr.Type.PTR, leaf.symb.lexeme);
            case CHARCONST:
                return new AbsAtomExpr(leaf.location(), AbsAtomExpr.Type.CHAR, leaf.symb.lexeme);
            case VOIDCONST:
                return new AbsAtomExpr(leaf.location(), AbsAtomExpr.Type.VOID, leaf.symb.lexeme);
            case IDENTIFIER:
                return new AbsVarName(leaf.location(), leaf.symb.lexeme);
            default:
                throw new Report.Error(leaf.location(), String.format("Abstr Leaf error: %s", leaf.symb.lexeme));
        }
    }

    @Override
    public AbsTree visit(DerNode node, AbsTree visArg) {
        //System.out.printf("%s -> ", node.label); //hideme
        Vector<DerTree> subtrs = node.subtrees();
        int nchild = subtrs.size();
        AbsTree a, e, l, r, s, t;
        int child_offset = 0;

        switch (node.label) {
            case Source:
            case Expr:
                if (getTerm(subtrs, 0) == LBRACKET) { //cast expr
                    t = subtrs.get(1).accept(this, null);
                    e = subtrs.get(3).accept(this, null);
                    r = new AbsCastExpr(new Location(t, e), (AbsType)t, (AbsExpr)e);
                    return r;
                }
                t = subtrs.get(0).accept(this, visArg);
                //relocate
                t = ((AbsExpr)t).relocate(node.location());
                return t;
            case Lowexpr:
                child_offset = 0; //parenthesis offset
                if (getTerm(subtrs, 0) == LPARENTHESIS) {
                    child_offset = 1;
                }
                if (getTerm(subtrs, 0 + child_offset) == IDENTIFIER) {
                    t = subtrs.get(0 + child_offset).accept(this, visArg);
                    r = subtrs.get(1 + child_offset).accept(this, null); //if this isn't null, we have a function
                    if (r != null) {
                        e = new AbsFunName(node.location(), ((DerLeaf) subtrs.get(0 + child_offset)).symb.lexeme, (AbsArgs) r);
                        return e;
                    } else {
                        return t;
                    }
                }
                if (getTerm(subtrs, 0) == LBRACE) {
                    //statements 1 Stmt, 2 Morestmt
                    s = subtrs.get(1).accept(this, null);
                    Vector<AbsStmt> stmts = new Vector<AbsStmt>();
                    stmts.add((AbsStmt)s);
                    AbsStmts ss = new AbsStmts(new Location(stmts.firstElement(), stmts.lastElement()), stmts);
                    s = subtrs.get(2).accept(this, ss);
                    //expression
                    e = subtrs.get(4).accept(this, null);
                    //declarations
                    AbsDecls dcls = new AbsDecls(node.location(), new Vector<AbsDecl>());
                    r = subtrs.get(5).accept(this, dcls);   //-> Maybewhere
                    //whole statement
                    t = new AbsStmtExpr(node.location(), (AbsDecls)r, (AbsStmts)s, (AbsExpr)e);
                    return t;
                } else {
                    t = subtrs.get(0 + child_offset).accept(this, visArg);
                    return t;
                }
            case Maybewhere:
                if (nchild == 0) {
                    return visArg;
                } else {
                    l = subtrs.get(1).accept(this, null);
                    Vector<AbsDecl> ad = ((AbsDecls)visArg).decls();
                    ad.add((AbsDecl)l);
                    r = subtrs.get(2).accept(this, new AbsDecls(new Location(ad.firstElement(), ad.lastElement()), ad));
                    return r;
                }
            case Moredecl:
                if (nchild == 0) {
                    return visArg;
                } else {
                    l = subtrs.get(1).accept(this, null);
                    Vector<AbsDecl> ad = ((AbsDecls)visArg).decls();
                    ad.add((AbsDecl)l);
                    r = subtrs.get(2).accept(this, new AbsDecls(new Location(ad.firstElement(), ad.lastElement()), ad));
                    return r;
                }
            case Decl:
                switch(getTerm(subtrs, 0)) {
                    case TYP:
                        a = subtrs.get(1).accept(this, null);
                        e = new AbsTypeDecl(node.location(), ((AbsTypeDecl)a).name, ((AbsTypeDecl)a).type);
                        return e;
                    case VAR:
                        a = subtrs.get(1).accept(this, null);
                        e = new AbsVarDecl(node.location(), ((AbsTypeDecl)a).name, ((AbsTypeDecl)a).type);
                        return e;
                    case FUN:
                        //name
                        String name = ((DerLeaf)subtrs.get(1)).symb.lexeme;
                        //parameters
                        //Use CompDecl for compatibility, transform in the end to ParDecl
                        Vector<AbsCompDecl> acd = new Vector<AbsCompDecl>();
                        a = new AbsCompDecls(node.location(), acd);
                        a = subtrs.get(3).accept(this, a);
                        acd = ((AbsCompDecls)a).compDecls();
                        //type
                        t = subtrs.get(6).accept(this, null);
                        //expression
                        e = subtrs.get(7).accept(this, null);

                        //transform parameters ro ParDecl
                        Vector<AbsParDecl> trapd = new Vector<AbsParDecl>();
                        for (AbsCompDecl tmp : acd) {
                            AbsParDecl tmp1 = new AbsParDecl(tmp.location(), tmp.name, tmp.type);
                            trapd.add(tmp1);
                        }
                        Location tmploc;
                        if (trapd.size() == 0) {
                            tmploc = node.location();
                        } else {
                            tmploc = new Location(trapd.firstElement(), trapd.lastElement());
                        }
                        AbsParDecls trapdd = new AbsParDecls(tmploc, trapd);

                        if (e == null) r = new AbsFunDecl(node.location(), name, trapdd, (AbsType)t);
                        else r = new AbsFunDef(node.location(), name, trapdd, (AbsType)t, (AbsExpr)e);

                        return r;
                }
            case Maybe1expr:
                if (nchild == 0) {
                    return null;
                } else {
                    e = subtrs.get(1).accept(this, null);
                    return e;
                }
            case Maybeidtyp:
                if (nchild == 0) {
                    return visArg;
                } else {
                    s = subtrs.get(0).accept(this, null);
                    AbsCompDecl tmpapd = new AbsCompDecl(s.location(), ((AbsTypeDecl)s).name, ((AbsTypeDecl)s).type);
                    Vector<AbsCompDecl> apd = ((AbsCompDecls)visArg).compDecls();
                    apd.add(tmpapd);
                    a = new AbsCompDecls(node.location(), apd);
                    a = subtrs.get(1).accept(this, a);
                    return a;
                }
            case Idtyp:
                l = subtrs.get(0).accept(this, null);
                r = subtrs.get(2).accept(this, null);
                //need to return something, change object type later
                t = new AbsTypeDecl(node.location(), ((AbsVarName)l).name, (AbsType)r);
                return t;
            case Moreidtyp:
                if (nchild == 0) {
                    return visArg;
                } else {
                    l = subtrs.get(1).accept(this, null);
                    Vector<AbsCompDecl> macdc = ((AbsCompDecls)visArg).compDecls();
                    AbsCompDecl tmpacdc = new AbsCompDecl(l.location(), ((AbsTypeDecl)l).name, ((AbsTypeDecl)l).type);
                    macdc.add(tmpacdc);
                    a = new AbsCompDecls(node.location(), macdc);
                    r = subtrs.get(2).accept(this, a);
                    return r;
                }
            case Idexpr:
                if (nchild == 0) {
                    return visArg;
                }
                if (getTerm(subtrs, 0) == LPARENTHESIS) { //function parameters
                    e = subtrs.get(1).accept(this, visArg);
                    return e;
                }
            case Maybeexpr:
                if (nchild == 0) {
                    return new AbsArgs(node.location(), new Vector<AbsExpr>());
                } else {
                    e = subtrs.get(0).accept(this, null);
                    Vector<AbsExpr> v = new Vector<AbsExpr>();
                    v.add((AbsExpr)e);
                    a = new AbsArgs(node.location(), v);
                    return subtrs.get(1).accept(this, a);
                }
            case Moreexpr:
                if (nchild == 0) {
                    return visArg;
                } else {
                    e = subtrs.get(1).accept(this, null);
                    Vector<AbsExpr> v = ((AbsArgs)visArg).args();
                    v.add((AbsExpr)e);
                    a = new AbsArgs(node.location(), v);
                    return subtrs.get(2).accept(this, a);
                }
            case Stmt:
                //AbsIfStmt
                //AbsExprStmt
                //AbsWhileStmt
                //AbsAssignStmt
                if (nchild == 2) {
                    e = subtrs.get(0).accept(this, visArg);
                    //todo not sure here...
                    r = subtrs.get(1).accept(this, visArg);
                    if (r == null) {
                        return new AbsExprStmt(node.location(), (AbsExpr) e);
                    } else {
                        return new AbsAssignStmt(node.location(), (AbsExpr)e, (AbsExpr)r);
                    }
                }
                switch(getTerm(subtrs, 0)) {
                    case IF:
                        //cond
                        a = subtrs.get(1).accept(this, null);
                        //then body
                        s = subtrs.get(3).accept(this, null);
                        Vector<AbsStmt> thenb = new Vector<AbsStmt>();
                        thenb.add((AbsStmt)s);
                        AbsStmts thenbb = new AbsStmts(new Location(thenb.firstElement(), thenb.lastElement()), thenb);
                        s = subtrs.get(4).accept(this, thenbb);
                        //else body
                        Vector<AbsStmt> elseb = new Vector<AbsStmt>();
                        AbsStmts elsebb = new AbsStmts(node.location(), elseb);
                        t = subtrs.get(5).accept(this, elsebb);
                        r = new AbsIfStmt(node.location(), (AbsExpr)a, (AbsStmts)s, (AbsStmts)t);
                        return r;
                    case WHILE:
                        //cond
                        a = subtrs.get(1).accept(this, null);
                        //body
                        e = subtrs.get(3).accept(this, null);
                        Vector<AbsStmt> wbod = new Vector<AbsStmt>();
                        wbod.add((AbsStmt)e);
                        AbsStmts wbodd = new AbsStmts(new Location(wbod.firstElement(), wbod.lastElement()), wbod);
                        e = subtrs.get(4).accept(this, wbodd);
                        t = new AbsWhileStmt(node.location(), (AbsExpr)a, (AbsStmts)e);
                        return t;
                    default:
                        return null;
                }
            case Maybestmt:
                if (nchild == 0) {
                    return visArg;
                } else {
                    a = subtrs.get(1).accept(this, null);
                    Vector<AbsStmt> ss = ((AbsStmts)visArg).stmts();
                    ss.add((AbsStmt)a);
                    r = new AbsStmts(new Location(ss.firstElement(), ss.lastElement()), ss);
                    t = subtrs.get(2).accept(this, r);
                    return t;
                }
            case Stmtexpr:
                if (nchild == 0) {
                    return visArg;
                } else {
                    t = subtrs.get(1).accept(this, null);
                    return t;
                }
            case Morestmt:
                if (nchild == 0) {
                    return visArg;
                } else {
                    s = subtrs.get(1).accept(this, null);
                    Vector<AbsStmt> stmts = ((AbsStmts)visArg).stmts();
                    stmts.add((AbsStmt)s);
                    a = new AbsStmts(new Location(stmts.firstElement(), stmts.lastElement()), stmts);
                    return subtrs.get(2).accept(this, a);
                }
            //bin expr
            case Orexpr:
            case Andexpr:
            case Relexpr:
            case Addexpr:
            case Mulexpr:
                l = subtrs.get(0).accept(this, visArg);
                t = subtrs.get(1).accept(this, l);
                //relocate
                //t = ((AbsExpr)t).relocate(new Location(l, t));
                return t;
            case Orexpr1:
            case Andexpr1:
            case Relexpr1:
            case Addexpr1:
            case Mulexpr1:
                if (nchild == 0) {
                    return visArg;
                } else {
                    //get that operand
                    String opstring = ((DerLeaf)subtrs.get(0)).symb.token.name();
                    AbsBinExpr.Oper op = AbsBinExpr.Oper.valueOf(opstring);

                    t = subtrs.get(1).accept(this, visArg);

                    //new left side of binary expression (visArg + t)
                    l = new AbsBinExpr(new Location(visArg, t), op, (AbsExpr)visArg, (AbsExpr)t);

                    e = subtrs.get(2).accept(this, l);

                    //relocate
                    //e = ((AbsExpr)e).relocate(new Location(l, e));

                    return e;
                }
            case Unexpr:
                if (getTerm(subtrs, 0) == LBRACKET) { //cast expr
                    t = subtrs.get(1).accept(this, null);
                    e = subtrs.get(3).accept(this, null);
                    r = new AbsCastExpr(new Location(t, e), (AbsType)t, (AbsExpr)e);
                    return r;
                }
                if (getTerm(subtrs, 0) == NEW) {
                    t = subtrs.get(1).accept(this, visArg);
                    return new AbsNewExpr(node.location(), (AbsType)t);
                }
                if (getTerm(subtrs, 0) == DEL) {
                    e = subtrs.get(1).accept(this, visArg);
                    return new AbsDelExpr(node.location(), (AbsExpr)e);
                }
                //if (nchild == 2 && (getTerm(subtrs, 0) == ADD || getTerm(subtrs, 0) == SUB)) {
                if (nchild == 2) {
                    AbsUnExpr.Oper op = AbsUnExpr.Oper.valueOf(getTerm(subtrs, 0).name());
                    t = subtrs.get(1).accept(this, visArg);
                    e = new AbsUnExpr(node.location(), op, (AbsExpr)t);
                    return e;
                } else if (nchild == 1) {
                    t = subtrs.get(0).accept(this, visArg);
                    return t;
                } else {
                    throw new Report.Error(node.location(), "Err: abstr, Unexpr.");
                }
            case Accexpr:
                t = subtrs.get(0).accept(this, visArg);
                e = subtrs.get(1).accept(this, t);
                return e;
            case Accexpr1:
                if (nchild == 0) {
                    return visArg;
                } else {
                    Object acc_frst = subtrs.get(0);
                    if (acc_frst instanceof DerLeaf && (((DerLeaf)acc_frst).symb).token == DOT) {    //{
                        //record
                        t = subtrs.get(1).accept(this, visArg);
                        e = new AbsRecExpr(node.location(), (AbsExpr)visArg, (AbsVarName)t);
                        r = subtrs.get(2).accept(this, e);
                        return r;
                    } else if (acc_frst instanceof DerLeaf && (((DerLeaf)acc_frst).symb).token == LBRACKET) {    //{
                        //array
                        t = subtrs.get(1).accept(this, visArg);
                        e = new AbsArrExpr(node.location(), (AbsExpr)visArg, (AbsExpr)t);
                        r = subtrs.get(3).accept(this, e);
                        return r;
                    }
                }
            case Type:
                if (nchild == 0) {
                    throw new Report.Error("Err: abstr, Type");
                } else {
                    switch (getTerm(subtrs, 0)) {
                        case VOID:
                        case BOOL:
                        case CHAR:
                        case INT:
                            String type_string = ((DerLeaf)subtrs.get(0)).symb.token.name();
                            AbsAtomType.Type ty = AbsAtomType.Type.valueOf(type_string);
                            return new AbsAtomType(node.location(), ty);
                        case ARR:
                            e = subtrs.get(2).accept(this, visArg);
                            t = subtrs.get(4).accept(this, visArg);
                            r = new AbsArrType(node.location(), (AbsExpr)e, (AbsType)t);
                            return r;
                        case REC:
                            l = subtrs.get(2).accept(this, null);
                            Vector<AbsCompDecl> acdc = new Vector<AbsCompDecl>();
                            //type problem...
                            AbsCompDecl tmpacdc = new AbsCompDecl(l.location(), ((AbsTypeDecl)l).name, ((AbsTypeDecl)l).type);
                            acdc.add(tmpacdc);
                            a = new AbsCompDecls(node.location(), acdc);
                            t = subtrs.get(3).accept(this, a);
                            r = new AbsRecType(node.location(), (AbsCompDecls)t);
                            return r;
                        case PTR:
                            t = subtrs.get(1).accept(this, null);
                            r = new AbsPtrType(node.location(), (AbsType)t);
                            return r;
                        default:
                            return new AbsTypeName(node.location(), ((DerLeaf)subtrs.get(0)).symb.lexeme);
                    }
                }
            default:
                throw new Report.Error(node.location(), "abstr, switch statement fail (default case)");
                /*
                if (subtrs.size() == 0) {
                    return null;
                } else {
                    return subtrs.get(0).accept(this, visArg);
                }
                */
        }
    }


}
