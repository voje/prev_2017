package compiler.phases.frames;

import java.util.*;

import common.utils.FrameHelper;
import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;
import compiler.phases.seman.*;
import compiler.phases.seman.type.*;

public class FrameEvaluator extends AbsFullVisitor<Object, Long> {

    //always building the last element in the arraylist
    //add a frameBuilder when entering funDef; remove when exiting funDef;
    private ArrayList<FrameHelper> frameBuilder = new ArrayList<FrameHelper>();
    public final long staticLinkSize = 8;

    @Override
    public Object visit(AbsFunDef funDef, Long visArg) {
        Long depth = visArg + 1; //increment static link depth (first function has depth 1)
        Label lbl;
        if (visArg == 0) {
            lbl = new Label(funDef.name); //global scope
        } else {
            lbl = new Label(); //anonymous
        }

        FrameHelper fh = new FrameHelper(lbl, depth.intValue());
        fh.paramOffset = staticLinkSize;
        frameBuilder.add(fh);

        //uses fh.paramOffset
        funDef.parDecls.accept(this, visArg);

        funDef.type.accept(this, visArg);

        funDef.value.accept(this, depth);
        //frameBuilder has accumulated frame size during funDef.value visit
        FrameHelper frhlp = frameBuilder.get(frameBuilder.size()-1);
        frameBuilder.remove(frameBuilder.size()-1); //pop

        Frame frm = new Frame(lbl, frhlp.depth, frhlp.localsSize, frhlp.maxArgsSize + staticLinkSize);
        Frames.frames.put(funDef, frm);
        return null;
    }

    @Override
    public Object visit(AbsVarDecl varDecl, Long visArg) {
        Access ac;
        SemType st = SemAn.descType().get(varDecl.type);
        if (varDecl.type instanceof AbsRecType) {
            //set offsets for components
            ((AbsRecType)varDecl.type).accept(this, visArg); //go set offsets of components
        }
        if (visArg == 0) {
            //global variables in Data
            ac = new AbsAccess(st.size(), new Label(varDecl.name));
        } else {
            FrameHelper fh = frameBuilder.get(frameBuilder.size()-1); //get current function frame
            long currentLocalsSize = fh.localsSize;
            int offset = -8 - (int)currentLocalsSize; //first local at FP-8-0
            currentLocalsSize = currentLocalsSize + st.size();
            fh.localsSize = currentLocalsSize;

            ac = new RelAccess(st.size(), offset, visArg.intValue());
            //todo check if we always get depth in visArg.. we could use frameHelper instead
        }
        Frames.accesses.put(varDecl, ac);
        //varDecl.type.accept(this, visArg);
        return null;
    }

    //If we're inside a funDef (last frame in frameBuilder):
    //find the "largest" funName call (with the most arguments)
    //so we can reserver enough space for arguments for all of the nested function calls.
    @Override
    public Object visit(AbsFunName funName, Long visArg) {
        //if we're not inside a funDef, skip this
        if (frameBuilder.size() == 0) {
            return null;
        }
        FrameHelper fh = frameBuilder.get(frameBuilder.size()-1);
        fh.countingArgs = true;

        //get called function's parameter list (calculate argSize)
        //we have frames for funDef, not for funDecl
        AbsFunDecl ad = (AbsFunDecl)SemAn.declAt().get(funName);
        AbsParDecls pds = ad.parDecls;
        pds.accept(this, visArg);

        fh.updateMaxSize(); //framehelper will end up with the largest funname args size

        fh.countingArgs = false;
        return null;
    }

    //when going through parameter declarations of a called function
    @Override
    public Object visit(AbsParDecl parDecl, Long visArg) {
        if (frameBuilder.size() != 0) {
            FrameHelper fh = frameBuilder.get(frameBuilder.size()-1);
            if (fh.countingArgs) {
                SemType st = SemAn.descType().get(parDecl.type);
                fh.argsSize += st.size();
            } else {
                //parameter access
                SemType st = SemAn.descType().get(parDecl.type);
                Access ac = new RelAccess(st.size(), fh.paramOffset, visArg.intValue());
                Frames.accesses.put(parDecl, ac);
                fh.paramOffset = fh.paramOffset + st.size();
            }
        }
        //parDecl.type.accept(this, visArg);
        return null;
    }

    //let each component get its own relAccess
    @Override
    public Object visit(AbsCompDecl compDecl, Long visArg) {
        Long offset = visArg;
        SemType st = SemAn.descType().get(compDecl.type);
        Access ac = new RelAccess(st.size(), offset, 0); //record components depth = 0
        Frames.accesses.put(compDecl, ac);
        offset += st.size();
        //compDecl.type.accept(this, visArg);
        return offset;
    }
    @Override
    public Object visit(AbsCompDecls compDecls, Long visArg) {
        Long offset = 0l;
        for (AbsCompDecl compDecl : compDecls.compDecls())
            offset = (Long)compDecl.accept(this, offset);
        return null;
    }


    //------------------------------

    @Override
    public Object visit(AbsAtomType atomType, Long visArg) {
        return null;
    }

    @Override
    public Object visit(AbsArgs args, Long visArg) {
        for (AbsExpr arg : args.args())
            arg.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsStmtExpr stmtExpr, Long visArg) {
        stmtExpr.decls.accept(this, visArg);
        stmtExpr.stmts.accept(this, visArg);
        stmtExpr.expr.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsArrExpr arrExpr, Long visArg) {
        arrExpr.array.accept(this, visArg);
        arrExpr.index.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsArrType arrType, Long visArg) {
        arrType.len.accept(this, visArg);
        arrType.elemType.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsAssignStmt assignStmt, Long visArg) {
        assignStmt.dst.accept(this, visArg);
        assignStmt.src.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsAtomExpr atomExpr, Long visArg) {
        return null;
    }

    @Override
    public Object visit(AbsBinExpr binExpr, Long visArg) {
        binExpr.fstExpr.accept(this, visArg);
        binExpr.sndExpr.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsCastExpr castExpr, Long visArg) {
        castExpr.type.accept(this, visArg);
        castExpr.expr.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsDecls decls, Long visArg) {
        for (AbsDecl decl : decls.decls())
            decl.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsDelExpr delExpr, Long visArg) {
        delExpr.expr.accept(this, visArg);
        return null;
    }

    @Override
    public  Object visit(AbsExprStmt exprStmt, Long visArg) {
        exprStmt.expr.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsFunDecl funDecl, Long visArg) {
        funDecl.parDecls.accept(this, visArg);
        funDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsIfStmt ifStmt, Long visArg) {
        ifStmt.cond.accept(this, visArg);
        ifStmt.thenBody.accept(this, visArg);
        ifStmt.elseBody.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsNewExpr newExpr, Long visArg) {
        newExpr.type.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsParDecls parDecls, Long visArg) {
        for (AbsParDecl parDecl : parDecls.parDecls())
            parDecl.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsPtrType ptrType, Long visArg) {
        ptrType.subType.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsRecExpr recExpr, Long visArg) {
        recExpr.record.accept(this, visArg);
        recExpr.comp.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsRecType recType, Long visArg) {
        recType.compDecls.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsStmts stmts, Long visArg) {
        for (AbsStmt stmt : stmts.stmts())
            stmt.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsTypeDecl typeDecl, Long visArg) {
        typeDecl.type.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsTypeName typeName, Long visArg) {
        return null;
    }

    @Override
    public Object visit(AbsUnExpr unExpr, Long visArg) {
        unExpr.subExpr.accept(this, visArg);
        return null;
    }

    @Override
    public Object visit(AbsVarName varName, Long visArg) {
        return null;
    }

    @Override
    public Object visit(AbsWhileStmt whileStmt, Long visArg) {
        whileStmt.cond.accept(this, visArg);
        whileStmt.body.accept(this, visArg);
        return null;
    }

}
