package compiler.phases.liveness;

import compiler.phases.Phase;
import compiler.phases.asmgen.AsmGen;
import compiler.phases.asmgen.AsmInstr;
import compiler.phases.lincode.Fragment;

import java.util.HashMap;
import java.util.Vector;

/**
 * Created by kristjan on 24.5.2017.
 */
public class Liveness extends Phase {

    public static boolean debug = false;

    public static Vector<InterGraph> igraph = null;

    public Liveness() {
        super("liveness");
    }

    public static void gen_igraphs() {
        Vector<Vector<Vector<AsmInstr>>> vec = AsmGen.instructions();
        igraph = new Vector<InterGraph>();
        igraph.clear();
        //build a graph for each fragment
        for (int i=0; i<vec.size(); i++) {                      //loop through fragments
            Vector<Vector<AsmInstr>> fragment = vec.get(i);
            Vector<AsmInstr> finstr = new Vector<AsmInstr>();          //fragment instructions (easier debugging)
            for (int j=0; j<fragment.size(); j++) {             //loop through trees
                Vector<AsmInstr> canon = fragment.get(j);
                if (canon.size() == 0) continue;
                for (AsmInstr instr : canon) {                  //loop through instructions
                    finstr.add(instr);
                }
            }
            igraph.add(new InterGraph(finstr, i));
            finstr.clear();
        } //foreach fragment
    }

    public static void debug_print() {
        for (int i=0; i<igraph.size(); i++) {
            System.out.println("Fragment " + i);
            igraph.get(i).print_out_live();
            igraph.get(i).print_out_igraph();
            System.out.println();
        }
    }

    @Override
    public void close() {
        if (this.debug) {
            for (int i=0; i<igraph.size(); i++) {
                System.out.println("Fragment " + i);
                igraph.get(i).print_out_live();
                igraph.get(i).print_out_igraph();
                System.out.println();
            }
        } //if(this.debug)
        super.close();
    }
}
