package compiler.phases.liveness;

import compiler.phases.asmgen.AsmInstr;
import compiler.phases.frames.Label;
import compiler.phases.frames.Temp;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

/**
 * Created by kristjan on 24.5.2017.
 */
public class InterNode {
    private AsmInstr instr;
    public int id;
    public HashSet<InterNode> pred;
    public HashSet<InterNode> succ;

    public Vector<Temp> uses;
    public Vector<Temp> defs;
    public Vector<Label> jumps;

    public HashSet<Temp> in;
    public HashSet<Temp> out;

    public InterNode (AsmInstr instr, int id) {
        this.id = id;
        this.instr = instr;

        pred = new HashSet<InterNode>();
        succ = new HashSet<InterNode>();

        this.uses = instr.uses();
        this.defs = instr.defs();
        this.jumps = instr.jumps();

        this.in = new HashSet<Temp>();
        this.out = new HashSet<Temp>();
    }

    public InterNode in_out_copy() {
        //leave pred, succ, uses, etc we only need in and out
        InterNode nnode = new InterNode(this.instr, this.id);
        nnode.in = new HashSet<Temp>(this.in);
        nnode.out = new HashSet<Temp>(this.out);
        return nnode;
    }

    private String set_to_string(HashSet<InterNode> set) {
        String out = "( ";
        for (InterNode in : set) {
            out += in.id;
            out += ", ";
        }
        out += ")";
        return out;
    }

    private String set_to_string_live(HashSet<Temp> set) {
        String out = "( ";
        for (Temp t : set) {
            out += t.temp;
            out += ", ";
        }
        out += ")";
        return out;
    }

    public String toString() {
        return String.format("%3d| pred: %s, succ: %s", id, set_to_string(pred), set_to_string(succ));
    }

    public String toStringLive() {
        return String.format("%3d| %-20s %s", id, set_to_string_live(in), set_to_string_live(out));
    }
}
