package compiler.phases.liveness;

import common.report.Report;
import compiler.phases.asmgen.AsmGen;
import compiler.phases.asmgen.AsmInstr;
import compiler.phases.asmgen.AsmLABEL;
import compiler.phases.asmgen.AsmOPER;
import compiler.phases.frames.Label;
import compiler.phases.frames.Temp;
import compiler.phases.lincode.CodeFragment;
import compiler.phases.lincode.DataFragment;
import compiler.phases.lincode.Fragment;
import compiler.phases.lincode.LinCode;
import compiler.phases.regalloc.Regalloc;


import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Vector;

import static compiler.phases.imcgen.ImcGen.FP;
import static compiler.phases.imcgen.ImcGen.SP;
import static compiler.phases.imcgen.ImcGen.rR;
import static compiler.phases.regalloc.Regalloc.K;

/**
 * Created by kristjan on 24.5.2017.
 */
public class InterGraph {

    public int id = -1; //maps to corresponding fragment

    public HashMap<InterNode, AsmInstr> node_to_instr = new HashMap<InterNode, AsmInstr>();
    public Vector<InterNode> nodes = new Vector<InterNode>();
    public Vector<InterNode> prev_nodes; //compare while iterating

    //actual inter graph
    public HashSet<Node> ig = new HashSet<Node>(); //the actual interference graph
    public HashMap<Temp, Node> tn = new HashMap<Temp, Node>(); //for quick search by Temp in interference_graph

    //graph coloring
    private boolean change = false; //boolean for looping through graph coloring
    private Vector<Node> removed = new Vector<Node>();
    public HashMap<Temp, Integer> color_map = new HashMap<Temp, Integer>();

    public InterGraph(Vector<AsmInstr> instr, int id) {
        this.id = id;

        //add special registers to color_map
        //color_map.put(SP, 254);
        //color_map.put(FP, 253);
        //color_map.put(rR, 252); //adding rR messes up coloring... what??

        init_nodes(instr);
        update_succ_pred();
        calculate_interference();
        helper_add_unique_nodes();
        calculate_ig_vertices();
        //print_out_live();
        //print_out_igraph();

        //igraph should have all used temps listed in the left column
    }

    private void copy_nodes() {
        prev_nodes = new Vector<InterNode>();
        for (InterNode in : nodes) {
            prev_nodes.add(in.in_out_copy());
        }
    }

    private void calculate_ig_vertices() {
        //iterate through instructions and for each: add ins and outs as neighbours
        for (InterNode in : nodes) {
            for ( Temp t : in.in ) {
                Node n = tn.get(t);
                for (Temp tt : in.in){
                    if (!tt.equals(t)) {
                        n.add_edge_to(tn, tt);
                    }
                }
            }
        }
    }

    private void helper_add_unique_nodes() {
        //after calculating interferences, create a node for each tmp and add it

        //ommit special registers!!!

        HashSet<Temp> tmptmp = new HashSet<Temp>();
        for (InterNode in : nodes) {
            for (Temp t : in.in) {
                //if (t.equals(SP) || t.equals(FP) || t.equals(rR)) continue;
                tmptmp.add(t);
            }
            for (Temp t : in.out) {
                //if (t.equals(SP) || t.equals(FP) || t.equals(rR)) continue;
                tmptmp.add(t);
            }
        }
        //we have unique ins and outs, add them to interference_graph
        for (Temp t : tmptmp) {
            Node n = new Node(t);
            ig.add(n);
            tn.put(t, n);
        }
    }

    private boolean compare_nodes() {
        //returns true if prev_nodes and nodes contain the same in and out vectors
        if (prev_nodes == null) return false;
        for(int i=0; i<nodes.size(); i++) {
            InterNode n1 = nodes.get(i);
            InterNode n2 = prev_nodes.get(i);
            if (!(n1.in.equals(n2.in)))
                return false;
            if (!(n1.out.equals(n2.out)))
                return false;
        }
        return true;
    }

    private void calculate_interference() {
        //reverse nodes
        Collections.reverse(nodes);
        //last node(now first) was jump out of function, one before last saves RV.
        //add out dependency to node which saves to RV

        int nid = 2; //for function calls, skip the last nodes (saving RV TO FP) (no new registers)
        if (id == 0) nid = 0;
        InterNode n = nodes.get(nid);
        n.out.add(n.defs.get(0));

        int info = 0;
        while (!compare_nodes()) {  //keep looping until there's nothing to change
            //System.out.printf("calculate_interference: %d%n", info++);
            copy_nodes();
            for (InterNode in : nodes) {
                //in.in = in.use U (in.out - in.def)
                for (Temp t : in.uses) { //in.use
                    if (t.equals(SP) || t.equals(FP) || t.equals(rR)) continue;     //ommit special registers
                    in.in.add(t);
                }
                HashSet<Temp> out_minus_def = new HashSet<Temp>();
                for (Temp t : in.out) //in.out
                    out_minus_def.add(t);
                for (Temp t : in.defs) { //-in.def
                    if (t.equals(SP) || t.equals(FP) || t.equals(rR)) continue;
                    out_minus_def.remove(t);
                }
                for (Temp t : out_minus_def) //add to in.in
                    in.in.add(t);

                //for all successors suc of in:
                //in.out = suc.in
                for (InterNode suc : in.succ) {
                    for (Temp t : suc.in) {
                        in.out.add(t);
                    }
                }

            } //nodes for loop
        } //while
    }

    private void add_node(InterNode in, AsmInstr instr) {
        nodes.add(in);
        node_to_instr.put(in, instr);
    }

    private InterNode node_by_label(Label lab) {
        for (InterNode in : nodes) {
            AsmInstr ai = node_to_instr.get(in);
            if (ai instanceof AsmLABEL && ((AsmLABEL)ai).toString().equals(lab.name)) {
                return in;
            }
        }
        return null;
    }

    private void init_nodes(Vector<AsmInstr> instructions) {
        //full nodes vector and create mappings between nodes and instructions
        for (int i = 0; i < instructions.size(); i++) {
            AsmInstr instr = instructions.get(i);
            InterNode in = new InterNode(instr, i);
            add_node(in, instr);
        }
    }

    public void print_out_live() {
        System.out.println("########################################################");
        System.out.println("########################################################");
        System.out.printf("   | %-20s %s%n", "in", "out");
        System.out.println("--------------------------------------");
        for (InterNode in : nodes) {
            System.out.println(in.toStringLive());
        }
    }

    public void print_out_igraph() {
        System.out.println("iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
        System.out.println("Temp| Neighbours");
        System.out.println("--------------------");
        for (Node n : ig){
            System.out.println(n.toString());
        }
    }

    private void update_succ_pred () {
        //foreach node N, add successor S + prepend predecessor N to S
        for (int i=0; i<nodes.size()-1; i++) {
            InterNode ths = nodes.get(i);
            InterNode nxt = nodes.get(i+1);
            ths.succ.add(nxt);
            nxt.pred.add(ths);

            //handle jumps TODO if you add another Asm jump command, handle it
            //current commands (JMP, BNZ)
            AsmInstr instr = node_to_instr.get(ths);
            if (instr.jumps() != null) {
                for (int j=0; j<instr.jumps().size(); j++) {
                    Label jl = instr.jumps().get(j);
                    InterNode jnode = node_by_label(jl);
                    if (jnode != null) { //if it's null, we're jumping to next fragment
                        ths.succ.add(jnode);
                        jnode.pred.add(ths);
                    }
                }
            }
        }
    }

    private int h_count_neigh(Node n) {
        int i=0;
        for (Temp t : n.temp_neigh) {
            Node tmp_n = tn.get(t);
            if (!tmp_n.removed) i++;    //if the neigh wasn't removed, increment
        }
        return i;
    }

    private void h_simplify() {
        for (Node n : ig) {
            if (n.removed) continue;
            if (h_count_neigh(n) < K) {
                n.removed = true;
                removed.add(n);
                change = true;
            }
        }
    }

    private void h_spill() {
        for (Node n : ig) {
            if (n.removed) continue;
            if (h_count_neigh(n) >= K) {
                n.possible_spill = true;
                n.removed = true;
                removed.add(n);
                change = true;
                break;  //spill one; try to simplify again
            }
        }
    }

    private boolean h_color_node(Node n) {
        //try all colors
        for (int color=0; color<K; color++) {
            boolean coloring_possible = true;
            for (Temp t : n.temp_neigh) {
                Node nei = tn.get(t);
                if (nei.color == color) {
                    coloring_possible = false;
                    break;
                }
            }
            if (coloring_possible) {
                n.removed = false;
                n.color = color;
                color_map.put(n.temp, color);
                return true;
            }
        }
        return false;
    }

    private void fix_spills(Vector<Node> spills) {
        //for each spill in spills:
        //loop through fragment code
        //after def, store in temp variables
        //before use, load from temp variables

        //discare data fragment
        Fragment fragment = LinCode.get_fragment(this.id);
        if (fragment instanceof DataFragment) return;
        CodeFragment frag = (CodeFragment)fragment;

        Vector<Vector<AsmInstr>> code = AsmGen.fragment_instructions(this.id);
        int spill_offset = 0;
        for (Node spill : spills) {
            spill_offset -= 8;
            Temp tspill = spill.temp;
            for (int i=0; i<code.size(); i++) {                 //canon trees
                for (int j=0; j<code.get(i).size(); j++) {      //instructions
                    AsmInstr instr = code.get(i).get(j);
                    boolean inDefs = false;
                    boolean inUses = false;
                    for (Temp t : instr.defs()) {
                        if (t.equals(tspill)) inDefs = true;
                    }
                    for (Temp t : instr.uses()) {
                        if (t.equals(tspill)) inUses = true;
                    }
                    if (inDefs && inUses) {
                        //add load before this instruction
                        Vector<Temp> defs = new Vector<Temp>(); defs.add(tspill);
                        Vector<Temp> uses = new Vector<Temp>(); uses.add(SP);
                        int offset = -8 - (int)frag.frame.locsSize - 16 + spill_offset;
                        AsmOPER load = new AsmOPER("LDO, `d0, `s0, " + offset, uses, defs, null);
                        code.get(i).insertElementAt(load, j);

                        //add store after this instruction
                        uses = new Vector<Temp>(); uses.add(tspill); uses.add(SP);
                        AsmOPER store = new AsmOPER("STO `s0, `s1, " + Integer.toString(offset), uses, null, null);
                        code.get(i).insertElementAt(store, j+2);

                        j = j+2;
                    } else if (inDefs) {
                        //add store after this instruction
                        Vector<Temp> uses = new Vector<Temp>(); uses.add(tspill); uses.add(SP);
                        int offset = -8 - (int)frag.frame.locsSize - 16 + spill_offset;
                        AsmOPER store = new AsmOPER("STO `s0, `s1, " + Integer.toString(offset), uses, null, null);
                        code.get(i).insertElementAt(store, j+1);
                        j++; //don't eval next instruction (the newly added store)
                    } else if (inUses) {
                        //add load before this instruction
                        Vector<Temp> defs = new Vector<Temp>(); defs.add(tspill);
                        Vector<Temp> uses = new Vector<Temp>(); uses.add(SP);
                        int offset = -8 - (int)frag.frame.locsSize - 16 + spill_offset;
                        AsmOPER load = new AsmOPER("LDO, `d0, `s0, " + offset, uses, defs, null);
                        code.get(i).insertElementAt(load, j);
                        j++; //don't visit this instruction again
                    }
                }
            }
        }
    }

    //graph coloring
    public int color_me() {
        Vector<Node> spills = new Vector<Node>();

        //remove nodes 1 by 1 if n < K
        //flag for spill if n >= K
        //return nodes (always try to color one of available colors)
        //if it fails, rewrite code (handle spill) and redo the whole thing

        //Node n = igraph.ig.get(i)
        //Temp first_neigh = n.temp_neigh.get(0)
        if (ig.size() == 0) return -1;

        //first loop and keep simplifying and spilling
        //fill the removed vector
        do {
            change = false;
            h_simplify();
            if (change) continue;   //keep simplifying
            h_spill();
        } while (change);

        //loop removed in reverse order and color the nodes.
        //confirm spills
        Collections.reverse(removed);
        for (Node n : removed) {
            boolean status = h_color_node(n);
            if (!status) {
                //we should be looking at a spill that we couldn't color
                if (!n.possible_spill) throw new Report.Error("We couldn't return a non-spilled node.");
                n.confirmed_spill = true;
                n.color = -2;   //for easire print
                spills.add(n);
            }
        }

        //System.out.println("Confirmed spill count: " + spills.size());
        if (Regalloc.fix_spills) fix_spills(spills);

        return spills.size(); //if return != 0; we need to repeat interference and coloring
    }

}
