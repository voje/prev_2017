package compiler.phases.liveness;

import compiler.phases.frames.Temp;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by kristjan on 24.5.2017.
 * This node represents the actial register + all connections to other registers
 * that are alive with this register
 */
public class Node {
    public Temp temp;
    public HashSet<Temp> temp_neigh; //might want to upgrade these to Node later

    //variables for graph coloring
    public boolean removed = false;
    public boolean possible_spill = false;
    public boolean confirmed_spill = false;
    public int color = -1;

    public Node (Temp t) {
        this.temp = t;
        this.temp_neigh = new HashSet<Temp>();
    }

    public void add_edge_to(HashMap<Temp, Node>tn, Temp t) {
        //we assume that InterGraph.interference_graph contains all nodes already
        //add neighbour to this node, add this node as n's neighbour
        this.temp_neigh.add(t);
        Node n = tn.get(t);
        n.temp_neigh.add(this.temp);
    }

    private String edges_to_string() {
        String out = "( ";
        for (Temp t : temp_neigh) {
            out += t.temp;
            out += ", ";
        }
        out += ")";
        return out;
    }

    public String toString() {
        String tmp = "| color: " + this.color;
        return String.format("%3s | %15s %5s", this.temp.temp, edges_to_string(), tmp);
    }
}
