package compiler.phases.mmsgen;

import common.report.Report;
import compiler.phases.Phase;
import compiler.phases.asmgen.AsmGen;
import compiler.phases.asmgen.AsmInstr;
import compiler.phases.frames.Frame;
import compiler.phases.lincode.CodeFragment;
import compiler.phases.lincode.DataFragment;
import compiler.phases.lincode.Fragment;
import compiler.phases.lincode.LinCode;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Created by kristjan on 4.7.2017.
 */
public class MmsGen extends Phase {

    private List<String> lines;

    private final String data_frg_anchor = "#data_fragments_anchor";
    private final String code_frg_anchor = "#code_fragments_anchor";
    private String pth;
    private String outpth;

    public MmsGen(String pth) {
        super("MmsGen");
        this.pth = pth;
        generate_outpath();
    }

    private void generate_outpath () {
        String[] tmp;
        String srcName = compiler.Main.cmdLineArgValue("--src-file-name");
        tmp = srcName.split("/");
        srcName = tmp[tmp.length-1];
        tmp = srcName.split("\\.");
        srcName = tmp[0] + ".mms";
        tmp = pth.split("/");
        outpth = "";
        for (int i=0; i<tmp.length-1; i++) {
           outpth = outpth + tmp[i] + "/";
        }
        outpth = outpth + srcName;
    }

    public void generate () {
        //reads template file and fills it with:
        //data_fragments
        //code_fragments
        //print_current_dir();
        try {
            lines = Files.readAllLines(Paths.get(pth), Charset.defaultCharset());
            insert_data_fragments();
            insert_code_fragments();
        } catch (IOException e) {
            throw new Report.Error("Failed reading template file: " + e.getMessage());
        }

        //write to file outpth
        try{
            PrintWriter writer = new PrintWriter(outpth, "UTF-8");
            for (String s : lines) {
                writer.println(s);
            }
            writer.close();
            System.out.printf("Written to file: %s%n", this.outpth);
        } catch (IOException e) {
            throw new Report.Error("Failed writing to file: " + e.getMessage());
        }

    }

    private int find_line (String matching_string) {
        //finds a line matching a string; there are no newlines in lines vector
        for (int i=0; i<lines.size(); i++) {
            if (lines.get(i).contains(matching_string)) return i;
        }
        return -1;
    }

    private ArrayList<String> generate_data_fragments () {
        ArrayList<String> lst = new ArrayList<String>();
        for (Fragment frg : LinCode.fragments()) {
            if (frg instanceof DataFragment) {
                lst.add( frg.toAsm() );
            }
        }
        return lst;
    }

    private ArrayList<String> generate_code_fragments () {
        ArrayList<String> lst = new ArrayList<String>();
        for (Fragment frg : LinCode.fragments()) {
            if (frg instanceof CodeFragment) {
                Frame frame = ((CodeFragment)frg).frame;
                lst.add( frg.toAsm() );
            }
        }
        return lst;
    }

    private void insert_data_fragments () {
        int lidx = find_line(data_frg_anchor);
        if (lidx == -1) return;
        lines.remove(lidx);
        //insert lines from last to first into location lidx
        ArrayList<String> lst = generate_data_fragments();
        for (int i=lst.size()-1; i>=0; i--) {
            lines.add(lidx, lst.get(i));
        }
    }

    private void insert_code_fragments () {
        int lidx = find_line(code_frg_anchor);
        if (lidx == -1) return;
        lines.remove(lidx);
        //insert lines from last to first into location lidx
        ArrayList<String> lst = generate_code_fragments();
        for (int i=lst.size()-1; i>=0; i--) {
            lines.add(lidx, lst.get(i));
        }
    }

    public static void print_current_dir () {
        final String dir = System.getProperty("user.dir");
        System.out.println("current dir = " + dir);
    }
}

