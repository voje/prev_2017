package compiler.phases.lexan;

import java.io.*;
import common.report.*;
import compiler.phases.*;

/**
 * Lexical analysis.
 * 
 * @author sliva
 *
 */
public class LexAn extends Phase {

    private int line, column;
    private boolean reached_eof;

    /**
     * The name of the source file.
     */
    private final String srcFileName;

    /**
     * The source file reader.
     */
    private final BufferedReader srcFile;

    /**
     * Constructs a new lexical analysis phase.
     */
    public LexAn() {
        super("lexan");
        srcFileName = compiler.Main.cmdLineArgValue("--src-file-name");
        try {
            srcFile = new BufferedReader(new FileReader(srcFileName));
        } catch (IOException ___) {
            throw new Report.Error("Cannot open source file '" + srcFileName + "'.");
        }
        line = 1;
        column = 0;
    }

    /**
     * The lexer.
     * <p>
     * This method returns the next symbol from the source file. To perform the
     * lexical analysis of the entire source file, this method must be called
     * until it returns EOF. This method calls {@link #lexify()}, logs its
     * result if requested, and returns it.
     *
     * @return The next symbol from the source file.
     */
    public Symbol lexer() {
        Symbol symb = null;
        try {
            symb = lexify();
        } catch (IOException e) {
            e.printStackTrace();
        }
        symb.log(logger);
        return symb;
    }

    @Override
    public void close() {
        try {
            srcFile.close();
        } catch (IOException ___) {
            Report.warning("Cannot close source file '" + this.srcFileName + "'.");
        }
        super.close();
    }

    // --- LEXER ---

    /**
     * Performs the lexical analysis of the source file.
     * <p>
     * This method returns the next symbol from the source file. To perform the
     * lexical analysis of the entire source file, this method must be called
     * until it returns EOF.
     *
     * @return The next symbol from the source file or EOF if no symbol is
     * available any more.
     */
    private Symbol lexify() throws IOException {
        String lexeme = "unknown";

        char nxc = readChar();

        //spaces, tabs, newlines, cr, comments (skip)
        while (nxc == ' ' || nxc == '\t' || nxc == '\n' || nxc == '\r' || nxc == '#') {
            if (nxc == '#') {
                nxc = readChar();
                while (!reached_eof && nxc != '\n') {
                    nxc = readChar();
                }
            }
            nxc = readChar();
        }

        //EOF
        if ( reached_eof ) {
            return new Symbol(Term.EOF, "EOF", new Location(line, column));
        }


        Location startLoc = new Location(line, column);

        //char literal
        if (nxc == '\'') {
            nxc = readChar();
            if (nxc >= 32 && nxc <= 126) {
                lexeme = "\'" + String.valueOf(nxc) + "\'";
                if (readChar() == '\'') {
                    return new Symbol(Term.CHARCONST, lexeme, new Location(startLoc, new Location(line, column)));
                } else {
                    throw new Report.Error(new Location(line, column), "Lex: character length exceeded.");
                }
            } else {
                throw new Report.Error(new Location(line, column), "Lex: invalid character.");
            }
        }

        //integer literal
        if ( isNumeric(nxc) ) {
            String num = "" + nxc;
            while ( isNumeric(peekChar()) ) {
                num += readChar();
            }
            return new Symbol(Term.INTCONST, num, new Location(startLoc, new Location(line, column)));
        }

        // This segment read + or - and checks if it's a part of an integer constant!!!
        // what if we get 2+3; we would get 2 int constants instead of int + int
        // Drop the whole thing and read +/- in the symbols switch.
        /*
        if (nxc == '+' || nxc == '-') {
            Symbol ret = new Symbol(Term.ADD, String.valueOf(nxc), startLoc);
            if ( isNumeric(peekChar()) ) {
                String signed = "" + nxc;
                while ( isNumeric(peekChar()) ) {
                    signed += readChar();
                }
                return new Symbol(Term.INTCONST, signed, new Location(startLoc, new Location(line, column)));
            } else {
                switch (nxc) {
                    case '+':
                        return new Symbol(Term.ADD, String.valueOf(nxc), startLoc);
                    case '-':
                        return new Symbol(Term.SUB, String.valueOf(nxc), startLoc);
                    default:
                        throw new Report.Error("We shouldn't be here.");
                }
            }
        }
        */

        //symbols (minus +, -)
        switch (nxc) {
            case '!': {
                if (peekChar() == '=')
                    return new Symbol(Term.NEQ, String.valueOf("" + nxc + readChar()), new Location(startLoc, new Location(line, column)));
                else
                    return new Symbol(Term.NOT, String.valueOf(nxc), new Location(startLoc, startLoc));
            }
            case '|':
                return new Symbol(Term.IOR, String.valueOf(nxc), new Location(startLoc, startLoc));
            case '+':
                return new Symbol(Term.ADD, String.valueOf(nxc), new Location(startLoc, startLoc));
            case '-':
                return new Symbol(Term.SUB, String.valueOf(nxc), new Location(startLoc, startLoc));
            case '^':
                return new Symbol(Term.XOR, String.valueOf(nxc), new Location(startLoc, startLoc));
            case '&':
                return new Symbol(Term.AND, String.valueOf(nxc), new Location(startLoc, startLoc));
            case '=': {
                if (peekChar() == '=')
                    return new Symbol(Term.EQU, String.valueOf("" + nxc + readChar()), new Location(startLoc, new Location(line, column)));
                else
                    return new Symbol(Term.ASSIGN, String.valueOf(nxc), new Location(startLoc, startLoc));
            }
            case '<':
                if (peekChar() == '=')
                    return new Symbol(Term.LEQ, String.valueOf("" + nxc + readChar()), new Location(startLoc, new Location(line, column)));
                else
                    return new Symbol(Term.LTH, String.valueOf(nxc), new Location(startLoc, startLoc));
            case '>':
                if (peekChar() == '=')
                    return new Symbol(Term.GEQ, String.valueOf("" + nxc + readChar()), new Location(startLoc, new Location(line, column)));
                else
                    return new Symbol(Term.GTH, String.valueOf(nxc), new Location(startLoc, startLoc));
            case '*':
                return new Symbol(Term.MUL, String.valueOf(nxc), new Location(startLoc, startLoc));
            case '/':
                return new Symbol(Term.DIV, String.valueOf(nxc), new Location(startLoc, startLoc));
            case '%':
                return new Symbol(Term.MOD, String.valueOf(nxc), new Location(startLoc, startLoc));
            case '@':
                return new Symbol(Term.VAL, String.valueOf(nxc), new Location(startLoc, startLoc));
            case '$':
                return new Symbol(Term.MEM, String.valueOf(nxc), new Location(startLoc, startLoc));
            case '.':
                return new Symbol(Term.DOT, String.valueOf(nxc), new Location(startLoc, startLoc));
            case ',':
                return new Symbol(Term.COMMA, String.valueOf(nxc), new Location(startLoc, startLoc));
            case ':':
                return new Symbol(Term.COLON, String.valueOf(nxc), new Location(startLoc, startLoc));
            case ';':
                return new Symbol(Term.SEMIC, String.valueOf(nxc), new Location(startLoc, startLoc));
            case '[':
                return new Symbol(Term.LBRACKET, String.valueOf(nxc), new Location(startLoc, startLoc));
            case ']':
                return new Symbol(Term.RBRACKET, String.valueOf(nxc), new Location(startLoc, startLoc));
            case '(':
                return new Symbol(Term.LPARENTHESIS, String.valueOf(nxc), new Location(startLoc, startLoc));
            case ')':
                return new Symbol(Term.RPARENTHESIS, String.valueOf(nxc), new Location(startLoc, startLoc));
            case '{':
                return new Symbol(Term.LBRACE, String.valueOf(nxc), new Location(startLoc, startLoc));
            case '}':
                return new Symbol(Term.RBRACE, String.valueOf(nxc), new Location(startLoc, startLoc));
            default:
                break;
        }

        //string a-zA-Z0-9_, starts with ^[0-9]
        if ( isIdentAscii(nxc) ) {
            String builder = "" + nxc;
            while ( isNumeric(peekChar()) || isIdentAscii(peekChar()) ) {
                builder += readChar();
            }
            Location endLoc = new Location(line, column);
            //let's find out what our string represents
            switch (builder) {
                case "none":
                    return new Symbol(Term.VOIDCONST, builder, new Location(startLoc, endLoc));
                case "true":
                    return new Symbol(Term.BOOLCONST, builder, new Location(startLoc, endLoc));
                case "false":
                    return new Symbol(Term.BOOLCONST, builder, new Location(startLoc, endLoc));
                case "null":
                    return new Symbol(Term.PTRCONST, builder, new Location(startLoc, endLoc));
                case "arr":
                    return new Symbol(Term.ARR, builder, new Location(startLoc, endLoc));
                case "bool":
                    return new Symbol(Term.BOOL, builder, new Location(startLoc, endLoc));
                case "char":
                    return new Symbol(Term.CHAR, builder, new Location(startLoc, endLoc));
                case "del":
                    return new Symbol(Term.DEL, builder, new Location(startLoc, endLoc));
                case "do":
                    return new Symbol(Term.DO, builder, new Location(startLoc, endLoc));
                case "else":
                    return new Symbol(Term.ELSE, builder, new Location(startLoc, endLoc));
                case "end":
                    return new Symbol(Term.END, builder, new Location(startLoc, endLoc));
                case "fun":
                    return new Symbol(Term.FUN, builder, new Location(startLoc, endLoc));
                case "if":
                    return new Symbol(Term.IF, builder, new Location(startLoc, endLoc));
                case "int":
                    return new Symbol(Term.INT, builder, new Location(startLoc, endLoc));
                case "new":
                    return new Symbol(Term.NEW, builder, new Location(startLoc, endLoc));
                case "ptr":
                    return new Symbol(Term.PTR, builder, new Location(startLoc, endLoc));
                case "rec":
                    return new Symbol(Term.REC, builder, new Location(startLoc, endLoc));
                case "then":
                    return new Symbol(Term.THEN, builder, new Location(startLoc, endLoc));
                case "typ":
                    return new Symbol(Term.TYP, builder, new Location(startLoc, endLoc));
                case "var":
                    return new Symbol(Term.VAR, builder, new Location(startLoc, endLoc));
                case "void":
                    return new Symbol(Term.VOID, builder, new Location(startLoc, endLoc));
                case "where":
                    return new Symbol(Term.WHERE, builder, new Location(startLoc, endLoc));
                case "while":
                    return new Symbol(Term.WHILE, builder, new Location(startLoc, endLoc));
                default:
                    return new Symbol(Term.IDENTIFIER, builder, new Location(startLoc, endLoc));
            }

        }
        //we shouldn't be here
        throw new Report.Error(new Location(line, column), String.format("Unknown character: %s.%n", String.valueOf(nxc)));
    }

    //read char and set line and column to current location
    private char readChar() throws IOException {
        int ic = srcFile.read();
        if ( ic == -1 ) {
            reached_eof = true;
            return '4';
        }
        char c = (char)ic;
        column++;
        if (c == '\n') {
            line++;
            column = 0;
        }
        //System.out.printf("%s [%d, %d]%n", String.valueOf(c), line, column);
        return c;
    }

    private char peekChar() throws IOException {
        srcFile.mark(1);
        char c = (char)srcFile.read();
        srcFile.reset();
        return c;
    }

    private boolean isNumeric(char c) {
        if ( c >= '0' && c <= '9' )
            return true;
        return false;
    }

    private boolean isIdentAscii(char c) {
        //check for a-zA-Z_
        if ( (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '_' )
            return true;
        else
            return false;
    }

}

