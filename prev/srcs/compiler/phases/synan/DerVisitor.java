package compiler.phases.synan;

import compiler.phases.synan.dertree.*;

public interface DerVisitor<Result, Arg> {

	public Result visit(DerLeaf leaf, Arg visArg);

	public Result visit(DerNode node, Arg visArg);

}
