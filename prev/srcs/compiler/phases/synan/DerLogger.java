package compiler.phases.synan;

import common.logger.*;
import compiler.phases.synan.dertree.*;

public class DerLogger implements DerVisitor<Object, Object> {

	private final Logger logger;

	public DerLogger(Logger logger) {
		this.logger = logger;
	}

	@Override
	public Object visit(DerLeaf leaf, Object visArg) {
		if (logger == null)
			return null;
		leaf.symb.log(logger);
		return null;
	}

	@Override
	public Object visit(DerNode node, Object visArg) {
		if (logger == null)
			return null;
		logger.begElement("nont");
		logger.addAttribute("label", node.label.toString());
		for (DerTree subTree : node.subtrees()) {
			subTree.accept(this, visArg);
		}
		if (node.location() != null)
			node.location().log(logger);
		logger.endElement();
		return null;
	}

}
