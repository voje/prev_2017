package compiler.phases.synan.dertree;

import java.util.*;
import common.report.*;
import compiler.phases.synan.*;

public class DerNode extends DerTree {

	public Nont label;

	private final Vector<DerTree> subtrees;

	private Location location;

	public DerNode(Nont label) {
		this.label = label;
		this.subtrees = new Vector<DerTree>();
	}

	public DerNode add(DerTree subtree) {
		subtrees.addElement(subtree);
		Location location = subtree.location();
		this.location = (this.location == null) ? location
				: ((location == null) ? this.location : new Location(this.location, location));
		return this;
	}

	public Vector<DerTree> subtrees() {
		return new Vector<DerTree>(subtrees);
	}
	
	public DerTree subtree(int index) {
		return subtrees.elementAt(index);
	}

	@Override
	public Location location() {
		return location;
	}

	@Override
	public <Result, Arg> Result accept(DerVisitor<Result, Arg> visitor, Arg accArg) {
		return visitor.visit(this, accArg);
	}

}
