package compiler.phases.synan.dertree;

import common.report.*;
import compiler.phases.synan.*;

public abstract class DerTree implements Locatable {

	public abstract <Result, Arg> Result accept(DerVisitor<Result, Arg> visitor, Arg accArg);

}
