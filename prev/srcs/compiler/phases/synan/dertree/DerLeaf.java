package compiler.phases.synan.dertree;

import common.report.*;
import compiler.phases.lexan.*;
import compiler.phases.synan.*;

public class DerLeaf extends DerTree {

	public final Symbol symb;

	public DerLeaf(Symbol symb) {
		this.symb = symb;
	}

	@Override
	public Location location() {
		return symb.location();
	}

	@Override
	public <Result, Arg> Result accept(DerVisitor<Result, Arg> visitor, Arg accArg) {
		return visitor.visit(this, accArg);
	}

}
