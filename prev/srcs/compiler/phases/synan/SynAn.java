package compiler.phases.synan;

import common.report.*;
import compiler.phases.*;
import compiler.phases.lexan.*;
import compiler.phases.synan.dertree.*;

import static compiler.phases.lexan.Term.*;

public class SynAn extends Phase {

	private static DerTree derTree = null;

	public static DerTree derTree() {
		return derTree;
	}

	private final LexAn lexAn;

	public SynAn() {
		super("synan");
		lexAn = new LexAn();
	}

	private Symbol currSymb = null;

	private Symbol skip(DerNode node) {
		if (currSymb != null)
			node.add(new DerLeaf(currSymb));
            currSymb = lexAn.lexer();   //read next symbol?
		return null;
	}

	private void check_skip(Term symb, DerNode node) {
	    if (!currSymb.token.equals(symb)) {
            throw new Report.Error(currSymb.location(), String.format("%s, Synan, parseLowexpr()",currSymb.lexeme));
        }
        if (currSymb != null)
            node.add(new DerLeaf(currSymb));
        currSymb = lexAn.lexer();   //read next symbol?
    }

	public DerTree parser() {
        currSymb = lexAn.lexer();   //read first symbol?
        if (currSymb.token.equals(EOF))
            return null;
		derTree = parseSource();
		currSymb = currSymb == null ? lexAn.lexer() : currSymb;
		if (currSymb.token != Term.EOF)
			throw new Report.Error(currSymb, "Unexpected '" + currSymb + "' at the end of a program.");
		derTree.accept(new DerLogger(logger), null);
		return derTree;
	}

	@Override
	public void close() {
		lexAn.close();
		super.close();
	}

	// --- PARSER ---

	private DerNode parseSource() {
		DerNode node = new DerNode(Nont.Source);
		node.add(parseExpr());
		return node;
	}

	private DerNode parseExpr() {
		DerNode node = new DerNode(Nont.Expr);
		/*switch (currSymb.token) {

		    //this goes in unexpr
            case LBRACKET:
                skip(node);
                node.add(parseType());
                check_skip(RBRACKET, node);
                node.add(parseExpr());
                break;

            default:
                node.add(parseOrexpr());
        }*/
		node.add(parseOrexpr());
		return node;
	}

	private DerNode parseOrexpr() {
        DerNode node = new DerNode(Nont.Orexpr);
        node.add(parseAndexpr());
		node.add(parseOrexpr1());
		return node;
	}

	private DerNode parseOrexpr1() {
		DerNode node = new DerNode(Nont.Orexpr1);
			switch (currSymb.token) {
				case IOR:
				case XOR:
					skip(node); //eat | or ^
					node.add(parseAndexpr());
					node.add(parseOrexpr1());
					break;
				default:
				    //
			}
		return node;
	}

	private DerNode parseAndexpr() {
		DerNode node = new DerNode(Nont.Andexpr);
		node.add(parseRelexpr());
		node.add(parseAndexpr1());
		return node;
	}

	private DerNode parseAndexpr1() {
		DerNode node = new DerNode(Nont.Andexpr1);
		switch (currSymb.token) {
			case AND:
				skip(node);
				node.add(parseRelexpr());
				node.add(parseAndexpr1());
				break;
			default:
			    //
		}
		return node;
	}

	private DerNode parseRelexpr() {
		DerNode node = new DerNode(Nont.Relexpr);
		node.add(parseAddexpr());
		node.add(parseRelexpr1());
		return node;
	}

	private DerNode parseRelexpr1() {
		DerNode node = new DerNode(Nont.Relexpr1);
		switch (currSymb.token) {
			case EQU:
			case NEQ:
			case LTH:
			case GTH:
            case LEQ:
            case GEQ:
            	skip(node);
            	node.add(parseAddexpr());
            	node.add(parseRelexpr1());
				break;
			default:
			    //
		}
		return node;
	}

	private DerNode parseAddexpr() {
		DerNode node = new DerNode(Nont.Addexpr);
		node.add(parseMulexpr());
		node.add(parseAddexpr1());
		return node;
	}

	private DerNode parseAddexpr1() {
		DerNode node = new DerNode(Nont.Addexpr1);
		switch (currSymb.token) {
			case ADD:
			case SUB:
				skip(node);
				node.add(parseMulexpr());
				node.add(parseAddexpr1());
				break;
			default:
			    //
		}
		return node;
	}

	private DerNode parseMulexpr() {
		DerNode node = new DerNode(Nont.Mulexpr);
		node.add(parseUnexpr());
		node.add(parseMulexpr1());
		return node;
	}

	private DerNode parseMulexpr1() {
		DerNode node = new DerNode(Nont.Mulexpr1);
		switch (currSymb.token) {
			case MUL:
			case DIV:
			case MOD:
				skip(node);
				node.add(parseUnexpr());
				node.add(parseMulexpr1());
            break;
			default:
			    //
		}
		return node;
	}

	private DerNode parseUnexpr() {
		DerNode node = new DerNode(Nont.Unexpr);
		switch (currSymb.token) {
			case ADD:
			case SUB:
            case NOT:
            case VAL:
            case MEM:
            case DEL:
			    skip(node);
                node.add(parseUnexpr());
                break;
            case NEW:
                skip(node);
                node.add(parseType());
                break;
            case LBRACKET:
                skip(node);
                node.add(parseType());
                check_skip(RBRACKET, node);
                node.add(parseUnexpr());
                break;
			default:
			    node.add(parseAccexpr());
		}
		return node;
	}

	private DerNode parseAccexpr() {
	    DerNode node = new DerNode(Nont.Accexpr);
        node.add(parseLowexpr());
        node.add(parseAccexpr1());
	    return node;
    }

    private DerNode parseAccexpr1() {
        DerNode node = new DerNode(Nont.Accexpr1);
        switch (currSymb.token) {
            case LBRACKET:
                skip(node);
                node.add(parseExpr());
                check_skip(RBRACKET, node);
                node.add(parseAccexpr1());
                break;
            case DOT:
                skip(node);
                check_skip(IDENTIFIER, node);
                node.add(parseAccexpr1());
                break;
            default:
                //
        }
        return node;
    }

	private DerNode parseLowexpr() {
		DerNode node = new DerNode(Nont.Lowexpr);
		//end it
			switch (currSymb.token) {
			    //literal
                case VOIDCONST:
                case BOOLCONST:
                case CHARCONST:
                case INTCONST:
                case PTRCONST:
					skip(node);
					break;
                case LPARENTHESIS:
                    skip(node);
                    node.add(parseExpr());
                    check_skip(RPARENTHESIS, node);
                    break;
                case LBRACE:
                    skip(node);
                    node.add(parseStmt());
                    node.add(parseMorestmt());
                    check_skip(COLON, node);
                    node.add(parseExpr());
                    node.add(parseMaybewhere());
                    check_skip(RBRACE, node);
                    break;
                case IDENTIFIER:
                    skip(node);
                    node.add(parseIdexpr());
                    break;
				default:
					throw new Report.Error(currSymb.location(), String.format("%s, Synan, parseLowexpr()",currSymb.lexeme));
			}
		return node;
	}

	private DerNode parseIdexpr() {
        DerNode node = new DerNode(Nont.Idexpr);
            switch (currSymb.token) {
                case LPARENTHESIS:
                    skip(node);
                    node.add(parseMaybeexpr());
                    check_skip(RPARENTHESIS, node);
                    break;
                default:
                    //
            }
        return node;
    }

    private DerNode parseMaybeexpr() {
	    DerNode node = new DerNode(Nont.Maybeexpr);
	        switch (currSymb.token) { //lookahead, don't skip
                case RPARENTHESIS:
                    break;
                default:
                    node.add(parseExpr());
                    node.add(parseMoreexpr());
            }
	    return node;
    }

    private DerNode parseMoreexpr() {
	    DerNode node = new DerNode(Nont.Moreexpr);
	        switch (currSymb.token) {
                case COMMA:
                    skip(node);
                    node.add(parseExpr());
                    node.add(parseMoreexpr());
                    break;
                default:
                    //
            }
	    return node;
    }

    private DerNode parseType() {
	    DerNode node = new DerNode(Nont.Type);
        switch (currSymb.token) {
            case VOID:
            case BOOL:
            case CHAR:
            case INT:
            case IDENTIFIER:
                skip(node);
                break;
            case PTR:
                skip(node);
                node.add(parseType());
                break;
            case ARR:
                skip(node);
                check_skip(LBRACKET, node);
                node.add(parseExpr());
                check_skip(RBRACKET, node);
                node.add(parseType());
                break;
            case REC:
                skip(node);
                check_skip(LPARENTHESIS, node);
                node.add(parseIdtyp());
                node.add(parseMoreidtyp());
                check_skip(RPARENTHESIS, node);
                break;
            default:
                throw new Report.Error(currSymb.location(), String.format("%s, Synan, parseType()",currSymb.lexeme));
        }
	    return node;
    }

    private DerNode parseMoreidtyp() {
	    DerNode node = new DerNode(Nont.Moreidtyp);
        switch (currSymb.token) {
            case COMMA:
                skip(node);
                node.add(parseIdtyp());
                node.add(parseMoreidtyp());
                break;
            default:
                //
        }
	    return node;
    }

    private DerNode parseStmt() {
	    DerNode node = new DerNode(Nont.Stmt);
	    switch (currSymb.token) {
            case IF:
                skip(node);
                node.add(parseExpr());
                check_skip(THEN, node);
                node.add(parseStmt());
                node.add(parseMorestmt());
                node.add(parseMaybestmt());
                check_skip(END, node);
                break;
            case WHILE:
                skip(node);
                node.add(parseExpr());
                check_skip(DO, node);
                node.add(parseStmt());
                node.add(parseMorestmt());
                check_skip(END, node);
                break;
            default:
                node.add(parseExpr());
                node.add(parseStmtexpr());
        }
	    return node;
    }

    private DerNode parseMaybestmt() {
	    DerNode node = new DerNode(Nont.Maybestmt);
        switch (currSymb.token) {
            case ELSE:
                skip(node);
                node.add(parseStmt());
                node.add(parseMorestmt());
                break;
            default:
                //
        }
	    return node;
    }

    private DerNode parseStmtexpr() {
	    DerNode node = new DerNode(Nont.Stmtexpr);
        switch (currSymb.token) {
            case ASSIGN:
                skip(node);
                node.add(parseExpr());
                break;
            default:
                //
        }
	    return node;
    }

    private DerNode parseMorestmt() {
        DerNode node = new DerNode(Nont.Morestmt);
        switch (currSymb.token) {
            case SEMIC:
                skip(node);
                node.add(parseStmt());
                node.add(parseMorestmt());
                break;
            default:
                //
        }
        return node;
    }

    private DerNode parseMaybewhere() {
        DerNode node = new DerNode(Nont.Maybewhere);
        switch (currSymb.token) {
            case WHERE:
                skip(node);
                node.add(parseDecl());
                node.add(parseMoredecl());
                break;
        }
        return node;
    }

    private DerNode parseDecl() {
        DerNode node = new DerNode(Nont.Decl);
        switch (currSymb.token) {
            case TYP:
                skip(node);
                node.add(parseIdtyp());
                break;
            case VAR:
                skip(node);
                node.add(parseIdtyp());
                break;
            case FUN:
                skip(node);
                check_skip(IDENTIFIER, node);
                check_skip(LPARENTHESIS, node);
                node.add(parseMaybeidtyp());
                check_skip(RPARENTHESIS, node);
                check_skip(COLON, node);
                node.add(parseType());
                node.add(parseMaybe1expr());
                break;
            default:
                throw new Report.Error(currSymb.location(), String.format("%s, Synan, parseDecl()",currSymb.lexeme));
        }
        return node;
    }

    private DerNode parseMaybeidtyp() {
        DerNode node = new DerNode(Nont.Maybeidtyp);
        switch (currSymb.token) { //lookahead, don't skip
            case IDENTIFIER:
                node.add(parseIdtyp());
                node.add(parseMoreidtyp());
                break;
            case RPARENTHESIS:
                //
                break;
            default:
                throw new Report.Error(currSymb.location(), String.format("%s, Synan, parseMaybeidtyp()",currSymb.lexeme));
        }
        return node;
    }

    private DerNode parseMaybe1expr() {
        DerNode node = new DerNode(Nont.Maybe1expr);
        switch (currSymb.token) {
            case ASSIGN:
                skip(node);
                node.add(parseExpr());
                break;
            default:
                //
        }
        return node;
    }

    private DerNode parseIdtyp() {
	    DerNode node = new DerNode(Nont.Idtyp);
	    check_skip(IDENTIFIER, node);
	    check_skip(COLON, node);
	    node.add(parseType());
	    return node;
    }

    private DerNode parseMoredecl() {
	    DerNode node = new DerNode(Nont.Moredecl);
        switch (currSymb.token) {
            case SEMIC:
                skip(node);
                node.add(parseDecl());
                node.add(parseMoredecl());
                break;
            default:
                //
        }
	    return node;
    }

}
