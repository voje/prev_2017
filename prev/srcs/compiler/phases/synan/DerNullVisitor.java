package compiler.phases.synan;

import compiler.phases.synan.dertree.*;

public class DerNullVisitor<Result, Arg> implements DerVisitor<Result, Arg> {

	@Override
	public Result visit(DerLeaf leaf, Arg visArg) {
		return null;
	}

	@Override
	public Result visit(DerNode node, Arg visArg) {
		return null;
	}

}
