package compiler.phases.asmgen;

import common.report.Report;
import compiler.phases.frames.Label;
import compiler.phases.frames.Temp;
import compiler.phases.imcgen.ImcVisitor;
import compiler.phases.imcgen.code.*;
import compiler.phases.lincode.ImcFullVisitor;
import compiler.phases.regalloc.Regalloc;

import java.util.Vector;

import static compiler.phases.imcgen.ImcGen.FP;
import static compiler.phases.imcgen.ImcGen.SP;
import static compiler.phases.imcgen.ImcGen.rR;

/**
 * Created by kristjan on 16.5.2017.
 */
public class TilePaver implements ImcVisitor<Temp, Object> {
    //recursively: place tiles from biggest to smallest

    @Override
    public Temp visit(ImcBINOP binOp, Object visArg) {
        Temp t1 = binOp.fstExpr.accept(this, visArg); //recycle t1 into result
        Temp t2 = binOp.sndExpr.accept(this, visArg);

        if (    binOp.oper.equals(ImcBINOP.Oper.ADD) ||
                binOp.oper.equals(ImcBINOP.Oper.SUB) ||
                binOp.oper.equals(ImcBINOP.Oper.DIV) ||
                binOp.oper.equals(ImcBINOP.Oper.MUL)) {

            //in case we're adding to a global register(FP), use a new one
            Temp res;
            if (t1.equals(FP)) {
                res = new Temp();
            } else {
                res = t1;
            }

            Vector<Temp> u = new Vector<Temp>(); u.add(t1); u.add(t2);
            Vector<Temp> d = new Vector<Temp>(); d.add(res);
            AsmOPER ao = new AsmOPER(binOp.oper.name() + ", `d0, `s0, `s1", u, d, null);
            AsmGen.addInstr(ao);
            return res;
        } else if (binOp.oper.equals(ImcBINOP.Oper.MOD)) {
            Vector<Temp> u = new Vector<Temp>(); u.add(t1); u.add(t2);
            Vector<Temp> d = new Vector<Temp>(); d.add(t1); d.add(rR);
            AsmOPER ao = new AsmOPER("DIV `d0, `s0, `s1", u, d, null);
            AsmGen.addInstr(ao);
            u = new Vector<Temp>(); u.add(rR);
            d = new Vector<Temp>(); d.add(t1);
            AsmMOVE ao1 = new AsmMOVE("SET `d0, `s0", u, d, null);
            AsmGen.addInstr(ao1);
        } else if (binOp.oper.equals(ImcBINOP.Oper.EQU) ||
                binOp.oper.equals(ImcBINOP.Oper.NEQ) ||
                binOp.oper.equals(ImcBINOP.Oper.GEQ) ||
                binOp.oper.equals(ImcBINOP.Oper.GTH) ||
                binOp.oper.equals(ImcBINOP.Oper.LEQ) ||
                binOp.oper.equals(ImcBINOP.Oper.LTH) ) {
            //set a helper register (constant 1 - False)
            Temp one = new Temp();
            Vector<Temp> d = new Vector<Temp>(); d.add(one);
            AsmOPER ao0 = new AsmOPER("SETL, `d0, 1", null, d, null);
            AsmGen.addInstr(ao0);
            //subtract and compare (if true, return 0)
            Vector<Temp> u = new Vector<Temp>(); u.add(t1); u.add(t2);
            d = new Vector<Temp>(); d.add(t1);
            AsmOPER ao = new AsmOPER("CMP `d0, `s0, `s1", u, d, null);
            AsmGen.addInstr(ao);
            //diff in t1 (-1, 0, 1)
            if (binOp.oper.equals(ImcBINOP.Oper.EQU)) {
                return t1;
            } else if (binOp.oper.equals(ImcBINOP.Oper.NEQ)) {
                //if t1 == 0, set nonzero
                u = new Vector<Temp>(); u.add(t1); u.add(one);
                d = new Vector<Temp>(); d.add(t1);
                AsmOPER ao1 = new AsmOPER("ZSZ `d0, `s0, `s1", u, d, null);
                AsmGen.addInstr(ao1);
                return t1;
            } else if (binOp.oper.equals(ImcBINOP.Oper.GEQ)) {
                //if t1 < 0, set nonzero
                u = new Vector<Temp>(); u.add(t1); u.add(one);
                d = new Vector<Temp>(); d.add(t1);
                AsmOPER ao1 = new AsmOPER("ZSN, `d0, `s0, `s1", u, d, null);
                AsmGen.addInstr(ao1);
                return t1;
            } else if (binOp.oper.equals(ImcBINOP.Oper.GTH)) {
                //if t1 < 0, set nonzero
                u = new Vector<Temp>(); u.add(t1); u.add(one);
                d = new Vector<Temp>(); d.add(t1);
                AsmOPER ao1 = new AsmOPER("ZSNP, `d0, `s0, `s1", u, d, null);
                AsmGen.addInstr(ao1);
                return t1;
            } else if (binOp.oper.equals(ImcBINOP.Oper.LEQ)) {
                //if t1 < 0, set nonzero
                u = new Vector<Temp>(); u.add(t1); u.add(one);
                d = new Vector<Temp>(); d.add(t1);
                AsmOPER ao1 = new AsmOPER("ZSP, `d0, `s0, `s1", u, d, null);
                AsmGen.addInstr(ao1);
                return t1;
            } else if (binOp.oper.equals(ImcBINOP.Oper.LTH)) {
                //if t1 < 0, set nonzero
                u = new Vector<Temp>(); u.add(t1); u.add(one);
                d = new Vector<Temp>(); d.add(t1);
                AsmOPER ao1 = new AsmOPER("ZSNN, `d0, `s0, `s1", u, d, null);
                AsmGen.addInstr(ao1);
                return t1;
            }
        }
        return t1;
    }

    @Override
    public Temp visit(ImcCALL call, Object visArg) {
        int offset = 0;
        Temp tmparg;
        for (ImcExpr arg : call.args()) {

            //handle MEM
            if (arg instanceof ImcMEM) {
                ImcMEM mem = (ImcMEM)arg;
                if (mem.addr instanceof ImcNAME) {
                    ImcNAME nam = (ImcNAME) mem.addr;
                    Vector<Temp> defs = new Vector<Temp>();
                    tmparg = new Temp();
                    defs.add(tmparg); //in td, we get the original registers from the fragment
                    //load from label into tmparg
                    AsmOPER so = new AsmOPER("LDO, `d0, " + nam.label.name, null, defs, null);
                    AsmGen.addInstr(so);
                } else {
                    // CALL - mem - expr
                    Temp addr = mem.addr.accept(this, visArg);
                    tmparg = new Temp();
                    Vector<Temp> defs = new Vector<Temp>();
                    defs.add(tmparg);
                    Vector<Temp> uses = new Vector<Temp>();
                    uses.add(addr);
                    AsmOPER so = new AsmOPER("LDO, `d0, `s0", uses, defs, null);
                    AsmGen.addInstr(so);
                }
            } else {
                tmparg = arg.accept(this, visArg);
            }

            Vector<Temp> u = new Vector<Temp>(); u.add(tmparg); u.add(SP);
            AsmOPER ao = new AsmOPER("STO `s0, `s1, " + Integer.toString(offset), u, null, null);
            AsmGen.addInstr(ao);
            offset += 8;
        }
        //PUSHJ
        Temp retreg = new Temp();
        Vector<Label> j = new Vector<Label>(); j.add(call.label);
        /*
        Vector<Temp> d = new Vector<Temp>(); d.add(retreg);
        AsmOPER ao1 = new AsmOPER("PUSHJ `d0, " + j.get(0).name, null, d, j);
        */
        //Pushj with hard coded number of push registers
        AsmOPER ao1 = new AsmOPER("PUSHJ $" + (Regalloc.K-1) + ", " + j.get(0).name, null, null, j );
        AsmGen.addInstr(ao1);

        //after PUSHJ, fetch return value from SP and store it into return register
        Vector<Temp> uses2 = new Vector<Temp>(); uses2.add(SP);
        Vector<Temp> defs2 = new Vector<Temp>(); defs2.add(retreg);
        AsmOPER ao2 = new AsmOPER("LDO `d0 `s0", uses2, defs2, null);
        AsmGen.addInstr(ao2);

        return retreg;
    }

    @Override
    public Temp visit(ImcCJUMP cjump, Object visArg) {
        Temp t1 = cjump.cond.accept(this, visArg); //t1 = 0 if true
        //if true, jump, if false, continut
        Vector<Temp> u = new Vector<Temp>(); u.add(t1);
        Vector<Label> j = new Vector<Label>(); j.add(cjump.posLabel); //the one that doesn't follow
        AsmOPER ao = new AsmOPER("BNZ `s0 " + j.get(0).name, u, null, j);
        AsmGen.addInstr(ao);
        return null;
    }

    @Override
    public Temp visit(ImcCONST constant, Object visArg) {
        //command: 4 x 8b
        //in case we land in here, we're doing atomic const tile
        Temp res = new Temp();
        //we need to cut up the constant and serve it to ao1-4
        long l = constant.value;
        boolean isneg = (l < 0);
        if (isneg) l *= (-1);

        String s1 = Long.toString(l & 0xFF); l = l >> 8;
        String s2 = Long.toString(l & 0xFF); l = l >> 8;
        String s3 = Long.toString(l & 0xFF); l = l >> 8;
        String s4 = Long.toString(l & 0xFF); l = l >> 8;

        Vector<Temp> d1 = new Vector<Temp>(); d1.add(res);
        AsmOPER ao1 = new AsmOPER("SETL `d0, " + s1, null, d1, null); //sets the lower 8b
        AsmGen.addInstr(ao1);

        if (l > 127) {
            AsmOPER ao2 = new AsmOPER("SETML, `d0, " + s2, null, d1, null);
            AsmOPER ao3 = new AsmOPER("SETMH, `d0, " + s3, null, d1, null);
            AsmOPER ao4 = new AsmOPER("SETH, `d0, " + s4, null, d1, null);
            AsmGen.addInstr(ao2);
            AsmGen.addInstr(ao3);
            AsmGen.addInstr(ao4);
        }


        if (isneg) { //hack: negate if neg
            Vector<Temp> v1 = new Vector<Temp>(); v1.add(res);
            AsmOPER oa2 = new AsmOPER("NEG `d0, 0, `s0", v1, v1, null);
            AsmGen.addInstr(oa2);
        }
        return res;
    }

    @Override
    public Temp visit(ImcESTMT eStmt, Object visArg) {
        eStmt.expr.accept(this, visArg);
        return null;
    }

    @Override
    public Temp visit(ImcJUMP jump, Object visArg) {
        Vector<Label> j = new Vector<Label>(); j.add(jump.label);
        AsmOPER jop = new AsmOPER("JMP " + j.get(0).name, null, null, j);
        AsmGen.addInstr(jop);
        return null;
    }

    @Override
    public Temp visit(ImcLABEL label, Object visArg) {
        AsmLABEL al = new AsmLABEL(label.label);
        AsmGen.addInstr(al);
        return null;
    }

    @Override
    public Temp visit(ImcMEM mem, Object visArg) {
        Temp res = mem.addr.accept(this, visArg);
        return res;
    }

    @Override
    public Temp visit(ImcMOVE move, Object visArg) {
        Temp td = move.dst.accept(this, visArg); //try to recycle these 2 temporaries
        Temp ts = move.src.accept(this, visArg);

        if (move.dst instanceof ImcMEM ) {
            //STORE
            ImcMEM dstmem = (ImcMEM)move.dst;
            if (dstmem.addr instanceof ImcNAME) {
                //STO $r, _label
                ImcNAME tmpname = (ImcNAME)dstmem.addr;
                //td = ((ImcNAME) dstmem.addr).accept(this, null);
                Vector<Temp> vs = new Vector<Temp>();
                vs.add(ts);
                //Vector<Temp> vd = new Vector<Temp>();
                //vd.add(td);
                AsmOPER ao = new AsmOPER("STO `s0, " + tmpname.label.name, vs, null, null);
                AsmGen.addInstr(ao);
            } else {
                //address is stored in td, value is stored in ts
                //STO $r1, $r2, $r3
                Vector<Temp> u = new Vector<Temp>(); u.add(ts); u.add(td);
                AsmOPER ao = new AsmOPER("STO `s0, `s1, 0", u, null, null);
                AsmGen.addInstr(ao);
            }
        } else if (move.src instanceof ImcMEM) {
            //LOAD
            ImcMEM srcmem = (ImcMEM) move.src;
            if (srcmem.addr instanceof ImcNAME) {
                ImcNAME tmpname = (ImcNAME) srcmem.addr;
                Vector<Temp> defs = new Vector<Temp>();
                defs.add(td); //in td, we get the original registers from the fragment
                AsmOPER so = new AsmOPER("LDO, `d0, " + tmpname.label.name, null, defs, null);
                AsmGen.addInstr(so);
            } else {
                //ts should contain the calculated address
                Vector<Temp> defs = new Vector<Temp>(); defs.add(td);
                Vector<Temp> uses = new Vector<Temp>(); uses.add(ts);
                AsmOPER so = new AsmOPER("LDO, `d0, `s0", uses, defs, null);
                AsmGen.addInstr(so);
            }
        } else if (move.dst instanceof ImcTEMP) {
            //SET $rdst, $rsrc (ts should hold a value from an expression)
            Vector<Temp> vd = new Vector<Temp>(); vd.add(td);
            Vector<Temp> vs = new Vector<Temp>(); vs.add(ts);
            AsmMOVE am = new AsmMOVE("SET `d0, `s0", vs, vd, null);
            AsmGen.addInstr(am);
        } else {
            throw new Report.Error("TilePaver | ImcMOVE");
        }

        //MOVE shouldn't return anything
        return null;
    }

    @Override
    public Temp visit(ImcNAME name, Object visArg) {
        Temp ret = new Temp();
        return ret;
    }

    @Override
    public Temp visit(ImcSEXPR sExpr, Object visArg) {
        sExpr.stmt.accept(this, visArg);
        sExpr.expr.accept(this, visArg);
        return null;
    }

    @Override
    public Temp visit(ImcSTMTS stmts, Object visArg) {
        for (ImcStmt stmt:stmts.stmts())
            stmt.accept(this, visArg);
        return null;
    }

    @Override
    public Temp visit(ImcTEMP temp, Object visArg) {
        return temp.temp;
    }

    @Override
    public Temp visit(ImcUNOP unOp, Object visArg) {
        Temp t1 = unOp.subExpr.accept(this, visArg);
        return t1;
    }
}
