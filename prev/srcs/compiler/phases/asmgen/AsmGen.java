package compiler.phases.asmgen;

import compiler.phases.*;
import compiler.phases.frames.Temp;
import compiler.phases.imcgen.code.ImcStmt;
import compiler.phases.lincode.*;

import java.util.LinkedList;
import java.util.Vector;

/**
 * Created by kristjan on 16.5.2017.
 */
public class AsmGen extends Phase {

    public static boolean debug = false;

    //we are generating a vector of AsmInstr
    //vector of fragments > vector of canon trees > vector of stmts
    public static final Vector<Vector<Vector<AsmInstr>>> instructions = new Vector<Vector<Vector<AsmInstr>>>();

    public static void hello() {
        System.out.println("hi");
    }

    public AsmGen() {
        super("asmgen");
    }

    public static void debug_print() {
        int id = 0;
        for (int i = 0; i < instructions.size(); i++) {
            Vector<Vector<AsmInstr>> fragment = instructions.get(i);
            System.out.printf("########################## Fragment: %d ##%n", i);
            for (int j = 0; j < fragment.size(); j++) {
                Vector<AsmInstr> canon = fragment.get(j);
                if (canon.size() == 0) continue;
                System.out.printf("-------------------------- Canon: %d --%n", j);
                for (AsmInstr instr : canon) {
                    System.out.printf("%3d|  ", id);
                    System.out.println(instr.toString());
                    id += 1;
                }
            }
        }
    }

    @Override
    public void close() {
        if (this.debug) {
            int id = 0;
            for (int i = 0; i < instructions.size(); i++) {
                Vector<Vector<AsmInstr>> fragment = instructions.get(i);
                System.out.printf("########################## Fragment: %d ##%n", i);
                for (int j = 0; j < fragment.size(); j++) {
                    Vector<AsmInstr> canon = fragment.get(j);
                    if (canon.size() == 0) continue;
                    System.out.printf("-------------------------- Canon: %d --%n", j);
                    for (AsmInstr instr : canon) {
                        System.out.printf("%3d|  ", id);
                        System.out.println(instr.toString());
                        id += 1;
                    }
                }
            }
        } //if(this.debug)
        super.close();
    }

    public static void addFragment() {
        instructions.add(new Vector<Vector<AsmInstr>>());
    }
    public static void addCanon() {
        instructions.lastElement().add(new Vector<AsmInstr>());
    }
    public static void addInstr(AsmInstr ai) {
        instructions.lastElement().lastElement().add(ai);
    }

    public static Vector<Vector<Vector<AsmInstr>>> instructions() {
        return instructions;
    }

    public static Vector<Vector<AsmInstr>> fragment_instructions(int fragment_id) {
        return instructions.get(fragment_id);
    }

    //iterate fragments, foreach CodeFragment: iterate stmts.
    //always add to instructions vector
    public void pave (LinkedList<Fragment> fragments) {
        for (int i=0; i<fragments.size(); i++) {
            if (fragments.get(i) instanceof DataFragment) {
               //skip
            } else {
                CodeFragment frg = (CodeFragment)fragments.get(i);
                this.addFragment();
                Vector<ImcStmt> stmts = frg.stmts();
                for (int j=0; j<stmts.size(); j++) {
                    this.addCanon();
                    stmts.get(j).accept(new TilePaver(), null);
                }
            }
        }
    }

}
