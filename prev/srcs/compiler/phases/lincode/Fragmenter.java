package compiler.phases.lincode;

import java.util.*;

import common.report.Report;
import common.utils.VisitorFlags;
import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;
import compiler.phases.frames.*;
import compiler.phases.imcgen.*;
import compiler.phases.imcgen.code.*;

import static compiler.phases.imcgen.ImcGen.FP;

public class Fragmenter extends AbsFullVisitor<Object, Object> {

    private Vector<ImcStmt> finishBlocks(Label bl, Label el, Vector<ImcStmt> canon) {
        //start with bl, if you find a label, prepend a jump; if jump/cond isn't followed by a label, error
        boolean inBlock = false;
        int s = canon.size();
        for (int i=0; i<s; i++) {
            if (inBlock) {
                if (canon.get(i) instanceof ImcLABEL) { //prepend a jump to that label
                    ImcLABEL lb = (ImcLABEL) canon.get(i);
                    ImcStmt jmp = new ImcJUMP(lb.label);
                    canon.add(i, jmp);
                    s++;
                    i++;
                }
                else if (canon.get(i) instanceof ImcJUMP || canon.get(i) instanceof ImcCJUMP) {
                    inBlock = false;
                }
            } else {
                if (canon.get(i) instanceof ImcLABEL)
                    inBlock = true;
                else if (canon.get(i) instanceof ImcJUMP || canon.get(i) instanceof ImcCJUMP)
                    throw new Report.Error("lincode: JUMP/CJUMP outside of block");
            }
        }
        return canon;
    }

    private Vector<ImcStmt> removeRedundantJumps(Vector<ImcStmt> canon) {
        int i = 0;
        while (i < canon.size()) {
            ImcStmt tmpstmt = canon.get(i);
            if (    tmpstmt instanceof ImcJUMP && i + 1 < canon.size()) {
                ImcStmt mbyLabel = canon.get(i+1);
                if (mbyLabel instanceof ImcLABEL) {
                    if ( ((ImcJUMP)tmpstmt).label.equals( ((ImcLABEL)mbyLabel).label ) ) {
                        //we found a jump that jumps straight into the next label
                        //remove the jump
                        canon.remove(i);
                        continue;
                    }
                }
            }
            i++;
        }
        return canon;
    }

	private VisitorFlags vf;
	public Fragmenter() {
		this.vf = new VisitorFlags();
	}

	//handling top level (main) function
	@Override
	public Object visit(AbsStmtExpr stmtExpr, Object visArg) {
		//check if we're in top level (first fragment);
        if (vf.topLevelStmtExpr) {
			vf.topLevelStmtExpr = false;

            Frame frame = new Frame(new Label(""), 0, 0, 0);
            Temp RV = new Temp();
            Label begLabel = new Label();
            Label endLabel = new Label();

            ImcExpr src = ImcGen.exprImCode.get(stmtExpr.expr);
            if (src instanceof ImcNAME) {
               src = new ImcMEM(src);
            }
            ImcStmt stmt = new ImcMOVE(new ImcTEMP(RV), src);
            {
                Vector<ImcStmt> canStmts = new Vector<ImcStmt>();
                //add stmts
                if (stmtExpr.stmts != null) {
                    for (AbsStmt tmpstmt : stmtExpr.stmts.stmts()) {
                        canStmts.add(ImcGen.stmtImCode.get(tmpstmt));
                    }
                }
                //add expr
                canStmts.add(stmt);

                //manipulate canon trees
                CanonGenerator cg = new CanonGenerator(canStmts);
                canStmts = cg.getCanon();

                //manipulate blocks
                //first add start and end label
                canStmts.add(0, new ImcLABEL(begLabel));
                //canStmts.add(new ImcJUMP(endLabel));
                canStmts = finishBlocks(begLabel, endLabel, canStmts);
                canStmts = removeRedundantJumps(canStmts);

                CodeFragment fragment = new CodeFragment(frame, canStmts, FP, RV, begLabel, endLabel);
                LinCode.add(fragment);
            }
		}
        if (stmtExpr.decls != null) stmtExpr.decls.accept(this, null);
        if (stmtExpr.stmts != null) stmtExpr.stmts.accept(this, null);
        stmtExpr.expr.accept(this, null);

		return null;
	}

	private ImcMOVE save_RV_to_FP (Temp rv) {
	    ImcTEMP src = new ImcTEMP(rv);
	    ImcMEM dst = new ImcMEM(new ImcTEMP(FP));
        ImcMOVE imcm = new ImcMOVE(dst, src);
        return imcm;
    }

	@Override
	public Object visit(AbsFunDef funDef, Object visArg) {
		Frame frame = Frames.frames.get(funDef);
		Temp RV = new Temp();
		//set jump from previous fragment?
        Fragment last = LinCode.fragments().getLast();
        Label begLabel = null;
        if ( last instanceof DataFragment) {
        	begLabel = new Label();
		} else {
			begLabel = ((CodeFragment) last).endLabel;
		}
		//Label begLabel = new Label();
		Label endLabel = new Label();
		ImcStmt stmt = new ImcMOVE(new ImcTEMP(RV), ImcGen.exprImCode.get(funDef.value));
		{
			Vector<ImcStmt> canStmts = new Vector<ImcStmt>();
			canStmts.add(stmt);

			//test
            CanonGenerator cg = new CanonGenerator(canStmts);
            canStmts = cg.getCanon();

            //manipulate blocks
            //first add start and end label
            canStmts.add(0, new ImcLABEL(begLabel));
            //canStmts.add(new ImcJUMP(endLabel));

            //Add saving return value to FP
            canStmts.add(save_RV_to_FP(RV));

            canStmts = finishBlocks(begLabel, endLabel, canStmts);
            canStmts = removeRedundantJumps(canStmts);

			CodeFragment fragment = new CodeFragment(frame, canStmts, FP, RV, begLabel, endLabel);
			LinCode.add(fragment);
		}
		funDef.value.accept(this, null);
		return null;
	}
	
	@Override
	public Object visit(AbsVarDecl varDecl, Object visArg) {
		Access access = Frames.accesses.get(varDecl);
		if (access instanceof AbsAccess) {
			AbsAccess absAccess = (AbsAccess)access;
			DataFragment fragment = new DataFragment(absAccess.label, absAccess.size);
			LinCode.add(fragment);
		}
		return null;
	}

    //convert every top-level non-stmtExpr into a stmtExpr to be handled as a top level (main) function
    public Object visit(AbsBinExpr expr, Object visArg) {
        if (vf.topLevelExpr) {
            vf.topLevelExpr = false;
            AbsStmtExpr entryPoint = new AbsStmtExpr(expr.location(), null, null, expr);
            entryPoint.accept(this, visArg);
        }
        return null;
    }
    public Object visit(AbsArrExpr expr, Object visArg) {
        if (vf.topLevelExpr) {
            vf.topLevelExpr = false;
            AbsStmtExpr entryPoint = new AbsStmtExpr(expr.location(), null, null, expr);
            entryPoint.accept(this, visArg);
        }
        return null;
    }
    public Object visit(AbsAtomExpr expr, Object visArg) {
        if (vf.topLevelExpr) {
            vf.topLevelExpr = false;
            AbsStmtExpr entryPoint = new AbsStmtExpr(expr.location(), null, null, expr);
            entryPoint.accept(this, visArg);
        }
        return null;
    }
    public Object visit(AbsCastExpr expr, Object visArg) {
        if (vf.topLevelExpr) {
            vf.topLevelExpr = false;
            AbsStmtExpr entryPoint = new AbsStmtExpr(expr.location(), null, null, expr);
            entryPoint.accept(this, visArg);
        }
        return null;
    }
    public Object visit(AbsDelExpr expr, Object visArg) {
        if (vf.topLevelExpr) {
            vf.topLevelExpr = false;
            AbsStmtExpr entryPoint = new AbsStmtExpr(expr.location(), null, null, expr);
            entryPoint.accept(this, visArg);
        }
        return null;
    }
    public Object visit(AbsNewExpr expr, Object visArg) {
        if (vf.topLevelExpr) {
            vf.topLevelExpr = false;
            AbsStmtExpr entryPoint = new AbsStmtExpr(expr.location(), null, null, expr);
            entryPoint.accept(this, visArg);
        }
        return null;
    }
    public Object visit(AbsRecExpr expr, Object visArg) {
        if (vf.topLevelExpr) {
            vf.topLevelExpr = false;
            AbsStmtExpr entryPoint = new AbsStmtExpr(expr.location(), null, null, expr);
            entryPoint.accept(this, visArg);
        }
        return null;
    }
    public Object visit(AbsUnExpr expr, Object visArg) {
        if (vf.topLevelExpr) {
            vf.topLevelExpr = false;
            AbsStmtExpr entryPoint = new AbsStmtExpr(expr.location(), null, null, expr);
            entryPoint.accept(this, visArg);
        }
        return null;
    }

}
