package compiler.phases.lincode;

import java.util.*;

import com.sun.org.apache.bcel.internal.classfile.Code;
import common.logger.*;
import compiler.phases.asmgen.AsmGen;
import compiler.phases.asmgen.AsmInstr;
import compiler.phases.asmgen.AsmLABEL;
import compiler.phases.frames.*;
import compiler.phases.imcgen.*;
import compiler.phases.imcgen.code.*;
import compiler.phases.liveness.Liveness;

public class CodeFragment extends Fragment {

	// The stack frame of function.
	public final Frame frame;
	
	// The linearized intermediate code.
	private final Vector<ImcStmt> stmts;
	
	// The frame pointer.
	public final Temp FP;
	
	// The return value.
	public final Temp RV;
	
	// The label the prologue jumps to.
	public final Label begLabel;
	
	// The label the epilogue starts with.
	public final Label endLabel;

	public CodeFragment(Frame frame, Vector<ImcStmt> stmts, Temp FP, Temp RV, Label begLabel, Label endLabel) {
		this.frame = frame;
		this.stmts = new Vector<ImcStmt>(stmts);
		this.FP = FP;
		this.RV = RV;
		this.begLabel = begLabel;
		this.endLabel = endLabel;
	}

	public Vector<ImcStmt> stmts() {
		return stmts;
	}

	private String add_prologue () {
	    long fragment_size = frame.size;
	    long old_RA_offset = frame.locsSize + 16;
	    String prologue = ""
        + "%s\tSET\t$0,FP\n"
        + "\tSET\tFP,SP\n"
        + "\tSET\t$1,%d         #frame size\n"
        + "\tSUB\tSP,SP,$1\n"
        + "\tSET\t$1,%d         #old RA offset\n"
        + "\tSUB\t$1,FP,$1\n"
        + "\tSTO\t$0,$1,0		#storing old FP\n"
        + "\tGET\t$0,rJ\n"
        + "\tSTO\t$0,$1,8		#storing RA\n"
	    + "\t#prologue^\n";
        String fprologue = String.format(prologue, frame.label.name, fragment_size, old_RA_offset);
	    return fprologue;
    }

	private String add_epilogue () {
	    long old_RA_offset = frame.locsSize + 16;
	    String epilogue = ""
        + "\t#epilogue\n"
        + "\tSTO\t$0,FP,0       #TODO change $0 to RV register\n"
        + "\tSET\t$1,%d\n"
        + "\tSUB\t$1,FP,$1\n"
        + "\tLDO\t$0,$1,8\n"
        + "\tPUT\trJ,$0         #restore RA\n"
        + "\tSET\tSP,FP         #restore SP\n"
        + "\tLDO\tFP,$1,0       #restore FP\n"
        + "\tPOP\t0,0\n";
	    String fepilogue = String.format(epilogue, old_RA_offset);
	    return fepilogue;
	}

	private int get_id () {
	    //loop through fragments, increment Id on code fragments
        int id = -1;
        for (Fragment fr : LinCode.fragments()) {
            if (fr instanceof CodeFragment) id++;
            if (fr.equals(this)) return id;
        }
        return -2;
    }

	private String add_body () {
	    String body = "";
	    int id = get_id(); //id of this fragment
        Vector<Vector<AsmInstr>> instructions = AsmGen.fragment_instructions(id);
        HashMap<Temp, Integer> cm = Liveness.igraph.get(id).color_map;
        for (int i=0; i<instructions.size(); i++) {
           Vector<AsmInstr> canon = instructions.get(i);
           if (canon.size() == 0) continue;
           //body += String.format("#Canon: %d%n", i);
           for (int j=0; j<canon.size(); j++) {
                AsmInstr instr = canon.get(j);
                if ( instr instanceof AsmLABEL ) {
                    body += instr.toString(cm);
                    body +=	"\tADD\t$0,$0,0\n"; //all labels point to a dummy operand
                } else {
                    body += "\t";
                    body += instr.toString(cm);
                    body += "\n";
                }
           }
        }
        return body;
    }


	@Override
	public String toAsm () {
        String res = "";
        res += add_prologue();
        res += "\n";
        res += add_body();
        res += "\n";
        res += add_epilogue();
        res += "\n\n";

		return res;
	}

	@Override
	public void log(Logger logger) {
		if (logger == null)
			return;
		logger.begElement("imccode");
		logger.addAttribute("FP", new Long(FP.temp).toString());
		logger.addAttribute("RV", new Long(RV.temp).toString());
		logger.addAttribute("beglabel", begLabel.name);
		logger.addAttribute("endlabel", endLabel.name);
		frame.log(logger);
		for (ImcStmt stmt: stmts) {
			logger.begElement("imclin");
			stmt.accept(new ImcLogger(), logger);
			logger.endElement();
		}
		logger.endElement();
	}
	
}
