package compiler.phases.lincode;

import common.logger.*;
import compiler.phases.frames.Label;
import compiler.phases.frames.Temp;
import compiler.phases.imcgen.ImcVisitor;
import compiler.phases.imcgen.code.*;

import java.util.HashMap;
import java.util.Vector;

public class CanonGenerator implements ImcVisitor<Object, Object> {

    private HashMap<ImcStmt, ImcTEMP> map;
    private Vector<ImcStmt> canon;
    private boolean clear; //while iterating, if we change the tree, clear is set to false
    private int i; //global index of i-th element in canon (for insertions)

    public CanonGenerator(Vector<ImcStmt> canon) {
        this.map = new HashMap<ImcStmt, ImcTEMP>();
        this.canon = canon;
        this.clear = false;
        generate();
    }

    private void generate() {
        while (!clear) {
            clear = true;
            for (this.i=0; this.i<canon.size(); this.i++) {
                ImcStmt tmp = canon.get(this.i);
                tmp.accept(this, null);
            }
        }
    }

    public Vector<ImcStmt>getCanon(){
        return this.canon;
    }

    @Override
    public Object visit(ImcBINOP binOp, Object visArg) {
        //if left or right expression is more complex than a const, create temporary and insert
        boolean changed = false; //if canon changes, repeat iteration
        if (! (binOp.fstExpr instanceof ImcCONST || binOp.fstExpr instanceof ImcTEMP) ) {
            ImcTEMP tmp = new ImcTEMP(new Temp());
            ImcStmt l = new ImcMOVE(tmp, binOp.fstExpr);
            canon.add(this.i, l); this.i++; //move index if we need to insert right expression
            binOp.fstExpr = tmp;
            changed = true;
        }
        if (! (binOp.sndExpr instanceof ImcCONST || binOp.sndExpr instanceof ImcTEMP) ) {
            ImcTEMP tmp = new ImcTEMP(new Temp());
            ImcStmt r = new ImcMOVE(tmp, binOp.sndExpr);
            canon.add(this.i, r);
            binOp.sndExpr = tmp;
            changed = true;
        }
        if (changed) {
            clear = false;
            return null;
        }

        Object tmp;
        tmp = binOp.fstExpr.accept(this, visArg);
        if (tmp instanceof ImcExpr) {
            binOp.fstExpr = (ImcExpr)tmp;
        }
        tmp = binOp.sndExpr.accept(this, visArg);
        if (tmp instanceof ImcExpr) {
            binOp.sndExpr = (ImcExpr)tmp;
        }
        return null;
    }

    @Override
    public Object visit(ImcCALL call, Object visArg) {
        Object tmp;
        for (int j = 0; j < call.args.size(); j++) {
            ImcExpr arg = call.args.elementAt(j);
            tmp = arg.accept(this, visArg);
            if (tmp instanceof ImcCALL) {
                ImcExpr ftemp = new ImcTEMP(new Temp());
                ImcStmt sx = new ImcMOVE(ftemp, (ImcCALL)tmp);
                canon.add(this.i, sx);
                call.args.set(j, ftemp);
            }
            else if (tmp instanceof ImcExpr) {
                call.args.set(j, (ImcExpr) tmp); //replace sexpr arg with an expr
            }
        }
        return call;
    }

    @Override
    public Object visit(ImcCJUMP cjump, Object visArg) {
        Object tmp;
        tmp = cjump.cond.accept(this, visArg);
        if (tmp instanceof ImcCALL) {
            ImcExpr ftemp = new ImcTEMP(new Temp());
            ImcStmt sx = new ImcMOVE(ftemp, (ImcCALL)tmp);
            canon.add(this.i, sx);
            cjump.cond = ftemp;
        }
        else if (tmp instanceof ImcExpr) {
            cjump.cond = (ImcExpr)tmp;
        }
        return null;
    }

    @Override
    public Object visit(ImcCONST constant, Object visArg) {
        return null;
    }

    @Override
    public Object visit(ImcESTMT eStmt, Object visArg) {
        //function call allowed here
        Object tmp;
        tmp = eStmt.expr.accept(this, visArg);
        if (tmp instanceof ImcExpr) {
            eStmt.expr = (ImcExpr)tmp;
        }
        return null;
    }

    @Override
    public Object visit(ImcJUMP jump, Object visArg) {
        return null;
    }

    @Override
    public Object visit(ImcLABEL label, Object visArg) {
        return null;
    }

    @Override
    public Object visit(ImcMEM mem, Object visArg) {
        Object tmp;
        tmp = mem.addr.accept(this, visArg);
        if (tmp instanceof ImcCALL) {
            ImcExpr ftemp = new ImcTEMP(new Temp());
            ImcStmt sx = new ImcMOVE(ftemp, (ImcCALL)tmp);
            canon.add(this.i, sx);
            mem.addr = ftemp;
        }
        else if (tmp instanceof ImcExpr) {
            mem.addr = (ImcExpr)tmp;
        }
        return null;
    }

    @Override
    public Object visit(ImcMOVE move, Object visArg) {
        Object dtmp;
        Object stmp;
        dtmp = move.dst.accept(this, visArg);
        if (dtmp instanceof ImcExpr) {
            move.dst = (ImcExpr)dtmp;
        }

        stmp = move.src.accept(this, visArg);
        if (stmp instanceof ImcCALL) {
            if (dtmp != null) { //MOVE(tmp, f()) is cool (ImcTMP returns null)
                ImcExpr ftemp = new ImcTEMP(new Temp());
                ImcStmt sx = new ImcMOVE(ftemp, (ImcCALL) stmp);
                canon.add(this.i, sx);
                move.src = ftemp;
            }
        }
        if (stmp instanceof ImcExpr) {
            move.src= (ImcExpr)stmp;
        }
        return null;
    }

    @Override
    public Object visit(ImcNAME name, Object visArg) {
        return null;
    }

    @Override
    public Object visit(ImcSEXPR sExpr, Object visArg) {
        //no need to go further, place the children in canon, reiterate

        //bubble up sexpr (prepend to canon)
        canon.add(this.i, sExpr.stmt);
        clear = false;

        //updateLce(sExpr);
        //sExpr.stmt.accept(this, visArg);

        //replace this node with sExpr.expr
        //send signal to parent, to replace me
        return sExpr.expr;

        //updateLce(sExpr);
        //sExpr.expr.accept(this, visArg);

        //canon.add(sExpr.stmt);
        //canon.add(new ImcESTMT(sExpr.expr));
        //return null;
    }

    @Override
    public Object visit(ImcSTMTS stmts, Object visArg) {
        //unpack stmts
        for (ImcStmt stmt:stmts.stmts()) {
            canon.add(this.i, stmt);
            this.i ++;
            //stmt.accept(this, visArg);
        }
        canon.remove(this.i);
        clear = false;
        return null;
    }

    @Override
    public Object visit(ImcTEMP temp, Object visArg) {
        return null;
    }

    @Override
    public Object visit(ImcUNOP unOp, Object visArg) {
        Object tmp = unOp.subExpr.accept(this, visArg);
        if (tmp instanceof ImcCALL) {
            ImcExpr ftemp = new ImcTEMP(new Temp());
            ImcStmt sx = new ImcMOVE(ftemp, (ImcCALL)tmp);
            canon.add(this.i, sx);
            unOp.subExpr = ftemp;
        }
        else if (tmp instanceof ImcExpr) {
            unOp.subExpr = (ImcExpr)tmp;
        }
        return null;
    }
}
