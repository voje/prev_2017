package compiler.phases.lincode;

import common.logger.*;
import compiler.phases.frames.*;

/**
 * A data fragment.
 * 
 * @author sliva
 *
 */
public class DataFragment extends Fragment {

	/** The entry label. */
	public final Label label;

	/** The size of data. */
	public final long size;

	/**
	 * Construct a new data fragment.
	 * 
	 * @param label
	 *            The entry label.
	 * @param size
	 *            The size of data.
	 */
	public DataFragment(Label label, long size) {
		this.label = label;
		this.size = size;
	}

	public String toString () {
		String ret = String.format("label: %s, size: %d", label.name, size);
		return ret;
	}

    @Override
	public String toAsm () {
	    String oper = "???";
	    int count = 999;
	    if (size % 8 == 0) { oper = "OCTA"; count = ((int)size)/8; }
        else if (size % 4 == 0) { oper = "TETRA"; count = ((int)size)/4; }
        else if (size % 2 == 0) { oper = "WYDE"; count = ((int)size)/2; }
        else { oper = "BYTE"; count = (int)size; }
	    String ret = String.format("%s\t%s\t%d", label.name, oper, count);
	    return ret;
    }

	@Override
	public void log(Logger logger) {
		if (logger == null)
			return;
		logger.begElement("imcdata");
		logger.addAttribute("label", label.name);
		logger.addAttribute("size", new Long(size).toString());
		logger.endElement();
	}

}
