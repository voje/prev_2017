package compiler.phases.lincode;

import compiler.phases.imcgen.ImcVisitor;
import compiler.phases.imcgen.code.*;

public class ImcFullVisitor implements ImcVisitor<Object, Object> {

    @Override
    public Object visit(ImcBINOP binOp, Object logger) {
        binOp.fstExpr.accept(this, logger);
        binOp.sndExpr.accept(this, logger);
        return null;
    }

    @Override
    public Object visit(ImcCALL call, Object logger) {
        for (ImcExpr arg : call.args())
            arg.accept(this, logger);
        return null;
    }

    @Override
    public Object visit(ImcCJUMP cjump, Object logger) {
        cjump.cond.accept(this, logger);
        return null;
    }

    @Override
    public Object visit(ImcCONST constant, Object logger) {
        return null;
    }

    @Override
    public Object visit(ImcESTMT eStmt, Object logger) {
        eStmt.expr.accept(this, logger);
        return null;
    }

    @Override
    public Object visit(ImcJUMP jump, Object logger) {
        return null;
    }

    @Override
    public Object visit(ImcLABEL label, Object logger) {
        return null;
    }

    @Override
    public Object visit(ImcMEM mem, Object logger) {
        mem.addr.accept(this, logger);

        return null;
    }

    @Override
    public Object visit(ImcMOVE move, Object logger) {
        move.dst.accept(this, logger);
        move.src.accept(this, logger);
        return null;
    }

    @Override
    public Object visit(ImcNAME name, Object logger) {
        return null;
    }

    @Override
    public Object visit(ImcSEXPR sExpr, Object logger) {
        sExpr.stmt.accept(this, logger);
        sExpr.expr.accept(this, logger);
        return null;
    }

    @Override
    public Object visit(ImcSTMTS stmts, Object logger) {
        for (ImcStmt stmt:stmts.stmts())
            stmt.accept(this, logger);
        return null;
    }

    @Override
    public Object visit(ImcTEMP temp, Object logger) {
        return null;
    }

    @Override
    public Object visit(ImcUNOP unOp, Object logger) {
        unOp.subExpr.accept(this, logger);
        return null;
    }

}
