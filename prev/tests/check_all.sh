#!/bin/bash

#Loop over .prev and .err files in directory. Does not generate xml yet.

if [ $# -ne 1 ];then
  echo "First argument is phase (lexan, ...)"
  exit
fi

phase_dir="$1"
tmpfile="tmpfile.txt"
tmpfile="$(pwd)/$tmpfile"

echo "Writing to: $tmpfile"
echo "" > $tmpfile  #in case it exists

# test:
#filename="0.err"
#exec_command="java -classpath ../out/production/prev/ compiler.Main 
#--target-phase=$1 --logged-phase=$1 --xsl=../../data/ ./${phase_dir}/${filename}"
#$exec_command

cd $phase_dir

for f1 in $(ls -v); do
  ext="${f1##*.}"
  base="${f1%.*}"

  if [[ "$ext" == "xml" ]]; then 
    continue
  fi

  exec_command="java -classpath ../../out/production/prev/ compiler.Main --target-phase=$1 --logged-phase=$1 --xsl=../../data/ ./${f1}"
  
  #run it
  echo "%%%% File: $f1 %%%%"
  $exec_command

  echo #newline
done

#cleanup
cd ..
#todo: analyze tmpfile
#rm $tmpfile

# from /src, compile using: $ javac compiler/Main.java
# this worked in command line: $ java compiler.Main --logged-phase=lexan --xsl=../data/ ../tests/all.prev
# run a python server in /prev, to check results in browser
