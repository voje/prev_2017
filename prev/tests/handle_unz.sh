#!/bin/bash

if [ $# -ne 1 ];then
  echo "First argument is dest directory (lexan, ...)"
  exit
fi

dirname="unzipped"
destdir="$1"
pwd=$(pwd)
destdir="$pwd/$destdir"

if [ ! -d $destdir ]; then
  echo "dir error"
  exit
fi

pn=0

function d_walk {
  cd $1
  for d in $(ls); do
    #edit file name (we want unique, use dir struct)
    #pn=$2$d
    #pn="${pn//./_}"

    if [ -d $d ]; then
      echo "dir: $d"
      d_walk $d $pn
    elif [ -f $d ]; then
      echo "file: $pn.prev"
      destname="$destdir/$pn.prev"
      cp $d $destname
      pn=$((pn + 1))
    fi
  done 
  cd ..
}

d_walk $dirname $pn
