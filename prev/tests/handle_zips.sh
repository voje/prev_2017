#!/bin/bash

#unzip everzthing into outdir

#find /home/kristjan/Downloads/ -name *.zip -mmin -60 -exec cp {} ./zips/ \;

p=$(pwd)
indir="zips"
outdir="unzipped"

indir="$p/$indir"
outdir="$p/$outdir"

#clear outdir
$(rm -rf "$outdir/")
mkdir $outdir

cd $indir

for z in *; do
  echo $z
  mkdir $outdir/$z
  unzip $z -d $outdir/$z/
done

cd $p

