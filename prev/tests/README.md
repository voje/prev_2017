# Usage

```handle_zips.sh```
Unzips /zips into /unzipped

```handle_unz.sh```
Copies files from /unzipped to /lexan (arg) with unique file names based on
tree path.

```check_all.sh```
Based on default intellij path (/out/...) runs the program and generates .xml
files in /lexan (arg).

.
├── check_all.sh
├── emit_a.sh
├── handle_unz.sh
├── handle_zips.sh
├── lexan
│   ├── 0.err
│   ├── 0.lexan.lexan.xml
│   ├── 0.lexan.xml
│   ├── 0.prev
│   ├── 1.err
│   ├── 1.lexan.lexan.xml
│   ├── 1.lexan.xml
│   ├── 2.err
│   ├── 2.lexan.lexan.xml
│   ├── 2.lexan.xml
│   ├── 3.err
│   ├── 3.lexan.lexan.xml
│   ├── 3.lexan.xml
│   ├── all.lexan.lexan.xml
│   ├── all.lexan.xml
│   ├── all.prev
│   ├── intconst.lexan.lexan.xml
│   ├── intconst.lexan.xml
│   ├── intconst.prev
│   ├── lexan_testi_zipnapacni_1_prev
│   ├── lexan_testi_zipnapacni_1_prev.lexan.xml
│   ├── lexan_testi_zipnapacni_2_prev
│   ├── lexan_testi_zipnapacni_2_prev.lexan.xml
│   ├── lexan_testi_zipnapacni_3_prev
│   ├── lexan_testi_zipnapacni_3_prev.lexan.xml
│   ├── lexan_testi_zippravilni_1_prev
│   ├── lexan_testi_zippravilni_1_prev.lexan.xml
│   ├── lexan_testi_zippravilni_2_prev
│   ├── lexan_testi_zippravilni_2_prev.lexan.xml
│   ├── lexan_testi_zippravilni_3_prev
│   ├── lexan_testi_zippravilni_3_prev.lexan.xml
│   ├── prev_tests_zipfail_char1_prev
│   ├── prev_tests_zipfail_char1_prev.lexan.xml
│   ├── prev_tests_zipfail_char2_prev
│   ├── prev_tests_zipfail_char2_prev.lexan.xml
│   ├── prev_tests_zipfail_test2_prev
│   ├── prev_tests_zipfail_test2_prev.lexan.xml
│   ├── prev_tests_zipidents_prev
│   ├── prev_tests_zipidents_prev.lexan.xml
│   ├── prev_tests_zipsymbols_prev
│   ├── prev_tests_zipsymbols_prev.lexan.xml
│   ├── prev_tests_ziptest_prev
│   ├── prev_tests_ziptest_prev.lexan.xml
│   ├── spaces.lexan.lexan.xml
│   ├── spaces.lexan.xml
│   ├── spaces.prev
│   ├── stuff.lexan.lexan.xml
│   ├── stuff.lexan.xml
│   ├── stuff.prev
│   ├── testi(1)_ziptest0_prev
│   ├── testi(1)_ziptest0_prev.lexan.xml
│   ├── testi(1)_ziptest1_prev
│   ├── testi(1)_ziptest1_prev.lexan.xml
│   ├── testi(1)_ziptest2_prev
│   ├── testi(1)_ziptest2_prev.lexan.xml
│   ├── testi(1)_ziptest3_prev
│   ├── testi(1)_ziptest3_prev.lexan.xml
│   ├── testi(1)_ziptest4_prev
│   ├── testi(1)_ziptest4_prev.lexan.xml
│   ├── testi(1)_ziptest5_prev
│   ├── testi(1)_ziptest5_prev.lexan.xml
│   ├── testi(2)_ziptestitest1_prev
│   ├── testi(2)_ziptestitest1_prev.lexan.xml
│   ├── testi(2)_ziptestitest2_prev
│   ├── testi(2)_ziptestitest2_prev.lexan.xml
│   ├── testi(2)_ziptestitest3_prev
│   ├── testi(2)_ziptestitest3_prev.lexan.xml
│   ├── testi(2)_ziptestitest4_prev
│   ├── testi(2)_ziptestitest4_prev.lexan.xml
│   ├── testi(2)_ziptestitest5_prev
│   ├── testi(2)_ziptestitest5_prev.lexan.xml
│   ├── testi(2)_ziptestitest6_prev
│   ├── testi(2)_ziptestitest6_prev.lexan.xml
│   ├── testi(3)_zip0_prev
│   ├── testi(3)_zip0_prev.lexan.xml
│   ├── testi(3)_zip1_prev
│   ├── testi(3)_zip1_prev.lexan.xml
│   ├── testi(3)_zip2_prev
│   ├── testi(3)_zip2_prev.lexan.xml
│   ├── testi(3)_zip3_prev
│   ├── testi(3)_zip3_prev.lexan.xml
│   ├── testi(3)_zip4_prev
│   ├── testi(3)_zip4_prev.lexan.xml
│   ├── testi(3)_zip5_prev
│   ├── testi(3)_zip5_prev.lexan.xml
│   ├── testi_lexan_ziperr1_prev
│   ├── testi_lexan_ziperr1_prev.lexan.xml
│   ├── testi_lexan_ziperr2_prev
│   ├── testi_lexan_ziperr2_prev.lexan.xml
│   ├── testi_lexan_ziperr3_prev
│   ├── testi_lexan_ziperr3_prev.lexan.xml
│   ├── testi_lexan_zipok1_prev
│   ├── testi_lexan_zipok1_prev.lexan.xml
│   ├── testi_lexan_zipok2_prev
│   ├── testi_lexan_zipok2_prev.lexan.xml
│   ├── testi_lexan_zipok3_prev
│   ├── testi_lexan_zipok3_prev.lexan.xml
│   ├── testi_ziptest1
│   ├── testi_ziptest1.lexan.xml
│   ├── testi_ziptest2
│   ├── testi_ziptest2.lexan.xml
│   ├── testi_ziptest3
│   ├── testi_ziptest3.lexan.xml
│   ├── testi_ziptest4
│   ├── testi_ziptest4.lexan.xml
│   ├── testi_ziptest5
│   ├── testi_ziptest5.lexan.xml
│   ├── testi_ziptest6
│   ├── testi_ziptest6.lexan.xml
│   ├── testi_ziptest7
│   ├── testi_ziptest7.lexan.xml
│   ├── tests_ziptestsfail1_prev
│   ├── tests_ziptestsfail1_prev.lexan.xml
│   ├── tests_ziptestsfail2_prev
│   ├── tests_ziptestsfail2_prev.lexan.xml
│   ├── tests_ziptestsfail3_prev
│   ├── tests_ziptestsfail3_prev.lexan.xml
│   ├── tests_ziptestsok1_prev
│   ├── tests_ziptestsok1_prev.lexan.xml
│   ├── tests_ziptestsok2_prev
│   ├── tests_ziptestsok2_prev.lexan.xml
│   ├── tests_ziptestsok3_prev
│   └── tests_ziptestsok3_prev.lexan.xml
├── README.md
├── test_run.sh
├── tmpfile.txt
├── unzipped
│   ├── lexan_testi.zip
│   │   ├── napacni_1.prev
│   │   ├── napacni_2.prev
│   │   ├── napacni_3.prev
│   │   ├── pravilni_1.prev
│   │   ├── pravilni_2.prev
│   │   └── pravilni_3.prev
│   ├── prev_tests.zip
│   │   ├── fail_char1.prev
│   │   ├── fail_char2.prev
│   │   ├── fail_test2.prev
│   │   ├── idents.prev
│   │   ├── symbols.prev
│   │   └── test.prev
│   ├── testi(1).zip
│   │   ├── test0.prev
│   │   ├── test1.prev
│   │   ├── test2.prev
│   │   ├── test3.prev
│   │   ├── test4.prev
│   │   └── test5.prev
│   ├── testi(2).zip
│   │   └── testi
│   │       ├── test1.prev
│   │       ├── test2.prev
│   │       ├── test3.prev
│   │       ├── test4.prev
│   │       ├── test5.prev
│   │       └── test6.prev
│   ├── testi(3).zip
│   │   ├── 0.prev
│   │   ├── 1.prev
│   │   ├── 2.prev
│   │   ├── 3.prev
│   │   ├── 4.prev
│   │   └── 5.prev
│   ├── testi_lexan.zip
│   │   ├── err1.prev
│   │   ├── err2.prev
│   │   ├── err3.prev
│   │   ├── ok1.prev
│   │   ├── ok2.prev
│   │   └── ok3.prev
│   ├── testi.zip
│   │   ├── test1
│   │   ├── test2
│   │   ├── test3
│   │   ├── test4
│   │   ├── test5
│   │   ├── test6
│   │   └── test7
│   └── tests.zip
│       └── tests
│           ├── fail1.prev
│           ├── fail2.prev
│           ├── fail3.prev
│           ├── ok1.prev
│           ├── ok2.prev
│           └── ok3.prev
└── zips
    ├── lexan_testi.zip
    ├── prev_tests.zip
    ├── testi(1).zip
    ├── testi(2).zip
    ├── testi(3).zip
    ├── testi_lexan.zip
    ├── testi.zip
    └── tests.zip

13 directories, 187 files
