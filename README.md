# Prevajalniki
Delujoči prevajalnik iz izmišljenega jezika Prev v strojno kodo za arhitekrutor MMIX. 
Prevajalnik je napisan v jeziku Java. 

## For creating a zip with excludes:
Go to repo root.
```bash
$zip -r 63xxxxxx-xx.zip prev -x prev/.idea/\* -x prev/.gitignore -x prev/prev.iml -x prev/out/\*
```

## Main arguments (intellij settings):
```--target-phase=lexan --logged-phase=lexan --xsl=../../data/ tests/lexan/0.prev```

## Syntax analysis
Hocemo desno rekurzivno gramatiko.
Odpravljanje leve rekurzije:
```
S -> a A .
S -> a B .
A -> .
B -> .

--->

S -> a S'.
S'-> A .
S'-> B .
A -> .
B -> .

--->

S -> a S'.
S'-> .
```
Check out <http://mdaines.github.io/grammophone/#/>.
S spodnjo gramatiko deluje funkcija TRANSFORM.
```
e -> e + t | t .
t -> t * f | f .
f -> 0 | ( e ) .
```
-> remove immediate left recursion (da ne bo treba na roke).
After you get LL(1), start coding! :) ( For every line in LL(1) Parsing table,
write a method. For every nonempty cell in row, write a case statement. )

This one is LL(1):
```
e -> t e' .
e' -> + t e' .
e' -> .
t -> f t' .
t' -> * f t' .
t' -> .
f -> 0 .
f -> ( e ) .
```
Pseudocode for parseE()
```
parseE() {
  switch() {
    case '0':
      parseT();
      parseE'(); break;
    case '(':
      parseT();
      parseE'(); break;
    default:
      err();
  }
}

parseF() {
  switch() {
    case '0':
      skip('0'); break;
    case '(':
      skip('(');
      parseE();
      skip('('); break;
    default:
      err();
  }
}
```


